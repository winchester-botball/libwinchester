#include "lego.h"
#include <math.h>
#include <kipr/wombat.h>

/*! \file lego.c
* General lego bot moving functions
*
*Contains many of the functions that drive lego bots
*
*/

/** Independently sets the right and left motor speeds using percent of max power 
*
* Independently sets motor values, which are percents of max power, of the right and left motor to allow movement. The motors will move indefintately unless explicitly stopped
* 
* @param left The percent power to use to move the left motor 
* @param right The percent power to use to move the right motor 
* 
*/
void motor_drive(int left, int right) {
   motor(LEFT_MOTOR, left);
   motor(RIGHT_MOTOR, right); 
}


/** Stops all motor movement
*
*It will cause the motors to stop moving and hold its position, in otherwords active breaking. It is better than setting motors to 0 power or mav and should 
*
*/
void drive_freeze() {
    freeze(LEFT_MOTOR);
    freeze(RIGHT_MOTOR);
}

/** Clears the motor rotation counters for the right and left motor
*
* Clears or sets the rotation counter of the right and left motor to 0
*
*/
void drive_clear() {
    cmpc(LEFT_MOTOR);
    cmpc(RIGHT_MOTOR);
}

/** Will set the velocity of each of the bots two wheels independently
*
*It will independently set the left and right motors speeds with mav and they will maintain that speed until they are stopped
*
* @param left The mav value used to set the speed of the left motor
* @param right The mav value used to set the speed of the right motor
*
*/
void drive(int left, int right) {
    mav(LEFT_MOTOR, left);
    mav(RIGHT_MOTOR, right);
}

/** A complicated way to turn right
 * 
 * Turns to the right using degrees for how much it should turn and raduis for the radius of the turn, allowing for exact point and swing turns
 * 
 * @param degrees How much the bot will turn in terms of degrees
 * @param radius The distance to the center of the circle created by the turn
 * 
 */

/*
void right(int degrees, double radius) {
    long left_arc = ((2 * radius + ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    long right_arc = ((2 * radius - ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    if (left_arc == 0l) {
        printf("Error, no turn. Aborting.");
        return;
    }
    int turn_r_speed = round(((float)right_arc / (float)left_arc) * SPD_R_TURN);
    if (turn_r_speed < 0)
        turn_r_speed = -turn_r_speed;
    if (left_arc > 0l)
        mav(left_motor, SPD_L_F);
    else
        mav(left_motor, -SPD_L_B);
    if (right_arc > 0l)
        mav(right_motor, turn_r_speed);
    else
        mav(right_motor, -turn_r_speed);
    left_arc += gmpc(left_motor);
    right_arc += gmpc(right_motor);
    if (right_arc - gmpc(right_motor) > 0l) {
        if (left_arc - gmpc(left_motor) > 0l) {
            while (right_arc > gmpc(right_motor) || left_arc > gmpc(left_motor)) {
                if (right_arc < gmpc(right_motor))
                    freeze(right_motor);
                if (left_arc < gmpc(left_motor))
                    freeze(left_motor);
            }
        } else {
            while (right_arc > gmpc(right_motor) || left_arc < gmpc(left_motor)) {
                if (right_arc < gmpc(right_motor))
                    freeze(right_motor);
                if (left_arc > gmpc(left_motor))
                    freeze(left_motor);
            }
        }
    } else {
        if (left_arc - gmpc(left_motor) > 0l) {
            while (right_arc < gmpc(right_motor) || left_arc > gmpc(left_motor)) {
                if (right_arc > gmpc(right_motor))
                    freeze(left_motor);
                if (left_arc < gmpc(left_motor))
                    freeze(left_motor);
            }
        } else {
            while (right_arc < gmpc(right_motor) || left_arc < gmpc(left_motor)) {
                if (right_arc > gmpc(right_motor))
                    freeze(right_motor);
                if (left_arc > gmpc(left_motor))
                    freeze(left_motor);
            }
        }
    }
    drive_freeze();
}
*/


/** A complicated way to turn right
 * 
 * Turns to the right using degrees for how much it should turn and raduis for the radius of the turn, allowing for exact point and swing turns.
 * Also, turns at a spefic speed.
 * 
 * @param degrees How much the bot will turn in terms of degrees
 * @param radius The distance to the center of the circle created by the turn
 * @param speed In mav, the speed that the motors will turn to execute the turn, thus the speed the turn is executed at
 * 
 */

/*
void right_speed(int degrees, double radius, int speed) {
    long left_arc = ((2 * radius + ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    long right_arc = ((2 * radius - ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    if (left_arc == 0l) {
        printf("Error, no turn. Aborting.");
        return;
    }
    int turn_r_speed = round(((float)right_arc / (float)left_arc) * SPD_R_TURN);
    if (turn_r_speed < 0)
        turn_r_speed = -turn_r_speed;
    if (left_arc > 0l)
        mav(left_motor, SPD_L_F * speed / MAX_SPEED);
    else
        mav(left_motor, -SPD_L_B * speed / MAX_SPEED);
    if (right_arc > 0l)
        mav(right_motor, turn_r_speed * speed / MAX_SPEED);
    else
        mav(right_motor, -turn_r_speed * speed / MAX_SPEED);
    left_arc += gmpc(left_motor);
    right_arc += gmpc(right_motor);
    if (right_arc - gmpc(right_motor) > 0l) {
        if (left_arc - gmpc(left_motor) > 0l) {
            while (right_arc > gmpc(right_motor) || left_arc > gmpc(left_motor)) {
                if (right_arc < gmpc(right_motor))
                    freeze(right_motor);
                if (left_arc < gmpc(left_motor))
                    freeze(left_motor);
            }
        } else {
            while (right_arc > gmpc(right_motor) || left_arc < gmpc(left_motor)) {
                if (right_arc < gmpc(right_motor))
                    freeze(right_motor);
                if (left_arc > gmpc(left_motor))
                    freeze(left_motor);
            }
        }
    } else {
        if (left_arc - gmpc(left_motor) > 0l) {
            while (right_arc < gmpc(right_motor) || left_arc > gmpc(left_motor)) {
                if (right_arc > gmpc(right_motor))
                    freeze(right_motor);
                if (left_arc < gmpc(left_motor))
                    freeze(left_motor);
            }
        } else {
            while (right_arc < gmpc(right_motor) || left_arc < gmpc(left_motor)) {
                if (right_arc > gmpc(right_motor))
                    freeze(right_motor);
                if (left_arc > gmpc(left_motor))
                    freeze(left_motor);
            }
        }
    }
    drive_freeze();
}

*/

/** A complicated way to turn left
 * 
 * Turns to the left using degrees for how much it should turn and raduis for the radius of the turn, allowing for exact point and swing turns
 * 
 * @param degrees How much the bot will turn in terms of degrees
 * @param radius The distance to the center of the circle created by the turn
 * 
 */

/*
void left(int degrees, double radius) {
    long left_arc = ((2 * radius - ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    long right_arc = ((2 * radius + ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    if (right_arc == 0l) {
        printf("Error, no turn. Aborting.");
        return;
    }
    int turn_l_speed = round((float)left_arc / (float)right_arc * SPD_L_TURN);
    if (turn_l_speed < 0)
        turn_l_speed = -turn_l_speed;
    if (right_arc  > 0l)
        mav(right_motor, SPD_R_F);
    else
        mav(right_motor, -SPD_R_B);
    if (left_arc > 0l)
        mav(left_motor, turn_l_speed);
    else
        mav(left_motor, -turn_l_speed);
    right_arc += gmpc(right_motor);
    left_arc += gmpc(left_motor);
    if (left_arc - gmpc(left_motor) > 0l) {
        if (right_arc - gmpc(right_motor) > 0l) {
            while (left_arc > gmpc(left_motor) || right_arc > gmpc(right_motor)) {
                if (left_arc < gmpc(left_motor))
                    freeze(left_motor);
                if (right_arc < gmpc(right_motor))
                    freeze(right_motor);
            }
        } else {
            while (left_arc > gmpc(left_motor) || right_arc < gmpc(right_motor)) {
                if (left_arc < gmpc(left_motor))
                    freeze(left_motor);
                if (right_arc > gmpc(right_motor))
                    freeze(right_motor);
            }
        }
    } else {
        if (right_arc - gmpc(right_motor) > 0l) {
            while (left_arc < gmpc(left_motor) || right_arc > gmpc(right_motor)) {
                if (left_arc > gmpc(left_motor))
                    freeze(left_motor);
                if (right_arc < gmpc(right_motor))
                    freeze(right_motor);
            }
        } else {
            while (left_arc < gmpc(left_motor) || right_arc < gmpc(right_motor)) {
                if (left_arc > gmpc(left_motor))
                    freeze(left_motor);
                if (right_arc > gmpc(right_motor))
                    freeze(right_motor);
            }
        }
    }
    drive_off();
}
*/


/** A complicated way to turn left
 * 
 * Turns to the left using degrees for how much it should turn and raduis for the radius of the turn, allowing for exact point and swing turns.
 * Also, turns at a spefic speed.
 * 
 * @param degrees How much the bot will turn in terms of degrees
 * @param radius The distance to the center of the circle created by the turn
 * @param speed In mav, the speed that the motors will turn to execute the turn, thus the speed the turn is executed at
 * 
 */

/*
void left_speed(int degrees, double radius, int speed) {
    long left_arc = ((2 * radius - ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    long right_arc = ((2 * radius + ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    if (right_arc == 0l) {
        printf("Error, no turn. Aborting.");
        return;
    }
    int turn_l_speed = round((float)left_arc / (float)right_arc * SPD_L_TURN);
    if (turn_l_speed < 0)
        turn_l_speed = -turn_l_speed;
    if (right_arc  > 0l)
        mav(right_motor, SPD_R_F * speed / MAX_SPEED);
    else
        mav(right_motor, -SPD_R_B * speed / MAX_SPEED);
    if (left_arc > 0l)
        mav(left_motor, turn_l_speed * speed / MAX_SPEED);
    else
        mav(left_motor, -turn_l_speed * speed / MAX_SPEED);
    right_arc += gmpc(right_motor);
    left_arc += gmpc(left_motor);
    if (left_arc - gmpc(left_motor) > 0l) {
        if (right_arc - gmpc(right_motor) > 0l) {
            while (left_arc > gmpc(left_motor) || right_arc > gmpc(right_motor)) {
                if (left_arc < gmpc(left_motor))
                    freeze(left_motor);
                if (right_arc < gmpc(right_motor))
                    freeze(right_motor);
            }
        } else {
            while (left_arc > gmpc(left_motor) || right_arc < gmpc(right_motor)) {
                if (left_arc < gmpc(left_motor))
                    freeze(left_motor);
                if (right_arc > gmpc(right_motor))
                    freeze(right_motor);
            }
        }
    } else {
        if (right_arc - gmpc(right_motor) > 0l) {
            while (left_arc < gmpc(left_motor) || right_arc > gmpc(right_motor)) {
                if (left_arc > gmpc(left_motor))
                    freeze(left_motor);
                if (right_arc < gmpc(right_motor))
                    freeze(right_motor);
            }
        } else {
            while (left_arc < gmpc(left_motor) || right_arc < gmpc(right_motor)) {
                if (left_arc > gmpc(left_motor))
                    freeze(left_motor);
                if (right_arc > gmpc(right_motor))
                    freeze(right_motor);
            }
        }
    }
    drive_off();
}
*/

/** A complicated way to move a spefic number of cm straight forward
 * 
 * This uses the motor tick counter to calculate how much rotation is neccary to cover a 
 * speficic distance forward with the diameter of the wheel taken into acount in the code
 * 
 * @param distance In centimeters, the distance forward the robot will travel
 * 
 */

/*
void forward(int distance) {
    if (distance < 0) {
        distance = -distance;
        printf("Error, negative distance! Switching to positive\n");
    }
    long move_distance = distance * CM_TO_BEMF;
    long l_target = gmpc(left_motor) + move_distance;
    long r_target = gmpc(right_motor) + move_distance;
    mav(left_motor, SPD_L_F);
    mav(right_motor, SPD_R_F);
    while (gmpc(left_motor) < l_target && gmpc(right_motor) < r_target) {
        if (gmpc(left_motor) >= l_target)
            freeze(left_motor);
        if (gmpc(right_motor) >= r_target)
            freeze(right_motor);
    }
    drive_freeze();
}
*/


/** A complicated way to move a spefic number of centimeters straight forward at a certain speed
 * 
 * This uses the motor tick counter to calculate how much rotation is neccary to cover a 
 * speficic distance forward with the diameter of the wheel taken into acount in the code.
 * Also the speed of the motors are input in mav.
 * 
 * @param distance In centimeters, the distance forward the robot will travel
 * @param speed In mav, the speed the motors' will move at for the duration of the movement forward
 * 
 */

/*
void forward_speed(int distance, int speed) {
    if (distance < 0) {
        distance = -distance;
        printf("Error, negative distance! Switching to positive\n");
    }
    long move_distance = distance * CM_TO_BEMF;
    long l_target = gmpc(left_motor) + move_distance;
    long r_target = gmpc(right_motor) + move_distance;
    mav(left_motor, speed * SPD_L_F / MAX_SPEED);
    mav(right_motor, speed * SPD_R_F / MAX_SPEED);
    while (gmpc(left_motor) < l_target && gmpc(right_motor) < r_target) {
        if (gmpc(left_motor) >= l_target)
            freeze(left_motor);
        if (gmpc(right_motor) >= r_target)
            freeze(right_motor);
    }
    drive_freeze();
}
*/

/** A complicated way to move a spefic number of centimeters straight backwards at a certain speed
 * 
 * This uses the motor tick counter to calculate how much rotation is neccary to cover a 
 * speficic distance backwards with the diameter of the wheel taken into acount in the code.
 * 
 * @param distance In centimeters, the distance backwards the robot will travel
 * 
 */

/*
void backward(int distance) {
    if (distance < 0) {
        distance = -distance;
        printf("Error, negative distance! Switching to positive\n");
    }
    long move_distance = distance * CM_TO_BEMF;
    long l_target = gmpc(left_motor) - move_distance;
    long r_target = gmpc(right_motor) - move_distance;
    mav(left_motor, -SPD_L_B);
    mav(right_motor, -SPD_R_B);
    while (gmpc(left_motor) > l_target && gmpc(right_motor) > r_target) {
        if (gmpc(left_motor) <= l_target)
            freeze(left_motor);
        if (gmpc(right_motor) <= r_target)
            freeze(right_motor);
    }
    drive_freeze();
}
*/

/** A complicated way to move a spefic number of centimeters straight backwards at a certain speed
 * 
 * This uses the motor tick counter to calculate how much rotation is neccary to cover a 
 * speficic distance backwards with the diameter of the wheel taken into acount in the code.
 * Also the speed of the motors are input in mav.
 * 
 * @param distance In centimeters, the distance backwards the robot will travel
 * @param speed In mav, the speed the motors will move at for the duration of the movement backwards
 * 
 */

/*
void backward_speed(int distance, int speed) {
    if (distance < 0) {
        distance = -distance;
        printf("Error, negative distance! Switching to positive\n");
    }
    long move_distance = distance * CM_TO_BEMF;
    long l_target = gmpc(left_motor) - move_distance;
    long r_target = gmpc(right_motor) - move_distance;
    mav(left_motor, -speed * SPD_L_B / MAX_SPEED);
    mav(right_motor, -speed * SPD_R_B / MAX_SPEED);
    while (gmpc(left_motor) > l_target && gmpc(right_motor) > r_target) {
        if (gmpc(left_motor) <= l_target)
            freeze(left_motor);
        if (gmpc(right_motor) <= r_target)
            freeze(right_motor);
    }
    drive_freeze();
}
*/

/** A complicated way to move a specific distance forwards using gyro
 * 
 * Uses gyro to calculate the distance traveled so the robot can move a spefic distance forwards
 * 
 * @param distance The distance forwards that the robot will move forward
 * @param speed The speed in mav at which the robot will move forward
 * 
 */

/*
void forward_gyro(int distance, int speed) {
    if (distance < 0) {
        distance = -distance;
        printf("Error, negative distance! Switching to positive\n");
    }
    if (speed >= 1450) {
        speed = 1400;  // Cannot be full speed or slightly less otherwise gyro corrections won't work
    }

    // TODO: Some gyro calibration code here - Kipr has a function but hasn't implemented yet

    long move_distance = distance * CM_TO_BEMF;
    long l_target = gmpc(left_motor) + move_distance;
    long r_target = gmpc(right_motor) + move_distance;

    mav(left_motor, speed);
    mav(right_motor, speed);

    while (gmpc(left_motor) > l_target && gmpc(right_motor) > r_target) {
        // Not sure which one to use here
        double gyroX = gyro_x();
        if (gyroX > 0.1) {
            mav(right_motor, speed*Kp);
            mav(left_motor, speed);
            msleep(10);
        }
        if (gyroX < 0.1) {
            mav(left_motor, speed*Kp);
            mav(right_motor, speed);
            msleep(10);
        }

        if (gmpc(left_motor) <= l_target)
            freeze(left_motor);
        if (gmpc(right_motor) <= r_target)
            freeze(right_motor);
    }
}
*/

/** A complicated way to turn right using gyro
 * 
 * Turns to the right using degrees for how much it should turn and gyro to calulate how much the robot has turned
 * Also, turns at a spefic speed.
 * 
 * @param speed In mav, the speed that the motors' will turn to execute the turn, thus the speed the turn is executed at
 * @param degrees How much the bot will turn in terms of degrees
 * 
 */

/*
void right_turn(int speed, float degrees) {
    msleep(50);
    gyro_z();
    float offset = 0;
    float final_rot = -degrees * GYRO_PER_ROT / 360;
    while(offset > final_rot) {
        drive(speed * LEFT_OFFSET, -speed * RIGHT_OFFSET);
        if (offset < final_rot * 0.6)
            drive(speed * LEFT_OFFSET / 2, -speed * RIGHT_OFFSET / 2);
        int val = gyro_z();
        offset += val - GYRO_DEV;
        msleep(50);
    }
    ao();
    msleep(100);
}
*/

/** A complicated way to turn left using gyro
 * 
 * Turns to the left using degrees for how much it should turn and gyro to calulate how much the robot has turned
 * Also, turns at a specific speed.
 * 
 * @param speed In mav, the speed that the motors' will turn to execute the turn, thus the speed the turn is executed at
 * @param degrees How much the bot will turn in terms of degrees
 * 
 */

/*
void left_turn(int speed, float degrees) {
    msleep(50);
    gyro_z();
    float offset = 0;
    float final_rot = degrees * GYRO_PER_ROT / 360;
    while(offset < final_rot) {
        create_drive_direct(-speed * LEFT_OFFSET, speed * RIGHT_OFFSET);
        if (offset > final_rot * 0.6)
            drive(-speed * LEFT_OFFSET / 2, speed * RIGHT_OFFSET / 2);
        int val = gyro_z();
        offset += val - GYRO_DEV;
        msleep(50);
    }
    ao();
    msleep(100);
}
*/

/** Here's how you calculate the fudge used in the driveUntilDistance function. Put this into your own function and call it in main
 *  when you first test the motors
 *
    int motor_left = 0;
    int motor_right = 0;
    
    while(right_button()==0) {
        msleep(1);
    }
    cmpc(lmotor);
    cmpc(rmotor);
    mav(lmotor, 500);
    msleep(2000);
    mav(lmotor, 100);
    while(right_button()==0) {
        msleep(1);
    }
    stop();
    motor_left = gmpc(lmotor);
    printf("%d ", motor_left);
    msleep(2000);
    while(right_button()==0) {
        msleep(1);
    }
    cmpc(rmotor);
    mav(rmotor, 500);
    msleep(2000);
    mav(rmotor, 100);
    while(right_button()==0) {
        msleep(1);
    }
    stop();
    motor_right = gmpc(rmotor);
    printf("- %d\n", motor_right);
    if(motor_right > motor_left) {
        fudge = -motor_left * 1.0 / (motor_right * 1.0);
    } else {
        fudge = motor_right * 1.0 / (motor_left * 1.0);
    }
    printf("Fudge: %f\n", fudge);
    msleep(2000);
    while(right_button()==0) {
        msleep(1);
    }
    drive(500, 500);
    msleep(10000);
    stop();
    msleep(300);
*/


/** drive by distance based on motor ticks 
 *  
 * Uses diameter of the wheel and motor ticks to drive the distance specified
 * Must also predefine diameter of the wheel
 * 
 * @param distance the distance you want the wheels to move
 * @param rate speed you want the motors to move
 *
 */

/*
void drive_by_distance(float distance, int rate){ //distance is in cm
    float rotations = distance/diam/M_PI;
    int totalTicks = (int)(rotations * 1800.0);
    printf("%d\n", totalTicks);
    mrp(LEFT_MOTOR, rate*(fudge < 0 ? -fudge : 1), (int)totalTicks*(fudge < 0 ? -fudge : 1));
    mrp(RIGHT_MOTOR, rate*(fudge > 0 ? fudge : 1), (int)totalTicks*(fudge > 0 ? fudge : 1));
    bmd(LEFT_MOTOR);
    bmd(RIGHT_MOTOR);
    drive_freeze();
}
*/