/** Krakatoa: The Chad 2.0 Robot of 2018
*
* Code for The Chad 2.0 of Winchester High School Botball Robotics Team
* This code was created for the Botball competition during 2018 (GCER)
*
* @team: Andrea Huang, Jason Lee, Rhea Desai, Roy Xing
*
* Sensors used: Linear Slide (Yeah, that's right, we actually use it)
* Actuators: 2 Servos, 1 Motor 
* Meant for the Wallaby controller from KIPR (external battery type)
*
* Robot Description: The Chad 2.0 Robot
*
* MOTORS are plugged in ground (BLACK) on outside, power (RED) on inside
*
* PUT CREATE IN SAFE MODE to charge
* Create Line Sensors: > Threshold = white
*/

//Constants---------------------------------------------------------------------------------------------
#include <kipr/botball.h>

#define LINEAR_PORT 0
#define PULLEY_PORT 0 
#define CLAW_PORT 3
#define WRIST_PORT 2

#define CLAW_CLOSE_POMS 1700 //1750
#define CLAW_CLOSE_FRISBEE 1630 //1620
#define CLAW_WIDE 1000
#define CLAW_OPEN 1200
#define CLAW_EXTRA_WIDE 840
#define CLAW_STARTING 1190

#define WRIST_UP 15
#define WRIST_DOWN 10
#define WRIST_DROP 100 //190
#define WRIST_VERTICAL 1900
#define WRIST_STARTING 1960

#define PULLEY_MAX 2750
#define PULLEY_UP 2400
#define PULLEY_START 716
#define PULLEY_DOWN 90

#define THRESHOLD 2000
#define TOPHAT_THRESHOLD 3000	//black < 3000 and white > 3000 //CHANGE POSSIBLEY
#define TOP_HAT_LEFT 3
#define TOP_HAT_RIGHT 1
#define LIGHT_PORT 5
//Servo and Motor Functions------------------------------------------------------------------------------

void drive(int left, int right){ 
    create_drive_direct(left, right);
}

void interpolateServoByTime(int port, int start, int end, int time){
    unsigned long endTime = systime() + time;
    while(systime() < endTime){
        set_servo_position(port, (end - start) * (1.0 - (((double)(endTime - systime())) / ((double)time))) + start);
        msleep(1);
    }
    set_servo_position(port, end);
}

int getSign(int n, int tolerance){
    if (n > tolerance) {
        return 1;
    } else if (n < -tolerance) {
        return -1;
    } else {
        return 0;
    }
}

void lineFollow(int timeout, int speed, int delay){
    unsigned long endTime = systime() + timeout;
    unsigned long time = systime();
    while ((time = systime()) < endTime) {
        int tophatValueLeft = analog(TOP_HAT_LEFT); // CHANGE analog(TOPHAT_PORT)
        int tophatValueRight = analog(TOP_HAT_RIGHT);
        float tophatLeft = (tophatValueRight > TOPHAT_THRESHOLD ? -0.7 : -1);
        float tophatRight = (tophatValueLeft > TOPHAT_THRESHOLD ? -0.7 : -1); 
        int currentSpeed = (tophatLeft == tophatRight ? speed : speed);
        create_drive_direct((int)(((float)currentSpeed) * tophatLeft), (int)(((float)currentSpeed) * tophatRight));
        msleep(delay);
    } 
    create_drive_direct(0, 0);
}

void interpolatePulleyWithTolerance(int power, int target, int tolerance) {
    int currentLinear = analog(LINEAR_PORT);
    int prevLinear = currentLinear;
    int sign = getSign(currentLinear - target, tolerance);
    int currentSign = sign;

    int warmup = 1000;
    int pushIndex = 0;
    int deltaArray[500] = {[0 ... 499] = 9999};
    int arraySize = (sizeof(deltaArray) / sizeof(int*));
    int totalDelta = deltaArray[0] * arraySize;

    motor(PULLEY_PORT, sign * power);
    while (currentSign == sign) {
        msleep(1);
        prevLinear = currentLinear;
        currentLinear = analog(LINEAR_PORT);

        if (warmup > 0) {
            warmup -= 1;
        } else {
            totalDelta -= deltaArray[pushIndex]; 
            deltaArray[pushIndex] = abs(prevLinear - currentLinear);
            totalDelta += deltaArray[pushIndex];
            pushIndex = (pushIndex + 1 ) % arraySize;
            if(totalDelta < 1 * arraySize){
                printf("=======Breaking======== %d \n", totalDelta);
                motor(PULLEY_PORT, -sign * power);
                msleep(arraySize);
                break;
            }
        }
        currentSign = getSign(currentLinear - target, tolerance);
    }
    motor(PULLEY_PORT, 0);
}

void interpolatePulley(int target){
    if (target < 200) {
        interpolatePulleyWithTolerance(100, 200, 0);
        motor(PULLEY_PORT, 100);
        msleep(350 * (((float)(200 - target)) / ((float)(200 - PULLEY_DOWN))));
        motor(PULLEY_PORT, 0);
    } else {
    	interpolatePulleyWithTolerance(100, target, 0);
    }
}
void lineFollowBackwards(int timeout, int speed, int delay){
    unsigned long endTime = systime() + timeout;
    unsigned long time = systime();
    while ((time = systime()) < endTime) {
        int tophatValueLeft = get_create_lfcliff_amt();
        int tophatValueRight = get_create_rfcliff_amt();
        float tophatLeft = (tophatValueLeft < THRESHOLD ? 0 : 1);
        float tophatRight = (tophatValueRight < THRESHOLD ? 0 : 1); 
        int currentSpeed = (tophatLeft == tophatRight ? speed : speed);
        create_drive_direct((int)(((float)currentSpeed) * tophatLeft), (int)(((float)currentSpeed) * tophatRight));
        msleep(delay);
    } 
    create_drive_direct(0, 0);
}

void driveToLine(int power, int adjustPower){
    int leftSign = getSign(THRESHOLD - get_create_lcliff_amt(), 0);
    int rightSign = getSign(THRESHOLD - get_create_rcliff_amt(), 0);
    while(!(leftSign == 1 && rightSign == 1)) {
        drive(leftSign * power, rightSign * power);
        msleep(1);
        leftSign = getSign(THRESHOLD - get_create_lcliff_amt(), 0);
        rightSign = getSign(THRESHOLD - get_create_rcliff_amt(), 0);
        if (leftSign == 1 || rightSign == 1) {
            power = adjustPower;
        }
    }
    drive(0, 0);
}

void driveToLineTopHat(int power, int adjustPower){
    int leftSign = getSign(TOPHAT_THRESHOLD - analog(TOP_HAT_LEFT), 0);
    int rightSign = getSign(TOPHAT_THRESHOLD - analog(TOP_HAT_RIGHT), 0);
    while(!(leftSign == -1 && rightSign == -1)) {
        drive(-1 * rightSign * power, -1 * leftSign * power);
        msleep(1);
        leftSign = getSign(TOPHAT_THRESHOLD - analog(TOP_HAT_LEFT), 0);
        rightSign = getSign(TOPHAT_THRESHOLD - analog(TOP_HAT_RIGHT), 0);
        if (leftSign == -1 || rightSign == -1) {
            power = adjustPower;
        }
    }
    drive(0, 0);
}


void driveToLineFront(int power, int adjustPower){
    int leftSign = getSign(THRESHOLD - get_create_lfcliff_amt(), 0);
    int rightSign = getSign(THRESHOLD - get_create_rfcliff_amt(), 0);
    while(!(leftSign == 1 && rightSign == 1)) {
        drive(leftSign * power, rightSign * power);
        msleep(1);
        leftSign = getSign(THRESHOLD - get_create_lfcliff_amt(), 0);
        rightSign = getSign(THRESHOLD - get_create_rfcliff_amt(), 0);
        if (leftSign == 1 || rightSign == 1){
            power = adjustPower;
        }
    }
    drive(0, 0);
}


//Board Functions------------------------------------------------------------------------------------------
void init(){ 
    create_connect(); 
    create_full();
    printf("Initial Pulley Position: %d\n", analog(LINEAR_PORT));
    interpolatePulley(PULLEY_START);
    set_servo_position(WRIST_PORT, get_servo_position(WRIST_PORT));
    set_servo_position(CLAW_PORT, get_servo_position(CLAW_PORT)); 
    enable_servos();
    if (get_servo_position(CLAW_PORT) != CLAW_STARTING) {
        interpolateServoByTime(CLAW_PORT, get_servo_position(CLAW_PORT), CLAW_STARTING, abs(get_servo_position(CLAW_PORT) - CLAW_STARTING));
    }
    if (get_servo_position(WRIST_PORT) != WRIST_STARTING) {
        interpolateServoByTime(WRIST_PORT, get_servo_position(WRIST_PORT), WRIST_STARTING, abs(get_servo_position(WRIST_PORT) - WRIST_STARTING));
    }
    msleep(500); //Put Wait For Light Here
}

void starting(){
    drive(0, 0);
    interpolateServoByTime(WRIST_PORT, WRIST_STARTING, WRIST_DOWN, 400);
    interpolateServoByTime(CLAW_PORT, CLAW_STARTING, CLAW_OPEN, 400); 
    interpolatePulley(PULLEY_DOWN); 
    drive(-200, -200);
    msleep(300);
    interpolateServoByTime(CLAW_PORT, CLAW_OPEN, CLAW_CLOSE_POMS, 200);
    motor(PULLEY_PORT, -100);
    msleep(400);
    motor(PULLEY_PORT, 0);
    driveToLine(200, 30);
    drive(-200, -200);
    msleep(290);
}

void getFirstPoms(){
    drive(0, 0);
    interpolatePulley(PULLEY_START); 
    drive(-300, 300); //Turn to line to get poms
    msleep(600);
    motor(PULLEY_PORT, 0);
    drive(-30, 30);
    while(get_create_lfcliff_amt() < THRESHOLD){ //Adjusts to line
        msleep(1);
    }
    drive(-30, 30);
    unsigned long time = systime();
    while(get_create_rfcliff_amt() > THRESHOLD){
        msleep(1);
    }
    time = systime() - time;
    drive(30, -30);
    msleep(time/2);
    drive(0, 0);
    //msleep(500);
    interpolatePulley(PULLEY_DOWN + 150);
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE_POMS, CLAW_EXTRA_WIDE, 400); //Drops first set of poms on second set
    interpolatePulley(PULLEY_DOWN);
    lineFollow(2000, 250, 20);
    drive(0, 0);
    interpolateServoByTime(CLAW_PORT, CLAW_EXTRA_WIDE, CLAW_CLOSE_POMS, 300); //drives and closes on poms. 
}

void tramDump(){
    motor(PULLEY_PORT, -100);
    lineFollow(1500, 250, 30);
    motor(PULLEY_PORT, 0);
    drive(-200, -200);
    while(get_create_lcliff_amt() > THRESHOLD){ //drives to middle line 
        msleep(1);
    }
    drive(0, 0);
    msleep(300);
    drive(250, 250); 
    msleep(250); //400
    drive(0, 0);
    motor(PULLEY_PORT, -100);
    interpolateServoByTime(WRIST_PORT, WRIST_UP, 120, 300); //Wrist up to down poms
    motor(PULLEY_PORT, 0);
    interpolatePulley(PULLEY_MAX - 50); 
    drive(-100, 100);
    msleep(1550);
    driveToLine(100, 30);
    drive(0, 0);
    msleep(500);
    drive(-50, -50);
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE_POMS, CLAW_WIDE, 700);
    drive(0, 0);
    //msleep(500);
    interpolateServoByTime(CLAW_PORT, CLAW_WIDE, CLAW_CLOSE_POMS, 500);
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE_POMS, CLAW_WIDE, 500);
    interpolateServoByTime(WRIST_PORT, 50, 400, 500); 
    interpolateServoByTime(WRIST_PORT, 400, WRIST_DROP, 500); //Dip

    drive(200, 200);
    msleep(100);
    drive(0, 0);

    driveToLine(-100, -20); //get back to the middle
    drive(300, 300);
    msleep(500);
    drive(200, 200);
    msleep(140);
    drive(0, 0);
}

void getFrisbee(){
    //turn
    drive(-250, 250);
    msleep(830);
    driveToLineFront(100, 30);
    //drive up to the tree
    //NOTE: this distance will probably change during testing
    drive(-250, -250);
    msleep(820); //900
    drive(0, 0);
    msleep(400); 

    //turn left to face tree
    drive(-300, 300);
    msleep(600); 
    drive(0, 0);

    //drive back a little bit for the line adjust
    drive(300, 300);
    msleep(500);
    drive(0, 0);

    //get frisbee
    driveToLine(200, 20);
    interpolatePulley(1300); //1600
    interpolateServoByTime(CLAW_PORT, CLAW_WIDE, CLAW_OPEN + 210, 500);
    interpolateServoByTime(WRIST_PORT, 50, 325, 250);
    drive(-250, -250); 
    interpolateServoByTime(WRIST_PORT, 325, 360, 500);
    drive(0, 0);
    interpolateServoByTime(CLAW_PORT, CLAW_OPEN + 210, CLAW_CLOSE_FRISBEE, 700);
    interpolatePulleyWithTolerance(50, 2000, 0);
    motor(PULLEY_PORT, -40); //20
    drive(-50, -50);
    msleep(750); //1500
    motor(PULLEY_PORT, 0);

    driveToLine(-100, -20);

    drive(-100, 100); 
    msleep(1500);
    drive(-100, -100);
    while(get_create_lcliff_amt() > THRESHOLD){ //drives to middle line 
        msleep(1);
    }
    drive(0, 0);
    drive(100, 100);
    msleep(600); //500
    drive(0, 0);
    interpolatePulley(PULLEY_MAX - 150);
    interpolateServoByTime(WRIST_PORT, 360, 500, 1000); //3: 550
    
    drive(-100, 100); //turns towards tram
    msleep(1570);
    drive(0, 0);
    interpolatePulley(PULLEY_MAX); 
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE_FRISBEE, 1500, 700); //added, #3: 1470
    driveToLine(100, 20);    
    interpolateServoByTime(WRIST_PORT, 500, 200, 1000);
    drive(-100, -100);
    msleep(200);
    drive(0, 0);
    interpolateServoByTime(WRIST_PORT, 200, 800, 500);
    interpolateServoByTime(CLAW_PORT, 1500, CLAW_OPEN, 700);
    msleep(500);
    interpolateServoByTime(WRIST_PORT, 800, WRIST_UP, 500);
    drive(300, 300); //200
    msleep(300); //1100
    drive(300, -300);
    msleep(600); 
    drive(0, 0);    
}

void getSecondPoms(){
    drive(0, 0);
    interpolateServoByTime(WRIST_PORT, WRIST_UP, WRIST_DOWN, 600); 
    interpolateServoByTime(CLAW_PORT, CLAW_OPEN, CLAW_WIDE, 600); 
    interpolatePulley(PULLEY_DOWN);  //moves everything to position to get poms
    drive(-200, -200);
    msleep(1000);
    lineFollow(2400, 250, 10);  //LINE FOLLOW 
    drive(0, 0); 
    interpolateServoByTime(CLAW_PORT, CLAW_EXTRA_WIDE, CLAW_CLOSE_POMS, 600);
    motor(PULLEY_PORT, -100);
    lineFollowBackwards(2000, 300, 20);
    motor(PULLEY_PORT, 0);
    drive(150, 150);
    while(get_create_lcliff_amt() > THRESHOLD){ //drives to middle line 
        msleep(1);
    }
    drive(200, 200);//added
    msleep(50);//added
    drive(200, 200);
    motor(PULLEY_PORT, -100);
    interpolateServoByTime(WRIST_PORT, WRIST_UP, 40, 250); //Wrist up to down poms, 200
    motor(PULLEY_PORT, 0);
    drive(0, 0);
    interpolatePulley(PULLEY_MAX - 100);
    drive(-200, 200); //Turns towards tram
    msleep(850); //900
    driveToLine(100, 30);
    drive(0, 0);
    msleep(500);
    drive(-50, -50);
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE_POMS, CLAW_WIDE, 700);
    drive(0, 0);
    //msleep(500);
    interpolateServoByTime(CLAW_PORT, CLAW_WIDE, CLAW_CLOSE_POMS, 500);
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE_POMS, CLAW_WIDE, 500);
    interpolateServoByTime(WRIST_PORT, 40, 400, 500); 
    interpolateServoByTime(WRIST_PORT, 400, WRIST_DROP, 500); //Dip
}

void tramMove(){
    drive(-150, -150); //Forward to tram to grab it
    msleep(500);
    drive(0, 0);
    interpolateServoByTime(WRIST_PORT, WRIST_DROP, 200, 600);
    interpolateServoByTime(CLAW_PORT, CLAW_WIDE, CLAW_CLOSE_POMS, 600); //Grabs tram
    driveToLine(-200, -20);
    drive(250, 250); //pulls tram back, hits cubes
    msleep(500); 	//1200
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE_POMS, CLAW_OPEN, 200);
    drive(150, 150);
    msleep(200);
    interpolateServoByTime(WRIST_PORT, 200, WRIST_UP, 200); 
    drive(-300, 300);
    msleep(600); 
    driveToLineFront(100, 20); //Drive to middle line
    drive(-200, -200); 
    msleep(500);	//650
    drive(300, -300); 
    msleep(600); 
    driveToLine(100, 40); //TESTING
    interpolatePulley(PULLEY_MAX);
    interpolateServoByTime(WRIST_PORT, WRIST_UP, 200, 600); 
    drive(100, -100); // Whacks the tram
    msleep(2300);
    drive(-250, 250);
    msleep(600);
    drive(0,0); 
    
    interpolatePulley(PULLEY_START+100);  
    interpolateServoByTime(CLAW_PORT, CLAW_OPEN, CLAW_CLOSE_POMS, 400); //Adjusts so the claw can go around pv
    interpolateServoByTime(WRIST_PORT, 200, WRIST_UP, 500);
    drive(-300, -300);
    msleep(975);		//1100 drive into black cog
    drive(0,0); 
    drive(250, -250);
    msleep(600);
    drive(0,0);
    interpolatePulley(PULLEY_MAX);
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE_POMS, CLAW_OPEN, 200); 
    drive(250, -250);
    msleep(400);
    drive(0, 0);
    msleep(500); 
    driveToLine(100, 40); //TESTING
    drive(-200, -200);
    msleep(650);
    interpolateServoByTime(WRIST_PORT, WRIST_UP, 250, 200);
}

void buttersBotguy(){
    drive(200, 200);
    msleep(500);
    driveToLine(-100, -40); //TESTING
    drive(250, 250);
    msleep(1100);
    drive(0, 0);
    interpolateServoByTime(WRIST_PORT, 250, WRIST_DOWN, 600); 
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE_POMS, CLAW_OPEN + 100, 500); 
    interpolatePulley(PULLEY_DOWN);
    driveToLine(100, 30); //use front sensors to drive to long line NOTE: HAS TO BE ALIGNED PERFECTLY
   // lineFollow(800, 100, 20);
    interpolateServoByTime(CLAW_PORT, CLAW_OPEN + 100, 1600, 600);
    drive(100, 100);
    msleep(700);
    drive(200, 200);
    msleep(500);
    drive(300, -300); //turns aggressivly and moves butters
    msleep(700); 
    drive(0, 0); 
    interpolateServoByTime(CLAW_PORT, 1600, 900, 600); 
    interpolatePulley(PULLEY_START - 50); 
    drive(-200, 200);
    msleep(1100); //turns back to face cage
   // driveToLine(-200, -40); //drives to zone line
    drive(0,0); 
    interpolatePulley(PULLEY_DOWN); //added
    interpolateServoByTime(CLAW_PORT, 900, CLAW_CLOSE_POMS, 500);
    drive(-200, -200); //drives forward
    msleep(300); //change //700
    drive(0, 0);
    motor(PULLEY_PORT, -100);
    msleep(200);
    motor(PULLEY_PORT, 0);
    driveToLine(100, 20);
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE_POMS, 1200, 500); 
    lineFollow(1500, 100, 20); //drive foward to get botguy
    drive(0,0); 
    interpolateServoByTime(CLAW_PORT, 1200, CLAW_CLOSE_POMS, 700); //grabs botguy
    drive(100, 100); 
    msleep(700);
    driveToLine(-100, -20); //drives back to long line 
    drive(200, 200); //drive back more 
    msleep(1300); 
    drive(0,0); 
    interpolatePulley(PULLEY_UP);
    driveToLine(100, 20); //drive back foward to line
    drive(0,0); 
    lineFollow(300, 200, 20); //line follows towards tram
    interpolatePulley(PULLEY_MAX); 
    interpolateServoByTime(WRIST_PORT, WRIST_DOWN, 900, 1200); //places botguy in tram
}


int main(){
   // wait_for_light(LIGHT_PORT);
    init();
    shut_down_in(119.5);
    int time = systime();
    printf("Create Battery %f%%\n", ((float)get_create_battery_charge()) / ((float)get_create_battery_capacity()) * 100.0);
    starting();
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s Started\n");
    
    getFirstPoms();
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s First Poms\n");
    
    tramDump();
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s Dumped\n");
    
    getFrisbee();
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s Frisbeed\n");
    
    getSecondPoms();
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s Second Poms\n"); 
    
    tramMove(); 
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s Tram successfully Shoved\n");
    
    buttersBotguy();
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s Butters & Botguy\n");
    msleep(10000);
    drive(0, 0);
    return 0;
}


