#include <kipr/botball.h>

#define RUN_SCRIPT create_write_byte(141) // macro to run the currently loaded script


int c2 = 36;
int cis2 = 37;
int des2 = 37;
int d2 = 38;
int dis2 = 39;
int ees2 = 39;
int e2 = 40;
int f2 = 41;
int fis2 = 42;
int ges2 = 42;
int g2 = 43;
int a2 = 45;
int ais2 = 46;
int bes2 = 46;
int b2 = 47;
int c3 = 48;
int cis3 = 49;
int des3 = 49;
int d3 = 50;
int dis3 = 51;
int ees3 = 51;
int e3 = 52;
int f3 = 53;
int fis3 = 54;
int ges3 = 54;
int g3 = 55;
int gis3 = 56;
int aes3 = 56;
int a3 = 57;
int ais3 = 58;
int bes3 = 58;
int b3 = 59;
int c4 = 60;
int cis4 = 61;
int des4 = 61;
int d4 = 62;
int dis4 = 63;
int ees4 = 63;
int e4 = 64;
int f4 = 65;
int fis4 = 66;
int ges4 = 66;
int g4 = 67;
int gis4 = 68;
int aes4 = 68;
int a4 = 69;
int ais4 = 70;
int bes4 = 70;
int b4 = 71;
int c5 = 72;
int cis5 = 73;
int des5 = 73;
int d5 = 74;
int dis5 = 75;
int ees5 = 75;
int e5 = 76;
int f5 = 77;
int fis5 = 78;
int ges5 = 78;
int g5 = 79;
int gis5 = 80;
int aes5 = 80;
int a5 = 81;
int ais5 = 82;
int bes5 = 82;
int b5 = 83;
int c6 = 84;
int cis6 = 85;
int des6 = 85;
int d6 = 86;
int dis6 = 87;
int ees6 = 87;
int e6 = 88;
int f6 = 89;
int fis6 = 90;
int ges6 = 90;
int g6 = 91;
int gis6 = 92;
int aes6 = 92;
int a6 = 93;
int ais6 = 94;
int bes6 = 94;
int b6 = 95;
int c7 = 96;
int cis7 = 97;
int des7 = 97;
int d7 = 98;
int dis7 = 99;
int ees7 = 99;
int e7 = 100;
int f7 = 101;
int fis7 = 102;
int ges7 = 102;
int g7 = 103;
int gis7 = 104;
int aes7 = 104;
int a7 = 105;
int ais7 = 106;
int bes7 = 106;
int b7 = 107;


//define some note lengths
//change the top MEASURE (4/4 time) to get faster/slower speeds
int MEASURE = 160;
#define HALF  MEASURE/2
#define Q  MEASURE/4
#define Qd  MEASURE*5/16
#define E  MEASURE/8
#define Ed  MEASURE*3/16
#define S  MEASURE/16
#define R 30

void imperial_march_one(){
    create_write_byte(140); 
    create_write_byte(0);
    create_write_byte(9);
    create_write_byte(69);
    create_write_byte(40);
    create_write_byte(69);
    create_write_byte(40);
    create_write_byte(69);
    create_write_byte(40);
    create_write_byte(65);
    create_write_byte(30);
    create_write_byte(72);
    create_write_byte(10);
    create_write_byte(69);
    create_write_byte(40);
    create_write_byte(65);
    create_write_byte(30);
    create_write_byte(72);
    create_write_byte(10);
    create_write_byte(69);
    create_write_byte(80);
}

void imperial_march_two(){
    create_write_byte(140); 
    create_write_byte(1);
    create_write_byte(9);
    create_write_byte(76);
    create_write_byte(40);
    create_write_byte(76);
    create_write_byte(40);
    create_write_byte(76);
    create_write_byte(40);
    create_write_byte(77);
    create_write_byte(30);
    create_write_byte(72);
    create_write_byte(10);
    create_write_byte(68);
    create_write_byte(40);
    create_write_byte(65);
    create_write_byte(30);
    create_write_byte(72);
    create_write_byte(10);
    create_write_byte(69);
    create_write_byte(80);
}

void imperial_march_three(){
    create_write_byte(140); 
    create_write_byte(2);
    create_write_byte(9);
    create_write_byte(81);
    create_write_byte(40);
    create_write_byte(69);
    create_write_byte(30);
    create_write_byte(69);
    create_write_byte(10);
    create_write_byte(81);
    create_write_byte(40);
    create_write_byte(80);
    create_write_byte(20);
    create_write_byte(79);
    create_write_byte(20);
    create_write_byte(78);
    create_write_byte(10);
    create_write_byte(77);
    create_write_byte(10);
    create_write_byte(78);
    create_write_byte(10);
}

void imperial_march_four(){
    create_write_byte(140); 
    create_write_byte(3);
    create_write_byte(7);
    create_write_byte(70);
    create_write_byte(20);
    create_write_byte(75);
    create_write_byte(40);
    create_write_byte(74);
    create_write_byte(20);
    create_write_byte(73);
    create_write_byte(20);
    create_write_byte(72);
    create_write_byte(10);
    create_write_byte(71);
    create_write_byte(10);
    create_write_byte(72);
    create_write_byte(20);
}

void imperial_march_five(){
    create_write_byte(140); 
    create_write_byte(0);
    create_write_byte(8);
    create_write_byte(65);
    create_write_byte(20);
    create_write_byte(68);
    create_write_byte(40);
    create_write_byte(65);
    create_write_byte(30);
    create_write_byte(68);
    create_write_byte(10);
    create_write_byte(72);
    create_write_byte(40);
    create_write_byte(69);
    create_write_byte(30);
    create_write_byte(72);
    create_write_byte(10);
    create_write_byte(76);
    create_write_byte(80);
}

void song(){
    imperial_march_one();
    msleep(300); // give serial connection some time
    RUN_SCRIPT;
    create_write_byte(0); //do I need these?
    msleep(4700); // allow time for the script to finish (+ some extra)
    
    imperial_march_two();
    msleep(300);
    RUN_SCRIPT;
    create_write_byte(1); //do I need these?
    msleep(4700); // allow time for the script to finish (+ some extra)

    imperial_march_three();
    msleep(400);
    RUN_SCRIPT;
    create_write_byte(2); //do I need these?
    msleep(3000); // allow time for the script to finish (+ some extra)

    imperial_march_four();
    msleep(485);
    RUN_SCRIPT;
    create_write_byte(3); //do I need these?
    msleep(2150); // allow time for the script to finish (+ some extra)

    imperial_march_five();
    msleep(385);
    RUN_SCRIPT;
    create_write_byte(0); //do I need these?
    msleep(3200); // allow time for the script to finish (+ some extra)
}

void mountain_king_one(){
    create_write_byte(140); 
    create_write_byte(0);
    create_write_byte(13);
    create_write_byte(a3);
    create_write_byte(E);
    create_write_byte(b3);
    create_write_byte(E);
    create_write_byte(c4);
    create_write_byte(E);
    create_write_byte(d4);
    create_write_byte(E);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(c4);
    create_write_byte(E);
    create_write_byte(e4);
    create_write_byte(Q);
    create_write_byte(dis4);
    create_write_byte(E);
    create_write_byte(b3);
    create_write_byte(E);
    create_write_byte(dis4);
    create_write_byte(Q);
    create_write_byte(d4);
    create_write_byte(E);
    create_write_byte(b3);
    create_write_byte(E);
    create_write_byte(d4);
    create_write_byte(Q);
}

void mountain_king_two(){
    create_write_byte(140); 
    create_write_byte(1);
    create_write_byte(13);
    create_write_byte(a3);
    create_write_byte(E);
    create_write_byte(b3);
    create_write_byte(E);
    create_write_byte(c4);
    create_write_byte(E);
    create_write_byte(d4);
    create_write_byte(E);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(c4);
    create_write_byte(E);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(a4);
    create_write_byte(E);
    create_write_byte(g4);
    create_write_byte(E);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(c4);
    create_write_byte(E);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(g4);
    create_write_byte(HALF);
}

void mountain_king_three(){
    create_write_byte(140); 
    create_write_byte(2);
    create_write_byte(13);
    create_write_byte(a4);
    create_write_byte(E*4/5);
    create_write_byte(b4);
    create_write_byte(E*4/5);
    create_write_byte(c5);
    create_write_byte(E*4/5);
    create_write_byte(d5);
    create_write_byte(E*4/5);
    create_write_byte(e5);
    create_write_byte(E*4/5);
    create_write_byte(c5);
    create_write_byte(E*4/5);
    create_write_byte(e5);
    create_write_byte(Q*4/5);
    create_write_byte(dis5);
    create_write_byte(E*4/5);
    create_write_byte(b4);
    create_write_byte(E*4/5);
    create_write_byte(dis5);
    create_write_byte(Q*4/5);
    create_write_byte(d5);
    create_write_byte(E*4/5);
    create_write_byte(b4);
    create_write_byte(E*4/5);
    create_write_byte(d5);
    create_write_byte(Q*4/5);
}

void mountain_king_four(){
    create_write_byte(140); 
    create_write_byte(3);
    create_write_byte(13);
    create_write_byte(a4);
    create_write_byte(E*4/5);
    create_write_byte(b4);
    create_write_byte(E*4/5);
    create_write_byte(c5);
    create_write_byte(E*4/5);
    create_write_byte(d5);
    create_write_byte(E*4/5);
    create_write_byte(e5);
    create_write_byte(E*4/5);
    create_write_byte(c5);
    create_write_byte(E*4/5);
    create_write_byte(e5);
    create_write_byte(E*4/5);
    create_write_byte(a5);
    create_write_byte(E*4/5);
    create_write_byte(g5);
    create_write_byte(E*4/5);
    create_write_byte(e5);
    create_write_byte(E*4/5);
    create_write_byte(c5);
    create_write_byte(E*4/5);
    create_write_byte(e5);
    create_write_byte(E*4/5);
    create_write_byte(g5);
    create_write_byte(HALF*4/5);
}

void mountain_king_five(){
    create_write_byte(140); 
    create_write_byte(0);
    create_write_byte(13);
    create_write_byte(a5);
    create_write_byte(E*3/5);
    create_write_byte(b5);
    create_write_byte(E*3/5);
    create_write_byte(c6);
    create_write_byte(E*3/5);
    create_write_byte(d6);
    create_write_byte(E*3/5);
    create_write_byte(e6);
    create_write_byte(E*3/5);
    create_write_byte(c6);
    create_write_byte(E*3/5);
    create_write_byte(e6);
    create_write_byte(Q*3/5);
    create_write_byte(dis6);
    create_write_byte(E*3/5);
    create_write_byte(b5);
    create_write_byte(E*3/5);
    create_write_byte(dis6);
    create_write_byte(Q*3/5);
    create_write_byte(d6);
    create_write_byte(E*3/5);
    create_write_byte(b5);
    create_write_byte(E*3/5);
    create_write_byte(d6);
    create_write_byte(Q*3/5);
}

void mountain_king_six(){
    create_write_byte(140); 
    create_write_byte(1);
    create_write_byte(13);
    create_write_byte(a5);
    create_write_byte(E*3/5);
    create_write_byte(b5);
    create_write_byte(E*3/5);
    create_write_byte(c6);
    create_write_byte(E*3/5);
    create_write_byte(d6);
    create_write_byte(E*3/5);
    create_write_byte(e6);
    create_write_byte(E*3/5);
    create_write_byte(c6);
    create_write_byte(E*3/5);
    create_write_byte(e6);
    create_write_byte(E*3/5);
    create_write_byte(a6);
    create_write_byte(E*3/5);
    create_write_byte(g6);
    create_write_byte(E*3/5);
    create_write_byte(e6);
    create_write_byte(E*3/5);
    create_write_byte(c6);
    create_write_byte(E*3/5);
    create_write_byte(e6);
    create_write_byte(E*3/5);
    create_write_byte(g6);
    create_write_byte(HALF*3/5);
}

void mountain_king_seven(){
    create_write_byte(140); 
    create_write_byte(2);
    create_write_byte(13);
    create_write_byte(a6);
    create_write_byte(E/2);
    create_write_byte(b6);
    create_write_byte(E/2);
    create_write_byte(c7);
    create_write_byte(E/2);
    create_write_byte(d7);
    create_write_byte(E/2);
    create_write_byte(e7);
    create_write_byte(E/2);
    create_write_byte(c7);
    create_write_byte(E/2);
    create_write_byte(e7);
    create_write_byte(Q/2);
    create_write_byte(dis7);
    create_write_byte(E/2);
    create_write_byte(b6);
    create_write_byte(E/2);
    create_write_byte(dis7);
    create_write_byte(Q/2);
    create_write_byte(d7);
    create_write_byte(E/2);
    create_write_byte(b6);
    create_write_byte(E/2);
    create_write_byte(d7);
    create_write_byte(Q/2);
}

void mountain_king_eight(){
    create_write_byte(140); 
    create_write_byte(3);
    create_write_byte(13);
    create_write_byte(a6);
    create_write_byte(E/2);
    create_write_byte(b6);
    create_write_byte(E/2);
    create_write_byte(c7);
    create_write_byte(E/2);
    create_write_byte(d7);
    create_write_byte(E/2);
    create_write_byte(e7);
    create_write_byte(E/2);
    create_write_byte(c7);
    create_write_byte(E/2);
    create_write_byte(e7);
    create_write_byte(E/2);
    create_write_byte(a7);
    create_write_byte(E/2);
    create_write_byte(g7);
    create_write_byte(E/2);
    create_write_byte(e7);
    create_write_byte(E/2);
    create_write_byte(c7);
    create_write_byte(E/2);
    create_write_byte(e7);
    create_write_byte(E/2);
    create_write_byte(g7);
    create_write_byte(HALF/2);
}

void mountain_king(){
    mountain_king_one();
    msleep(300); // give serial connection some time
    RUN_SCRIPT;
    create_write_byte(0); //do I need these?
    msleep(MEASURE*2*1000/64+10);
    
    mountain_king_two();
    RUN_SCRIPT;
    create_write_byte(1); //do I need these?
    msleep(MEASURE*2*1000/64); // allow time for the script to finish (+ some extra)
    
    mountain_king_three();
    RUN_SCRIPT;
    create_write_byte(2); //do I need these?
    msleep(MEASURE*2*1000*4/5/64+10); // allow time for the script to finish (+ some extra)
    
    mountain_king_four();
    RUN_SCRIPT;
    create_write_byte(3); //do I need these?
    msleep(MEASURE*2*1000*4/5/64); // allow time for the script to finish (+ some extra)
    
    mountain_king_five();
    RUN_SCRIPT;
    create_write_byte(0); //do I need these?
    msleep(MEASURE*2*1000*3/5/64+10); // allow time for the script to finish (+ some extra)
    
    mountain_king_six();
    RUN_SCRIPT;
    create_write_byte(1); //do I need these?
    msleep(MEASURE*2*1000*3/5/64); // allow time for the script to finish (+ some extra)
    
    mountain_king_seven();
    RUN_SCRIPT;
    create_write_byte(2); //do I need these?
    msleep(MEASURE*2*1000/2/64+10); // allow time for the script to finish (+ some extra)
    
    mountain_king_eight();
    RUN_SCRIPT;
    create_write_byte(3); //do I need these?
    msleep(MEASURE*2*1000/2/64); // allow time for the script to finish (+ some extra)
}
    

void setup(){
    printf("WALLABY BATTERY CHARGE: %f%%\n", 100*(double)power_level());
    create_connect();
    create_full();
    printf("CREATE BATTERY CHARGE: %f%%\n", (float)get_create_battery_charge() / (float)get_create_battery_capacity());
    thread threadId = thread_create(mountain_king);
    thread_start(threadId);
}

int main(){
    setup();
    printf("IMPERIAL MARCH\n");
    
    msleep(60000);
    

    create_disconnect();
    return 0;
}
