#include <kipr/botball.h>
#define RUN_SCRIPT create_write_byte(141) //macro to run the currently loaded script

//servos
#define PRIMARY 3
#define SECONDARY 2

//motors
#define RIGHT 1 //red on the bottom
#define LEFT 0

//servo positions
#define PRIMARYOUT 2000
#define SECONDARYIN 2000
#define PRIMARYIN 50
#define SECONDARYOUT 50

#define LIGHT_SENSOR 0

//sensors
#define LEFT_TOPHAT 1
#define RIGHT_TOPHAT 0

//header methods

int c2 = 36;
int cis2 = 37;
int des2 = 37;
int d2 = 38;
int dis2 = 39;
int ees2 = 39;
int e2 = 40;
int f2 = 41;
int fis2 = 42;
int ges2 = 42;
int g2 = 43;
int a2 = 45;
int ais2 = 46;
int bes2 = 46;
int b2 = 47;
int c3 = 48;
int cis3 = 49;
int des3 = 49;
int d3 = 50;
int dis3 = 51;
int ees3 = 51;
int e3 = 52;
int f3 = 53;
int fis3 = 54;
int ges3 = 54;
int g3 = 55;
int gis3 = 56;
int aes3 = 56;
int a3 = 57;
int ais3 = 58;
int bes3 = 58;
int b3 = 59;
int c4 = 60;
int cis4 = 61;
int des4 = 61;
int d4 = 62;
int dis4 = 63;
int ees4 = 63;
int e4 = 64;
int f4 = 65;
int fis4 = 66;
int ges4 = 66;
int g4 = 67;
int gis4 = 68;
int aes4 = 68;
int a4 = 69;
int ais4 = 70;
int bes4 = 70;
int b4 = 71;
int c5 = 72;
int cis5 = 73;
int des5 = 73;
int d5 = 74;
int dis5 = 75;
int ees5 = 75;
int e5 = 76;
int f5 = 77;
int fis5 = 78;
int ges5 = 78;
int g5 = 79;
int gis5 = 80;
int aes5 = 80;
int a5 = 81;
int ais5 = 82;
int bes5 = 82;
int b5 = 83;
int c6 = 84;
int cis6 = 85;
int des6 = 85;
int d6 = 86;
int dis6 = 87;
int ees6 = 87;
int e6 = 88;
int f6 = 89;
int fis6 = 90;
int ges6 = 90;
int g6 = 91;
int gis6 = 92;
int aes6 = 92;
int a6 = 93;
int ais6 = 94;
int bes6 = 94;
int b6 = 95;
int c7 = 96;
int cis7 = 97;
int des7 = 97;
int d7 = 98;
int dis7 = 99;
int ees7 = 99;
int e7 = 100;
int f7 = 101;
int fis7 = 102;
int ges7 = 102;
int g7 = 103;
int gis7 = 104;
int aes7 = 104;
int a7 = 105;
int ais7 = 106;
int bes7 = 106;
int b7 = 107;

//define some note lengths
//change the top MEASURE (4/4 time) to get faster/slower speeds
int MEASURE = 70;
#define HALF  MEASURE/4
#define Q  MEASURE/8
#define Qd  MEASURE*5/32
#define E  MEASURE/16
#define Ed  MEASURE*3/32
#define S  MEASURE/32
#define R 30

void negativeOne(){
    create_write_byte(140);    //things command specifies that the following commands are songs
    create_write_byte(2);        //the song number, identifies which song (0-3) will be queued up
    create_write_byte(3);
    create_write_byte(e4);
    create_write_byte(HALF);
    create_write_byte(e4);
    create_write_byte(Q);
    create_write_byte(R);
    create_write_byte(9); //hard coded 3/4 measure
}

void zero(){
	create_write_byte(140);    //things command specifies that the following commands are songs
    create_write_byte(0);        //the song number, identifies which song (0-3) will be queued up
    create_write_byte(4);
    create_write_byte(e4);
    create_write_byte(HALF);
    create_write_byte(e4);
    create_write_byte(Q);
    create_write_byte(R);
    create_write_byte(9); //hard coded 3/4 measure
    create_write_byte(bes5);
    create_write_byte(30);
}

void first(){
    create_write_byte(140);    //things command specifies that the following commands are songs
    create_write_byte(0);        //the song number, identifies which song (0-3) will be queued up
    create_write_byte(16);    //specifies the number of notes in the song
	create_write_byte(e5);
    create_write_byte(HALF);
    create_write_byte(e5);
    create_write_byte(Q);
    create_write_byte(e5);
    create_write_byte(HALF);
    create_write_byte(d5);
    create_write_byte(Q);
    create_write_byte(e5);
    create_write_byte(Q);
    create_write_byte(fis5);
    create_write_byte(Q);
    create_write_byte(g5);
    create_write_byte(Q);
    create_write_byte(fis5);
    create_write_byte(Q);
    create_write_byte(e5);
    create_write_byte(Q);
    create_write_byte(d5);
    create_write_byte(Q);
    
    create_write_byte(b5);
    create_write_byte(Q);
    create_write_byte(a5);
    create_write_byte(Q);
    create_write_byte(g5);
    create_write_byte(Q);
    create_write_byte(a5);
    create_write_byte(Q);
    create_write_byte(g5);
    create_write_byte(Q);
    create_write_byte(fis5);
    create_write_byte(Q);
}

void second(){
	create_write_byte(140);    //things command specifies that the following commands are songs
    create_write_byte(1);        //the song number, identifies which song (0-3) will be queued up
    create_write_byte(16);    //specifies the number of notes in the song
	create_write_byte(g5);
    create_write_byte(Q);
    create_write_byte(fis5);
    create_write_byte(Q);
    create_write_byte(d5);
    create_write_byte(Q);
    create_write_byte(e5);
    create_write_byte(HALF);
    create_write_byte(d5);
    create_write_byte(Q);
    
    create_write_byte(e5);
    create_write_byte(HALF);
    create_write_byte(e5);
    create_write_byte(Q);
    create_write_byte(e5);
    create_write_byte(HALF);
    create_write_byte(d5);
    create_write_byte(Q);
    create_write_byte(e5);
    create_write_byte(Q);
    create_write_byte(fis5);
    create_write_byte(Q);
    create_write_byte(g5);
    create_write_byte(Q);
    create_write_byte(fis5);
    create_write_byte(Q);
    create_write_byte(e5);
    create_write_byte(Q);
    create_write_byte(d5);
    create_write_byte(Q); 
    
    create_write_byte(e5);
    create_write_byte(HALF); 
}

void third(){
    create_write_byte(140);    //things command specifies that the following commands are songs
    create_write_byte(2);        //the song number, identifies which song (0-3) will be queued up
    create_write_byte(16);    //specifies the number of notes in the song
	create_write_byte(d5);
    create_write_byte(Q);
    create_write_byte(e5);
    create_write_byte(HALF);
    create_write_byte(d5);
    create_write_byte(Q);
    create_write_byte(e5);
    create_write_byte(Q);
    create_write_byte(fis5);
    create_write_byte(Q);
    create_write_byte(g5);
    create_write_byte(Q);
    create_write_byte(b5);
    create_write_byte(HALF);
    create_write_byte(R); //8
    create_write_byte(Q);
    
    create_write_byte(e5);
    create_write_byte(HALF);
    create_write_byte(e5);
    create_write_byte(Q);
    create_write_byte(e5);
    create_write_byte(HALF);
    create_write_byte(d5);
    create_write_byte(Q);
    create_write_byte(e5);
    create_write_byte(Q);
    create_write_byte(fis5);
    create_write_byte(Q);
    create_write_byte(g5);
    create_write_byte(Q);
    create_write_byte(fis5); 
    create_write_byte(Q);  
}

void fourth(){
    create_write_byte(140);    //things command specifies that the following commands are songs
    create_write_byte(0);        //the song number, identifies which song (0-3) will be queued up
    create_write_byte(12); 
    create_write_byte(e5);
    create_write_byte(Q);
    create_write_byte(d5);
    create_write_byte(Q);
    
    create_write_byte(b5);
    create_write_byte(Q);
    create_write_byte(a5);
    create_write_byte(Q);
    create_write_byte(g5);
    create_write_byte(Q);
    create_write_byte(a5);
    create_write_byte(Q);
    create_write_byte(g5);
    create_write_byte(Q);
    create_write_byte(fis5);
    create_write_byte(Q); 
    create_write_byte(g5);
    create_write_byte(Q);
    create_write_byte(fis5);
    create_write_byte(Q);
    create_write_byte(d5);
    create_write_byte(Q); 
    create_write_byte(e5);
    create_write_byte(HALF); 
    
    
}

void shipping(){
    int i = 0;
    while (i < 4){
        first();
        RUN_SCRIPT;
        msleep(300); // give serial connection some time
        create_write_byte(0); //do I need these?
        msleep(MEASURE*1.84*1000/64 - 13);

        second();
        RUN_SCRIPT;
        msleep(300);
        create_write_byte(1); //do I need these?
        msleep(MEASURE*1.84*1000/64 + 275); // allow time for the script to finish (+ some extra)

        third();
        RUN_SCRIPT;
        msleep(300);
        create_write_byte(2); //do I need these?
        msleep(MEASURE*1.84*1000/64 + 260); // allow time for the script to finish (+ some extra)

        fourth();
        RUN_SCRIPT;
        msleep(300);
        create_write_byte(0);
        msleep(MEASURE*1.84*1000/64 - 250); // allow time for the script to finish (+ some extra)
        i++;
    }
	
}

void tDrive(int left, int right) {
    motor(LEFT, -1*left);
    motor(RIGHT, 0.93*right);  
}

void cDrive(int lpower, int rpower){
    create_drive_direct(-1*rpower, -1*lpower);
}

int interpolateServoPosition(int start, int end, double percentage) {
    return start + (end - start) * percentage;
}

void interpolateServo(int port, int start, int end, int time) {
    unsigned long endTime = systime() + time;
    while (systime() < endTime) {
        set_servo_position(port, interpolateServoPosition(start, end,
            1.0 - (((double)(endTime - systime())) / ((double)time))));
        msleep(1);
    }
    set_servo_position(port, end);
}

void interpolateServos(int port, int start, int end,
                       int port2, int start2, int end2, int time) {
    unsigned long endTime = systime() + time;
    while (systime() < endTime) {
        set_servo_position(port, interpolateServoPosition(start, end, 
            1.0 - (((double)(endTime - systime())) / ((double)time))));
        set_servo_position(port2, interpolateServoPosition(start2, end2,
            1.0 - (((double)(endTime - systime())) / ((double)time))));
        msleep(1);
    }
    set_servo_position(port, end);
    set_servo_position(port2, end2);
}

void setup() {
    create_connect();
    create_full();
    //enable_servos();
    //set_servo_position(PRIMARY, PRIMARYIN);
    //set_servo_position(SECONDARY, SECONDARYIN);
}

void driveToLine(int dir){
    int threshold = 2000;
    int ldir = dir;
    int rdir = dir;
    while(!(analog(RIGHT_TOPHAT) > threshold && analog(LEFT_TOPHAT) > threshold)){
        tDrive(ldir, rdir);
        if(analog(RIGHT_TOPHAT) > threshold){
            rdir = 0; 
        }
        if(analog(LEFT_TOPHAT) > threshold){
            ldir = 0;   
        }
        msleep(1);
    }
    tDrive(0, 0);
    msleep(200);
}

void lineFollow(int time, int speed){
    int counter = 0;
    //int leading = 0;
    //int lagging = 0;
    while (counter <= time){
        int right = speed;
        int left = speed;
        
        if (analog(RIGHT_TOPHAT) < 2000){
            left *= -1;
            //leading = RIGHT_TOPHAT;
            //lagging = LEFT_TOPHAT;
        }
        if (analog(LEFT_TOPHAT) < 2000){
            right *= -1;
            //leading = RIGHT_TOPHAT;
            //lagging = LEFT_TOPHAT;
        }
        tDrive(left, right);
        msleep(1);
        counter++;
        printf("Left: %d", analog(LEFT_TOPHAT));
        printf(" Right: %d \n", analog(RIGHT_TOPHAT));
    }
    tDrive(0, 0);
}


int lineFollow2(int timeout, int speed, int delay){
    unsigned long endTime = systime() + timeout;
    unsigned long time = systime();
    int skipped = 0;
    while ((time = systime()) < endTime) {
        int tophatValueLeft = analog(LEFT_TOPHAT);
        int tophatValueRight = analog(RIGHT_TOPHAT);
        //printf("%d %d -> ", tophatValueLeft, tophatValueRight);
        int tophatLeft = (tophatValueLeft < 2000 ? 1 : -1);
        int tophatRight = (tophatValueRight < 2000 ? 1 : -1);

        tDrive(speed * tophatLeft, speed * tophatRight);
        msleep(delay);
        if (tophatLeft != tophatRight) {
            unsigned long delta = systime() - time;
            endTime += delta;
            skipped += delta;
        }
    }
    tDrive(0, 0);
    return skipped;
}


void battery(){
    create_connect();
    create_full();
    float value = 100.0 *(float)get_create_battery_charge() / (float) get_create_battery_capacity();
    printf("Create Charge: %f", value);
    printf("%% \n");
}

int parallelToLine(){
    int counter = 0;
    int l = 0;
    if (analog(LEFT_TOPHAT) > 2000 && analog(RIGHT_TOPHAT) > 2000){
        tDrive(100, 100);
        msleep(300);
        printf("YUH");
        return 8;
    }
    if (analog(LEFT_TOPHAT) < 2000 && analog(RIGHT_TOPHAT) > 2000){
        while (analog(LEFT_TOPHAT) < 2000){
            l = 1;
            tDrive(80, 0);
            msleep(1);
            counter++;
        }
    }
    if (analog(LEFT_TOPHAT) > 2000 && analog(RIGHT_TOPHAT) < 2000){
        while (analog(RIGHT_TOPHAT) < 2000){
            tDrive(0, 80);
            msleep(1);
            counter++;
        }
    }
    
    if (l){
        tDrive(0, 100);
        msleep(counter + 50);
        printf("EATL");
    }
    else{
        tDrive(100, 0);
        msleep(counter + 105);
        printf("EATR");
    }
    msleep(100);
    tDrive(0, 0);
    return 0;
}

int mainBattery(){ //batteryMain
    //tDrive(100, 100);
    //msleep(4000);
    battery();
    return 0;
}

void deployArm() {
    cDrive(0, 0);
    cDrive(200, 0);
    interpolateServo(PRIMARY, PRIMARYIN,  PRIMARYIN/4, 150);
    cDrive(0, 0);
    interpolateServo(PRIMARY, PRIMARYIN/4,  PRIMARYOUT, 450);
    msleep(500);
    interpolateServo(SECONDARY, SECONDARYIN, SECONDARYOUT, 100);
    cDrive(100, 100);
    msleep(300);
    cDrive(0, 0);
    
    /*interpolateServo(PRIMARY_PORT, PRIMARY_IN, PRIMARY_OUT, 1000);
    msleep(5000);
    interpolateServo(SECONDARY_PORT, SECONDARY_IN, SECONDARY_OUT, 100);
    */
}

void dickWave(int iterations){
    int i = 0;
    while (i < iterations){
        cDrive(-200, 200);
        msleep(400);
        cDrive(0, 0);

        //turn back
        cDrive(200, -200);
        msleep(400);
        cDrive(0, 0);
        i++;
    }

}

int main(){ //testing the arm again
    setup();
    printf("CREATE BATTERY CHARGE: %f%%\n", (float)get_create_battery_charge() / (float)get_create_battery_capacity());
    wait_for_light(LIGHT_SENSOR);
    shut_down_in(119);
    cDrive(500, 500);
    msleep(3000);
    cDrive(0, 0);
    
    //adjust
    cDrive(-400, 0);
    msleep(350);
    cDrive(0, 0);
    
    enable_servos();
    set_servo_position(PRIMARY, PRIMARYIN);
    set_servo_position(SECONDARY, SECONDARYIN);
    
    //deploy arm
    cDrive(0, 0);
    cDrive(200, 0);
    interpolateServo(PRIMARY, PRIMARYIN,  PRIMARYIN/4, 150);
    cDrive(0, 0);
    interpolateServo(PRIMARY, PRIMARYIN/4,  PRIMARYOUT, 450);
    //cDrive(0, -400);
    //msleep(100)
    //cDrive(0, 0);
    msleep(500);
    interpolateServo(SECONDARY, SECONDARYIN, SECONDARYOUT, 100);
    cDrive(100, 100);
    msleep(300);
    cDrive(0, 0);
    
    //wait for arm to finish up
    msleep(1000);
    
    //drive back
    cDrive(-500, -500);
    msleep(250);
    cDrive(0, 0);
    
    set_servo_position(SECONDARY, 250);
    
    dickWave(50);
    
    cDrive(500, 500);
    //cDrive(-500, 500);
    msleep(350);
    cDrive(0, 0);
    
    set_servo_position(SECONDARY, 25);
    
    return 0;
    
    
    
    //dickWave(1); //sorry Hillsdale
    
    interpolateServo(SECONDARY, SECONDARYIN, SECONDARYOUT, 100);
    
    //drive forward again
    cDrive(500, 500);
    msleep(350);
    cDrive(0, 0);
    
    //put arm back down
    set_servo_position(SECONDARY, 50);
    msleep(50);
    
    //turn right and then drive back
    cDrive(500, -500);
    msleep(200);
    cDrive(0, 0);
    msleep(1000);
    cDrive(-500, -500);
    msleep(200);
    cDrive(0, 0);
    
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    
    //shipping();
    
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    printf("ESSKKKETITTTTT\n");
    
    return 0;
}