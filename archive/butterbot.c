#include <kipr/botball.h>
/*
    CREATE BUTTERBOT FULL RUN
    60% BATTERY IS THE OPTIMAL RUN
*/

//servos
#define SHORTARM 1
#define LONGARM 0
#define SHIFTER 2
#define LONGARMCLAW 3

//motors
#define SHORTCLAW 3
#define DUMPER 0

//servos positions
#define DUMPER_DOWN 1700
#define DUMPER_MIDWAY 680
#define DUMPER_UP 360
#define SHORTARM_DOWN 1950
#define SHORTARM_MIDDLE 1500
#define SHORTARM_UPMORE 77
#define SHORTARM_UP 120 //90
#define SHORTARM_SPLIT 565
#define SHIFTER_NEUTRAL 750
#define SHIFTER_RIP 1500
#define SHIFTER_IN 200
#define SHIFTER_MID 550

#define LONGARMCLAW_OPEN 0
#define LONGARMCLAW_CLOSE 1750
#define SHORTCLAWCLOSE 1270
#define SHORTCLAWOPEN 300

#define LONGARM_UP 930
#define LONGARM_START 300
#define LONGARM_DEPLOY 1800

//Sensors
#define SHORTCLAWBUTTON 0
#define LEFT_TOPHAT 4
#define RIGHT_TOPHAT 1 //1
#define LONGARMBUTTON 9

//Constants

void short_arm_grab();
void short_claw_close();
void long_arm_lower();
int third_cube_turn();
void cube_adjust();
void cube_adjust_ii();

void dump_seven_stack();
void caress_ii();
void grab_long_arm_cubes();

void stop(){
    motor(SHORTCLAW, 0);
    motor(LONGARM, 0);
    create_drive_direct(0, 0);
}

void moveMotorFor(int port, int power, int time){
    motor(port, power);
    msleep(time);
    motor(port, 0);
}

void move_long_arm(int power){
    while (digital(LONGARMBUTTON) != 1){
        motor(LONGARM, power);
    }
}

void back_until_bump()
{
  while(get_create_lbump() != 1 || get_create_rbump() != 1){
    create_drive_direct(300, 300);
  }
  stop();
}

void back_until_bump_ii()
{
  while(get_create_lbump() != 1 && get_create_rbump() != 1){
    create_drive_direct(300, 300);
  }
  stop();
}

void drive(int Lpower, int Rpower){
    create_drive_direct(-1 * Rpower, -1 * Lpower);
}

void driveTime(int Lpower, int Rpower, int time){
    create_drive_direct(-1 * Rpower, -1 * Lpower);
    msleep(time);
    create_drive_direct(0,0);
}

int long_arm_grab_test(int m){
    int test = get_motor_position_counter(m);
    return test;
}

int interpolateServoPosition(int start, int end, double percentage){
    return start+(end-start)*percentage;
}

void interpolate(int actuator, int start, int end, int time, int interval){
    printf("interpolating \n");
    double currentTime = 0;
    while(currentTime < time){
        set_servo_position(actuator, interpolateServoPosition(start, end, currentTime/time));
        currentTime+=interval;
        msleep(interval);
    }
}

void short_arm_grab(){
    set_servo_position(SHIFTER, SHIFTER_NEUTRAL);
    printf("short arm grab\n");
    short_claw_close();
    interpolate(SHORTARM, SHORTARM_DOWN, SHORTARM_UP, 500, 2);
    moveMotorFor(SHORTCLAW, -40, 350);
}


void short_claw_open(){
    moveMotorFor(SHORTCLAW, -80, 250);
}

void short_claw_open_more(){
    moveMotorFor(SHORTCLAW, -80, 350);
}

void short_arm_down(){
    printf("short arm down \n");
    interpolate(SHORTARM, get_servo_position(SHORTARM), SHORTARM_DOWN, 300, 2);
}

void short_arm_middle(int time){
    int current = get_servo_position(SHORTARM);
    interpolate(SHORTARM, current, SHORTARM_MIDDLE, time, 2);
}

void short_arm_up(){
    printf("short arm down \n");
    interpolate(SHORTARM, get_servo_position(SHORTARM), SHORTARM_UP, 500, 2);
}

void start_setup(){
    create_connect();
    create_full();
    //set_servo_position(LONGARM, LONGARM_START); //CHANGEBACK
    //set_servo_position(LONGARMCLAW, LONGARMCLAW_CLOSE); COMMENT OUT FOR NOW
    set_servo_position(SHORTARM, SHORTARM_UP);
    set_servo_position(SHIFTER, SHIFTER_IN);
    enable_servos();
}

void shifting(){
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_MID, 250, 2);
    interpolate (SHIFTER, SHIFTER_MID, SHIFTER_NEUTRAL, 50, 2);
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 50, 2);
}

void shifting_ii(){
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_MID, 250, 2);
    interpolate (SHIFTER, SHIFTER_MID, SHIFTER_NEUTRAL, 50, 2);
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    //interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 50, 2);
}


void drive_until_black(int dir){
    int thresholdLeft = 1200;
    int thresholdRight = 1200;
    int rdir = dir;
    int ldir = dir;
    while(!(analog(RIGHT_TOPHAT) > thresholdRight && analog(LEFT_TOPHAT) > thresholdLeft) || !(rdir == 0 && ldir == 0)){
        printf("Left: %d, Right: %d ", analog(LEFT_TOPHAT), analog(RIGHT_TOPHAT));
        drive(ldir, rdir);
        if(analog(RIGHT_TOPHAT) > thresholdRight){
            rdir = 0;
        }
        if(analog(LEFT_TOPHAT) > thresholdLeft){
            ldir = 0;
        }
        msleep(1);
    }
    stop();
    printf("\n");
}

void short_claw_close_fast(){
    int counter = 0;
    motor(SHORTCLAW, 50);
    while(digital(SHORTCLAWBUTTON) != 1 && counter <= 1000){
        msleep(1);
        counter++;
    }
    stop();
}

void short_claw_close(){
    int counter = 0;
    motor(SHORTCLAW, 30);
    while(digital(SHORTCLAWBUTTON) != 1 && counter <= 1500){
        msleep(1);
        counter++;
    }
    stop();
}

void grab_cube(){
    short_arm_grab();
    interpolate(SHORTARM, SHORTARM_UP, SHORTARM_SPLIT, 100, 2);
    shifting();
    shifting();
}

int dumping(){ //testing
    moveMotorFor(DUMPER, -40,1000);
    return 0;
}

int main_third_cube_test(){ //testing turn to grab third cube on our side
    /*enable_servo(SHORTARM);

    set_servo_position(SHORTARM, SHORTARM_UP);
    msleep(3000);
    set_servo_position(SHORTARM, SHORTARM_DOWN);
    msleep(3000);
    */
    set_servo_position(SHORTARM, SHORTARM_UP);
    enable_servo(SHORTARM);
    create_connect();
    create_full();
    driveTime(300,300,200);
    driveTime(-200,-200,1800);

    short_claw_close();
    moveMotorFor(SHORTCLAW, -20, 1100);
    short_arm_down(200);
    driveTime(200,200,700);
    third_cube_turn();
    msleep(500);
    driveTime(-150,-150,2000);
    moveMotorFor(SHORTCLAW, -20, 500);
    driveTime(200,200,750);
    short_claw_close();
    return 0;
}

int main6969(){
    enable_servo(SHIFTER);
    while(1){
    shifting();
    }
    return 0;
}



int main_grab_dump() //testing grab and dump: EDIT BACK TO REGUALR
{
    set_servo_position(SHORTARM, SHORTARM_UP);
    set_servo_position(SHIFTER, SHIFTER_IN);
    set_servo_position(LONGARM, LONGARM_START);
    set_servo_position(LONGARMCLAW, LONGARMCLAW_OPEN);
    //set_servo_position(LONGARM, LONGARM_UP);
    enable_servos();
    short_claw_close();
    msleep(1000);
    printf( "%d \n", long_arm_grab_test(0));
    set_servo_position(SHORTARM, SHORTARM_DOWN);
    set_servo_position(SHIFTER, SHIFTER_NEUTRAL);
    printf ("setup done \n");
    int count = 0;
    moveMotorFor(SHORTCLAW, -20, 700);
    printf("claw opened \n");
    msleep(300);
    while(count < 6){
        printf("starting grab \n");
        short_arm_grab();
        short_arm_down();
        msleep(200);
        shifting();
        shifting();
        count++;
        if (count > 4){
            shifting();
            msleep(200);
        }
        printf("cube done \n");
    }
    short_arm_grab();
    short_arm_down();
    printf("last cube done \n");
    msleep(200);
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_MID, 250, 2);
    msleep(200);
    interpolate(SHIFTER, SHIFTER_NEUTRAL, SHIFTER_RIP, 150, 2);
    msleep(100);
    printf("starting dump \n");
    dump_seven_stack();
    printf("finished dump \n");
    set_servo_position(SHIFTER, 1600);
    for(;;);
    return 0;
}

int third_cube_turn(){ //grabbing third cube

    drive(-30,30);
    motor(SHORTCLAW, 8);
    msleep(1000);
    driveTime(100,100,300);
    motor(SHORTCLAW, 8);
    drive(-30,30);
    while(digital(SHORTCLAWBUTTON) != 1){
        msleep(2);
    }
    stop();
    //moveMotorFor(SHORTCLAW,8,2000);
    return 0;/*
    motor(SHORTCLAW, 11);
    msleep(100);
    drive(-40,40);

    msleep(500);
    driveTime(200,200,150);
    motor(SHORTCLAW, 11);
    drive(-40,40);
    msleep(500);
    driveTime(150,150,150);
    drive(30,-30);
    msleep(300);
    driveTime(0,0,1);
    int counter = 0;
    while(digital(SHORTCLAWBUTTON) != 1 && counter <= 2500){
        msleep(1);
        counter++;
    }
    stop();
    //moveMotorFor(SHORTCLAW,8,2000);
    return 0;
    */
}


void long_arm_claw_close(){
    moveMotorFor(LONGARMCLAW,40,1000);
}

void short_arm_split(){
    set_servo_position(SHORTARM, SHORTARM_SPLIT);
    moveMotorFor(SHORTCLAW, -30, 600);
    stop();
    set_servo_position(SHORTARM, SHORTARM_UPMORE);
    moveMotorFor(SHORTCLAW, -30, 1000);
    interpolate(SHORTARM, SHORTARM_UPMORE, SHORTARM_SPLIT, 1000, 2);
}

void setup2(){
    create_connect();
    create_full();
    set_servo_position(LONGARM, LONGARM_UP); //CHANGEBACK
    set_servo_position(LONGARMCLAW, LONGARMCLAW_CLOSE);
    set_servo_position(SHORTARM, SHORTARM_UP);
    set_servo_position(SHIFTER, SHIFTER_NEUTRAL);
    enable_servos();
}

void setup3(){
    create_connect();
    create_full();
    set_servo_position(LONGARM, LONGARM_DEPLOY+150); //CHANGEBACK
    set_servo_position(LONGARMCLAW, LONGARMCLAW_OPEN);
    set_servo_position(SHORTARM, SHORTARM_UP);
    set_servo_position(SHIFTER, SHIFTER_NEUTRAL);
    enable_servos();
}

void dump_seven_stack(){
    set_servo_position(SHIFTER, SHIFTER_RIP); //shifter pulls out
    set_servo_position(SHORTARM, SHORTARM_MIDDLE);
    msleep(500);
    moveMotorFor(DUMPER, -40, 1200);
    printf("Dumping has finished, 7 stack\n");
}

void dump_four_stack(){
    set_servo_position(SHIFTER, SHIFTER_RIP); //shifter pulls out
    set_servo_position(SHORTARM, SHORTARM_MIDDLE);
    moveMotorFor(DUMPER, -25, 600); //let the dumper let the butters slide down
    msleep(1500);
    moveMotorFor(DUMPER, -20, 1000);
    msleep(1500);
    printf("Dumping has finished, 4 stack\n");
}


void long_arm_claw_open(){
    moveMotorFor(LONGARMCLAW,-20,1000);
}

void cube_adjust_a(int d, int r){
    short_claw_close();
    driveTime(-300,-300,d);
    short_claw_open();
    driveTime(300,300,r);
}
void cube_adjust_b(int speed, int d, int r){
    short_claw_close();
    driveTime(-speed,-speed,d);
    short_claw_open();
    driveTime(speed,speed,r);
}

void cube_adjust(int d){
    short_claw_close();
    driveTime(-300,-300,d);
    short_claw_open();
    driveTime(300,300,175);
}

void cube_adjust_ii(int d){
    driveTime(-60, 60, 200);
    driveTime(200,200, 0.5 * d);
    driveTime(-60, 60, 300);
    driveTime(200,200, 0.5 * d);
}

int align_and_chop(){
    /*
    setup2();

    set_servo_position(LONGARMCLAW, LONGARMCLAW_CLOSE);
    interpolate(LONGARM, LONGARM_DEPLOY, LONGARM_UP, 2000,10);
    interpolate(LONGARM, LONGARM_UP, LONGARM_DEPLOY+100, 2000, 10);
    set_servo_position(LONGARMCLAW, LONGARMCLAW_OPEN);
    interpolate(LONGARM, LONGARM_DEPLOY+100, LONGARM_UP, 1000,10);
    */
    set_servo_position(SHIFTER, SHIFTER_NEUTRAL);
    driveTime(200,200,870); //controls horizontal alignment with cubes //fudge
    driveTime(-200,200,950); //fag 940

    motor(SHORTCLAW, -90);
    short_arm_down();
    stop();
    set_servo_position(LONGARMCLAW, LONGARMCLAW_CLOSE);
    driveTime(400,400,500);
    driveTime(200,200,400); //fag ,,500
    driveTime(-40,40,450); //fag -70, 70, 400
    msleep(100);
    driveTime(70,-70,50); //fag 70, -70, 150
    driveTime(150,150,700); //fag 200,200,500
    //FAG KEEP THIS IN, Readjust
    /*
    driveTime(-40,40,750); //fag -70,70,400
    driveTime(200,200,200);
    */
    driveTime(-200,-200,400);
    driveTime(100,-100,100);

    set_servo_position(SHORTARM, SHORTARM_SPLIT);
    msleep(200);
    short_claw_close_fast();

    driveTime(-100,100,90); //180
    stop();
    driveTime(100,100,600);
    moveMotorFor(SHORTCLAW, -60, 300);
    stop();
    set_servo_position(SHORTARM, SHORTARM_UP);
    moveMotorFor(SHORTCLAW, -60, 925*0.5); //TIMECHANGE
    interpolate(SHORTARM, SHORTARM_UP, SHORTARM_SPLIT-100, 500, 2);

    driveTime(30,-30,1000);
    interpolate(SHORTARM, SHORTARM_SPLIT-100,SHORTARM_UP, 100,20);

    driveTime(100,-100,100);
    set_servo_position(SHORTARM, SHORTARM_UP);
    driveTime(-100,100,230); //260
    interpolate(SHORTARM, SHORTARM_UP, SHORTARM_SPLIT-100, 500, 2);
    driveTime(30,-30,1200);
    interpolate(SHORTARM, SHORTARM_SPLIT-100,SHORTARM_UP, 100,20);
    driveTime(100,-100,100);


    //driveTime(-130,130,680+165); //-118.118, 845
    driveTime(-124,124,680+165); //-118.118, 845
    driveTime(280,280,210); //fag 230,230,210
    interpolate(SHORTARM, SHORTARM_UP, SHORTARM_SPLIT-100, 500, 2);

    driveTime(-30,50,1000);
    interpolate(SHORTARM, SHORTARM_SPLIT-100,SHORTARM_UP, 100,20);
    driveTime(-200,200,100); // fag ,,130
    set_servo_position(SHORTARM, SHORTARM_UP);

    driveTime(120,-200,260);
    driveTime(-200,-200,100); //fudge
    //driveTime(-70,70,100);
    interpolate(SHORTARM, SHORTARM_UP, SHORTARM_SPLIT-100, 500, 2);

    driveTime(-30,50,1000);
    interpolate(SHORTARM, SHORTARM_SPLIT-100,SHORTARM_UP, 100,20);
    driveTime(100,-100,100);
    motor(SHORTCLAW, 100);
    msleep(300);
    stop();
    set_servo_position(SHORTARM, SHORTARM_SPLIT);
    short_claw_close();
    driveTime(150,-150,270); //240 fag 230
    short_claw_open_more();
    //driveTime(-200,-200,200); fag
    short_arm_down();
    driveTime(-200,200,150); //fag
    driveTime(250,250,300); //fag 150, 150, 400
    //driveTime(-100,100,300);
    //driveTime(100,100,300);
    short_claw_close();
    driveTime(200,-200,150); //fag
    cube_adjust_a(450,300);

    short_arm_grab();
    motor(SHORTCLAW, -60);
    driveTime(-300,-300,125);
    interpolate(SHORTARM, SHORTARM_UP, SHORTARM_SPLIT, 100, 2);
    stop();
    //drive(-130,-130); //fag -230,-230
    //interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    //interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 50, 2);
    //stop();

    drive(200,-200); //280
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 200, 2);
    stop();
    short_arm_down();

    drive(250,250);
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    stop();
    drive(-150,150); //fag 100 time
    msleep(100);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 50, 2); //fag was above
    stop();
    driveTime(200,200,300);
    //cube_adjust_ii(400);
    cube_adjust_a(300,300);

    short_arm_grab();
    motor(SHORTCLAW, -80);
    interpolate(SHORTARM, SHORTARM_UP, SHORTARM_SPLIT, 100, 2);
    stop();
    drive(-250,-250);
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 50, 2);
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 50, 2);
    stop();
    driveTime(250,-250,200);
    short_arm_down();
    drive(200,200);
    shifting();
    stop();
    driveTime(-150,150,200);
    driveTime(150,150,300);
    driveTime(-100,100,100);
    short_arm_grab();
    interpolate(SHORTARM, SHORTARM_UP, SHORTARM_SPLIT, 100, 2);
    //cube_adjust_ii(600);
    return 0;
}

int main_dumping_test(){ //test dumping
    set_servo_position(SHIFTER, SHIFTER_RIP); //shifter pulls out
    set_servo_position(SHORTARM, SHORTARM_MIDDLE);
    msleep(500);
    disable_servos();
    moveMotorFor(DUMPER, -40,500);

    moveMotorFor(DUMPER, -18,600); //regurgitate butters into a pile
    msleep(200000);
    return 0;
}

int check_fucked(){
    if(analog(LEFT_TOPHAT) > 900 || analog(RIGHT_TOPHAT) > 900){

        printf("\n FUUUUUUUCCCKKKKEEDDD \n");
        return 1;
    }
    return 0;
}

int main(){ //Full Run
    wait_for_light(5);
    start_setup();

    shut_down_in(120);
    printf("starting");
    interpolate(LONGARM, LONGARM_START, LONGARM_UP, 500,10);
    msleep(1000); //wait for robot
    driveTime(200,-200,250);
    driveTime(400, 400, 500); //fag time:300
    driveTime(-350, -350, 535);
    driveTime(200,-200,920);
    driveTime(300, 300, 400); //drive towards the butter in starting box //450
    set_servo_position(SHORTARM, SHORTARM_DOWN);
    moveMotorFor(SHORTCLAW, -40, 250); //TIMECHANGE
    driveTime(100, 100, 750 *1.8);
    set_servo_position(SHIFTER, SHIFTER_NEUTRAL);
    short_arm_grab(); //pick up first butter
    //driveTime(-200,-200,400);
    set_servo_position(SHORTARM, SHORTARM_SPLIT);

    /*
    driveTime(72,-72,500); //DONT FUCKING DELETE THIS LINE, WE NEED THIS NIGGA SHIT

    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_MID, 250, 2);
    interpolate (SHIFTER, SHIFTER_MID, SHIFTER_NEUTRAL, 250, 2);
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 250, 2);

    //start grab poms

    moveMotorFor(SHORTCLAW, -20, 400); //open claw
    interpolate(SHORTARM, SHORTARM_MIDDLE, SHORTARM_DOWN, 200,2);
    driveTime(160,160,400);

    short_claw_close();
    set_servo_position(SHIFTER, SHIFTER_IN);
    interpolate(SHORTARM, SHORTARM_DOWN, SHORTARM_UP, 1000, 2); //SHORTARM_MIDDLE-200
    driveTime(-72,72,500);
   */
    //heading towards furrow, getting into position
    driveTime(-200,200,100);
    drive(400, 500);

    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_MID, 250, 2);
    interpolate (SHIFTER, SHIFTER_MID, SHIFTER_NEUTRAL, 100, 2);
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 100, 2);
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 100, 2);

    stop();
    //drive(450, 225);

    //stop();
    short_claw_close_fast();
    /*
    motor(SHORTCLAW, 30);
    while(digital(SHORTCLAWBUTTON) != 1){
        drive(400,300);
    }
    */
    stop();
    driveTime(400, 400,600);
    set_servo_position(SHIFTER, SHIFTER_IN);
    set_servo_position(SHORTARM, SHORTARM_UP);
    driveTime(-200, -200, 250);
    driveTime(200, -200, 930 + 500);
    interpolate(SHORTARM, SHORTARM_UP, SHORTARM_DOWN - 200, 300,10);
    driveTime(-100, 100, 700); //800 is the olde version turns back to align with furrows, fag

    //driveTime(100,100,100);
    set_servo_position(LONGARMCLAW, LONGARMCLAW_OPEN);

    //start three cube sweep
    interpolate(LONGARM, LONGARM_UP, LONGARM_DEPLOY+100, 1000, 10);//70
    msleep(500);
    while(digital(LONGARMBUTTON) != 1){
        drive(80,80);
    }
    printf("stopped");
    stop();
    driveTime(-250, -250, 500);
    //driveTime(100,-100,60);
    //set_servo_position(SHIFTER, SHIFTER_MID);
    while(digital(LONGARMBUTTON) != 1){
        drive(80,80);
    }
    stop();
    driveTime(-100,-100,150);
    driveTime(100,-100,150);
    msleep(200);  //fag2
    interpolate(LONGARMCLAW, LONGARMCLAW_OPEN, LONGARMCLAW_CLOSE, 600, 10);
    interpolate(LONGARM, LONGARM_DEPLOY+100, LONGARM_DEPLOY-300, 200, 10);
    back_until_bump();

    interpolate(LONGARM, LONGARM_DEPLOY-300, LONGARM_UP-100, 800, 100);
    set_servo_position(LONGARM, LONGARM_UP);
    driveTime(200,200,200);
    driveTime(200,-200,930);
    back_until_bump_ii();
    set_servo_position(SHORTARM, SHORTARM_SPLIT);
    int fucked = 0;
    if(analog(LEFT_TOPHAT) > 900 || analog(RIGHT_TOPHAT) > 900){
        fucked = 1;
                printf("\n FUUUUUUUCCCKKKKEEDDD \n");
    }
    if(fucked){
        driveTime(300,300,520);
        driveTime(400,-400,450);
        driveTime(400,400,400);
        driveTime(-400,-400,300);
        driveTime(-200,200,1860);
    }
    else{
        drive_until_black(150); //get back to starting box, get ready for the long ass drive
        msleep(100);
        driveTime(250,250,400);
        driveTime(-200,200,900);
    }

    driveTime(410,410,950); //long ass drives with swerves 410/400, fag //420/410
    driveTime(410,450,1820); //400/410
    if(fucked){
        driveTime(450,450,800);
    }
    else{
        driveTime(450,450,800);
    }
    //driveTime(300,350,2000); //swerve to balance out
    drive(450,450);
    //fag2
    if(analog(LEFT_TOPHAT) > 900 || analog(RIGHT_TOPHAT) > 900){
        fucked = 1;
        printf("\n FUUUUUUUCCCKKKKEEDDD \n");
    }
    msleep(300);
    stop();
    driveTime(-300,300,120);
    interpolate(SHORTARM, SHORTARM_MIDDLE-200, SHORTARM_DOWN, 300,10); //pound that gay ass cow
    stop();

    //spank blue cow
    driveTime(300,-300,670 + 120); //TIMECHANGE
    driveTime(-400,400,100); //TIMECHANGE
    interpolate(SHORTARM, SHORTARM_DOWN, SHORTARM_UP+100, 150,10);
    driveTime(-200,200,700); //fag2 800
    //driveTime(400,-400,100);

    //drive_until_black(250);

    driveTime(420,420,1200); //front parallel swerve //fag 440, 420, 1200

    driveTime(400,400,650); //fag2 450,450,

    driveTime(-480,-480,300); //fag
    driveTime(450,-450,500);

    if(!fucked){
        //fag2 fag2 fag2
        int counter = 0;
        while((analog(LEFT_TOPHAT) < 1200 || analog(RIGHT_TOPHAT) < 1200) && counter < 2000){ //gets right into the black line follow
            drive(250,250);
            counter++;
        }
        if(analog(RIGHT_TOPHAT) > 1200){
            while(analog(LEFT_TOPHAT) < 1200 && counter < 2000){
                drive(250,250);
                counter++;
            }
        }
        else{
            while(analog(RIGHT_TOPHAT) < 1200 && counter < 2000){
                drive(250,250);
                counter++;
            }
        }
        if(counter >= 2000){
            driveTime(300,300,367);
        }
        //if(!check_fucked()){
            drive_until_black(-150); //TIMECHANGE
            driveTime(-300,-300,500);
        //}
        /*else{
            driveTime(-300,-300,700);
        }*/
    }
    else{
        driveTime(450,450,2000);   //fag 300,300,500
        driveTime(-450,-450,1500);
    }
    driveTime(-400,400,465);
    driveTime(450,450,750); //front parallel, fag
    set_servo_position(SHIFTER, SHIFTER_IN);

    //going for first cube on our side
    driveTime(-300,-300,820);
    driveTime(-200,200,930);
    motor(SHORTCLAW, -35); //opens claw while putting it down wew
    short_arm_down();
    stop();
    driveTime(300,300,600); //fudge
    short_claw_close();

    driveTime(-300,-300,300);
    cube_adjust_a(300,300);
    short_arm_grab();
    set_servo_position(SHORTARM, SHORTARM_SPLIT);
    msleep(100);
    //set_servo_position(SHORTARM, SHORTARM_UP);
    short_claw_close_fast();

    //grabbing second cube on our side
    drive(-250,-250);
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 50, 2);
    stop();
    driveTime(200, -200, 850);
    drive(450, 450); //front parallel
    shifting_ii();
    msleep(200);
    stop();
    set_servo_position(SHORTARM, SHORTARM_UP);
    driveTime(-220, -220, 350); //back up
    driveTime(-200, 200, 900);
    motor(SHORTCLAW, -35); //opens claw while putting it down wew, ITERATION 2
    short_arm_down();
    stop();
    driveTime(280,280,750); //drive towards second cube //620

    short_claw_close();
    cube_adjust_a(400,250); //fag 400, 250
    //driveTime(-300,-300,250); //300
    //cube_adjust(300);
    short_arm_grab();
    set_servo_position(SHORTARM, SHORTARM_SPLIT);

    //going for third cube on our side
    //driveTime(200,200,300);
    driveTime(200,-200,900);
    drive(450,450); //front parallel
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 50, 2);
    msleep(200);
    stop();
    drive(-400,-400); //back up to prepare for the third cube //TIMECHANGE
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 200, 2);
    stop();
    driveTime(-200,200, 560);
    set_servo_position(SHORTARM, SHORTARM_SPLIT);
    short_claw_close_fast();
    moveMotorFor(SHORTCLAW, -60, 350); //TIMECHANGE
    short_arm_down(200);
    driveTime(250,250,200);
    //driveTime(230,230,440); //come a bit closer to third cube

    //third_cube_turn(); //turn specially to align with third cube, UPDATE MANUALLY DO A THIRD CUBE TURN
    driveTime(-40,40,350); //fag -60, 60, 700
    //driveTime(200,200,200);
    short_claw_close(); //CLOSE PULL OUT AND MORE PULL OUT
    drive(-250,-250); //TIMECHANGE
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 100, 2);
    shifting_ii();
    //msleep(250);
    stop();
    moveMotorFor(SHORTCLAW, -80, 250); //grab that nigga and eat it (well we only open it)
    driveTime(200,200,500);
    driveTime(-150, 150, 300);

    driveTime(200,200, 150);
    /*
    driveTime(200,200, 400);
    driveTime(-150, 150, 300);
    driveTime(200,200, 100);
    */
    short_claw_close(); //now really grab that nigga
    //driveTime(-300,-300,250);
    short_arm_grab();
    driveTime(400,400,100);
    set_servo_position(SHORTARM, SHORTARM_SPLIT);
    short_claw_close_fast();

    driveTime(-330,330,500); //fag 550
    //readjust and prep for butter splitting
    drive(400,400);
    shifting();
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 250, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 50, 2);
    stop();


    if(!fucked){
        driveTime(-200,200,960);
        drive_until_black(200);
        driveTime(-300,-300,300);
        driveTime(-200,200,200);
        back_until_bump_ii();
        driveTime(200,-200,600);
    }
    else{
        driveTime(400,400,250);
        driveTime(-400,400,460);
        back_until_bump_ii();
        driveTime(200,-200,350);
    }

    stop();


    //butter splitting
    interpolate(LONGARM, LONGARM_UP, LONGARM_DEPLOY+100, 1500,10);
    interpolate(LONGARMCLAW, LONGARMCLAW_CLOSE, LONGARMCLAW_OPEN,500,10);
    driveTime(-100,100,100);
    interpolate(LONGARM, LONGARM_DEPLOY+100, LONGARM_UP, 1000,10);
    driveTime(100,-100,100);
    grab_long_arm_cubes();
    //align_and_chop(); //use ninja techniques and then hope for the best when we attempt to grab it
    //short_arm_up(); //pull up

    /*
        drive(-400,-400);
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_MID, 400, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 100, 2);
    stop();


    */
    driveTime(-300,-300,300);
    driveTime(200,-200,800+150); //turn to face towards barn

    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_MID, 400, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 100, 2);
    stop();
    if(fucked){
        driveTime(300,300,150);
        driveTime(-200,200,200);
        short_arm_down();
        short_claw_close_fast();
        driveTime(150,-150,1200);
        driveTime(-150,150,1200);
        set_servo_position(SHORTARM, SHORTARM_SPLIT);
        msleep(100);
        drive(400,400);
        interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_MID, 400, 2);
        interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 100, 2);
        msleep(500);
        stop();
        driveTime(-400,-400,500);
    }
    else{

        drive_until_black(200);
        driveTime(-200,200,200);
        short_arm_down();
        short_claw_close_fast();
        driveTime(150,-150,1200);
        driveTime(-150,150,2800 / 3);

    }

    //interpolate(LONGARM, LONGARM_UP, LONGARM_START, 900,2);
    //stop();
    //disable_servo(LONGARM);
    //drive_until_black(250); //align with barn
    driveTime(-100, 100, 1860);
    interpolate(LONGARM, LONGARM_UP, LONGARM_UP+400, 200,1);
    disable_servo(LONGARM);
    dump_seven_stack(); //fudge

    for(;;){
    };
    return 0;
}


int caress_butters(){
    //create_connect();
    //create_full();
    set_servo_position(SHORTARM, 580);

    enable_servo(SHORTARM);
    int i = 0;
    while(i < 4){
        if (i == 0){
            driveTime(70, 70, 650);
        }
        else{
            driveTime(70, 70, 1000);
        }
        set_servo_position(SHORTARM, 470);
        driveTime(-70, -70, 500);
        set_servo_position(SHORTARM, 580);
        i++;
    }
    driveTime(-70, -70, 200);
    driveTime(30, -30, 1500);
    moveMotorFor(SHORTCLAW, -20, 200);
    driveTime(-30, 30, 3000);
    driveTime(30, -30, 1500);
    driveTime(-100, -100, 3000);
    //enable_servos();
    //short_arm_down();
    //driveTime(30, -30, 1000);

    return 0;
}

int main_align(){
    setup3();

    interpolate(LONGARMCLAW, LONGARMCLAW_OPEN, LONGARMCLAW_CLOSE, 800,10);
    interpolate(LONGARM, LONGARM_DEPLOY+150, LONGARM_UP,2000, 2);
    //driveTime(200,-200,1930);
    interpolate(LONGARM, LONGARM_UP, LONGARM_DEPLOY+100, 1500,10);
    interpolate(LONGARMCLAW, LONGARMCLAW_CLOSE, LONGARMCLAW_OPEN,500,10);
    driveTime(-100,100,100);
    interpolate(LONGARM, LONGARM_DEPLOY+100, LONGARM_UP, 1000,10);
    driveTime(100,-100,100);
    align_and_chop();

    main_dumping_test();
    for(;;);
    return 0;
}

int main_third_cube_turn(){
    setup2();
    drive(400,400); //front parallel
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 200, 2);
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 100, 2);
    msleep(800);
    stop();
    drive(-400,-400); //back up to prepare for the third cube //TIMECHANGE
    interpolate (SHIFTER, SHIFTER_NEUTRAL, SHIFTER_IN, 200, 2);
    stop();
    driveTime(-200,200, 560);
    set_servo_position(SHORTARM, SHORTARM_SPLIT);
    short_claw_close_fast();
    moveMotorFor(SHORTCLAW, -60, 350); //TIMECHANGE
    short_arm_down(200);
    driveTime(250,250,200);
    //driveTime(230,230,440); //come a bit closer to third cube
    //third_cube_turn(); //turn specially to align with third cube
    driveTime(-60,60,700);
    //driveTime(200,200,200);
    short_claw_close();
    drive(-250,-250); //TIMECHANGE
    interpolate (SHIFTER, SHIFTER_IN, SHIFTER_NEUTRAL, 100, 2);
    shifting();
    msleep(350);
    stop();
    moveMotorFor(SHORTCLAW, -80, 250); //grab that nigga and eat it (well we only open it)
    driveTime(200,200,500);
    driveTime(-150, 150, 300);
    driveTime(200,200, 400);
    driveTime(-150, 150, 300);
    driveTime(200,200, 100);
    short_claw_close(); //now really grab that nigga
    driveTime(-300,-300,150);
    short_arm_grab();
    set_servo_position(SHORTARM, SHORTARM_SPLIT);
    short_claw_close_fast();
    return 0;
}
int main_drive_until_black(){
    start_setup();
    drive_until_black(200);
    return 0;
}
void grab_long_arm_cubes(){

    set_servo_position(SHIFTER, SHIFTER_NEUTRAL);
    driveTime(215,215,870); //controls horizontal alignment with cubes //fudge
    driveTime(-200,200,950); //fag 940
    motor(SHORTCLAW, -70);
    short_arm_down();
    stop();
    set_servo_position(LONGARMCLAW, LONGARMCLAW_CLOSE);
    driveTime(400,400,500);
    driveTime(200,200,400); //fag ,,500
    driveTime(-40,40,450); //fag -70, 70, 400
    msleep(100);
    driveTime(70,-70,50); //fag 70, -70, 150
    driveTime(150,150,700); //fag 2
    driveTime(-40,40,150);
    driveTime(-100,100,50);
    driveTime(-300,-300,400);
    short_arm_up();
    driveTime(-200,200,950);
    driveTime(100,100,1000);
    driveTime(200,-200,930);
    driveTime(300,300,100);

    caress_ii();
}
int main_caress(){
    setup3();

    set_servo_position(LONGARMCLAW, LONGARMCLAW_CLOSE);
    interpolate(LONGARM, LONGARM_DEPLOY, LONGARM_UP, 1500,10);
    interpolate(LONGARM, LONGARM_UP, LONGARM_DEPLOY+100, 1500, 10);
    interpolate(LONGARMCLAW, LONGARMCLAW_CLOSE, LONGARMCLAW_OPEN,500,10);
    driveTime(-100,100,100);
    interpolate(LONGARM, LONGARM_DEPLOY+100, LONGARM_UP, 1000,10);
    /*
    short_claw_open();

    driveTime(100,-100,100);
    driveTime(200,200,180); //controls horizontal alignment with cubes //fudge
    driveTime(-200,200,1010); //fag 940
    driveTime(400,400,200);
    */



    set_servo_position(SHIFTER, SHIFTER_NEUTRAL);
    driveTime(215,215,870); //controls horizontal alignment with cubes //fudge
    driveTime(-200,200,950); //fag 940
    motor(SHORTCLAW, -70);
    short_arm_down();
    stop();
    set_servo_position(LONGARMCLAW, LONGARMCLAW_CLOSE);
    driveTime(400,400,500);
    driveTime(200,200,400); //fag ,,500
    driveTime(-40,40,450); //fag -70, 70, 400
    msleep(100);
    driveTime(70,-70,50); //fag 70, -70, 150
    driveTime(150,150,700); //fag 2
    driveTime(-40,40,150);
    driveTime(-100,100,50);
    driveTime(-300,-300,400);
    short_arm_up();
    driveTime(-200,200,950);
    driveTime(100,100,1000);
    driveTime(200,-200,930);
    driveTime(300,300,100);
    caress_ii();
    return 0;


}

void caress_ii(){

    short_arm_down();
    driveTime(100,100, 1100);
    //driveTime(100, 100, 600);
    /*
        set_servo_position(SHORTARM, SHORTARM_DOWN+200);
    msleep(50);
        driveTime(-70, -70, 300);
        set_servo_position(SHORTARM, SHORTARM_DOWN);
        driveTime(100, 100, 600);
        set_servo_position(SHORTARM, SHORTARM_DOWN+200);
        */
        //driveTime(-70, -70, 300);
    //driveTime(-200,-200,200);
        set_servo_position(SHORTARM, SHORTARM_DOWN);
    //driveTime(-60,60,100);
    short_claw_close();
    driveTime(-50,50,400);
    cube_adjust_b(200,900,450); //600,300
    short_arm_grab();
    set_servo_position(SHORTARM, SHORTARM_SPLIT);
    shifting();
    shifting();
    driveTime(100,-100,500);
    short_arm_down();

    driveTime(300,300,300);
    driveTime(-60,60,200);

    short_claw_close();
    cube_adjust_b(200,600,400);
    short_arm_grab();
    set_servo_position(SHORTARM, SHORTARM_SPLIT);
    shifting();
    shifting();
    short_arm_down();

    driveTime(300,300,600);
    driveTime(-50,50,450);
    driveTime(150,150,533);
    driveTime(-70,70,300);
    driveTime(150,150,200);
    short_claw_close();
    short_arm_grab();
    set_servo_position(SHORTARM, SHORTARM_SPLIT);
}
