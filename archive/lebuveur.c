#include <kipr/botball.h>

//PORTS
#define ARM 		1
#define CLAW 		3
#define ET_SERVO	2

//Analog PORTS
#define REFLECTANCE 5
#define ET 0
#define LIGHT_PORT 	3

//digital ports
#define LEVER 0

//Basic ARM movements
#define ARM_DOWN	2047
#define ARM_UP		1520
#define ARM_START   0
//Basic CLAW movements
#define CLAW_OPEN	690
#define CLAW_CLOSE	1300
#define CLAW_OPEN_PEOPLE 750
#define CLAW_MID	1010

//Thresholds - for create sensor
#define THRESHOLD 2500
#define SWHITE 500
#define SBLACK 2500

//Special
#define GRAY 500

//ET Positions
#define ET_START	0
#define ET_RUN		950
#define ET_SHOW		1910


//////////////////////////////////////////////////Basic Functions//////////////////////////////////////////////////////

void stop() {
    create_drive_direct(0, 0);
}

void drive(int speed, int time) {
    create_drive_direct(-speed *.96, -speed); 
    msleep(time);
    stop();
}

void turn_right(int speed, int time) {
    create_drive_direct(speed, -speed);
    msleep(time);
    stop();
}

void turn_left(int speed, int time) {
    create_drive_direct(-speed, speed);
    msleep(time);
    stop();
}

void interpolate(int srv, int pos, int step_size) {
    int cur_pos = get_servo_position(srv);
    int delta_remain = abs(pos - cur_pos);  
    int direction = pos > cur_pos ? 1 : -1;

    while(delta_remain >= 5) {
        //Make the final step fit so that the servo goes to its desired position
        if(delta_remain < step_size)
            step_size = delta_remain;

        set_servo_position(srv, cur_pos + direction * step_size);
        msleep(50);

        //Update the necessary variable
        cur_pos = get_servo_position(srv);
        delta_remain = abs(pos - cur_pos);
    }
}

void back_until_bump(){
    while(!get_create_lbump()&&!get_create_rbump()){
        create_drive_direct(200,200);
    }
    stop();
}

int getSign(int n, int tolerance){
    if(n > tolerance){
        return 1;
    }
    else if(n < -tolerance){
        return -1;
    }
    return 0;
}

void driveToLine(int power, int adjustPower){
    int leftSign = getSign(THRESHOLD - get_create_lcliff_amt(),0);
    int rightSign = getSign(THRESHOLD - get_create_rcliff_amt(),0);
    int rpower = rightSign * power;
    int lpower = leftSign * power;
    while(!(leftSign == 1 && rightSign == 1)){
        create_drive_direct(rpower, lpower);
        msleep(1);
        leftSign = getSign(THRESHOLD - get_create_lcliff_amt(),0);
        rightSign = getSign(THRESHOLD - get_create_rcliff_amt(),0);
        if(leftSign == 1){
            rpower = rightSign * adjustPower;
        }
        if(rightSign == 1){
            lpower = leftSign * adjustPower;
        }
    }
    stop();
}

inline int over_black()
{
    //printf("REFLECTANCE: %d %d\n", analog(REFLECTANCE), analog(REFLECTANCE) > THRESHOLD);
    return analog(REFLECTANCE) > 3300;
}

//for the black on the left and the white/gray on the right
void line_follow(int time) {
    int t = 0, interval = 10;
    while(t * interval < time){
        if(!over_black()) {
            create_drive_direct(-200, -150);
        }
        else {
            create_drive_direct(-150, -200);
        }
        msleep(interval);
        t++;
    }
    stop();
}

//for the black on the right and the white/Gray on the left
void line_follow_left(int time) {
    int t = 0, interval = 10;
    while(t * interval < time){
        if(over_black()) {
            create_drive_direct(-200, -150);
        }
        else {
            create_drive_direct(-150, -200);
        }
        msleep(interval);
        t++;
    }
    stop();
}

void line_follow_fast(int time) {
    int t = 0, interval = 10;
    while(t * interval < time){
        if(!over_black()) {
            create_drive_direct(-250, -200);
        }
        else {
            create_drive_direct(-200, -250);
        }
        msleep(interval);
        t++;
    }
    stop();
}

void lfet(int d){
    //int t = 0;
    //while(t <= 3000);
    //int x = analog(ET);
    //int y = analog(ET);
    //new
    //while(y - x <= 200){
    //printf("OLD = %d \nNEW = %d\n\n", x, y);
    //x = y;
   // set_servo_position(ET_SERVO, ET_SHOW);
    //msleep(1000);
    if(d > 0){
        int t = 0;
        while(analog(ET) > 1670 && t < 10){
            if(analog(ET) < 1800){
                t++;
            }
            if(!over_black()) {
                create_drive_direct(-140, -100);
            }
            else {
                create_drive_direct(-100, -140);
            }
            //msleep(10);
                //y = analog(ET);
                //msleep(interval);
                //t++;
        	printf("FRONT %d\n", analog(ET));
        }
        stop();
        printf("FINAL: %d\n", analog(ET));
    }
    if(d < 0){
        int t = 0;
        while(analog(ET) > 1900 || t < 10){
            if(analog(ET) < 1900){
                t++;
            }
            if(get_create_lfcliff_amt() > SBLACK) {
            create_drive_direct(140, 100);
            }
            else {
                create_drive_direct(100, 140);
            }
            printf("BACK %d\n", analog(ET));
        }
        stop();
    	printf("FINAL: %d\n", analog(ET));
    }
            
    if(d == 0){
        int t = 0;
        while(analog(ET) < 1700 || t < 10){
            if(analog(ET) < 1860){
                t++;
            }
            if(!over_black()) {
                create_drive_direct(-140, -100);
            }
            else {
                create_drive_direct(-100, -140);
            }
        	printf("MIDDLE %d\n", analog(ET));
        }
        stop();
        printf("FINAL: %d\n", analog(ET));
    }
    //set_servo_position(ET_SERVO, ET_RUN);
    msleep(200);
}

float average(int values[], int size) {
    int i = 0;
    float sum = 0;
    //printf("[");
    for (i = 0; i < size; i++) {
        //printf("%f, ", (float) values[i]);
        sum += (float) values[i];
    }
    //printf("]");
    //printf("[sum=%f, size=%d]", sum, size);
    return sum / ((float) size);
}

float stdSquared(int values[], int size) {
    float mean = average(values, size);
    int i = 0;
    float sum = 0;
    for (i = 0; i < size; i++) {
        float diff = ((float) values[i]) - mean;
        //printf("%f %f %d %f - ", diff, sum, values[i], mean);
        sum += diff * diff;
    }
    printf("%f-> ", sum / ((float) (size - 1)));
    return sum / ((float) (size - 1));
}

void driveTillET(int left, int right, int threshold, int sign) {
    int arraySize = 50;
    int etValues[arraySize];
    int currentIndex = -1;
    int tophat = analog(REFLECTANCE);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    int counter = 0;
    printf("Start: %d, ", analog(ET));
    stop();
    while (counter <= (arraySize+500) || (sign * (stdSquared(etValues, sizeof(etValues)/sizeof(int)) - threshold)) < 0) {
        create_drive_direct((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, (percent > 0.5 ? 1 : (percent / 0.5)) * right);
        //msleep(1);
        tophat = analog(REFLECTANCE);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        currentIndex = (currentIndex + 1) % arraySize;
        etValues[currentIndex] = analog(ET);
        //printf("%d, ", analog(ET));
        counter++;
    }
    printf("Done\n");
}

#define IRWHITE 2500
#define IRBLACK 500

void lineFollowIr(int left, int right, int time) {
    long endTime = systime() + time;
    int ir = get_create_lfcliff_amt();
    double percent = ((double) (ir - IRWHITE)) / ((double) (IRBLACK - IRWHITE));
    while (systime() < endTime) {
        create_drive_direct(((percent > 0.5 ? 1 : (percent / 0.5)) * left), ((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * right));
        msleep(1);
        ir = get_create_lfcliff_amt();
        printf("%f->", percent);
        percent = ((double) (ir - IRWHITE)) / ((double) (IRBLACK - IRWHITE));
    }
}

void back_follow(int time){
    int t = 0, interval = 10;
    while(t * interval < time){
        if(get_create_lfcliff_amt() > SBLACK) {
            create_drive_direct(250, 200);
        }
        else {
            create_drive_direct(200, 250);
        }
        msleep(interval);
        t++;
    }
    stop();
}

void bf_fast(int time){
    int t = 0, interval = 10;
    while(t * interval < time){
        if(get_create_lfcliff_amt() > SBLACK) {
            create_drive_direct(310, 270);
        }
        else {
            create_drive_direct(270, 310);
        }
        msleep(interval);
        t++;
    }
    stop();
}


void lineFollow(int left, int right, int time) {
    long endTime = systime() + time;
    int tophat = analog(REFLECTANCE);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    while (systime() < endTime) {
        create_drive_direct((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, (percent > 0.5 ? 1 : (percent / 0.5)) * right);
        msleep(1);
        tophat = analog(REFLECTANCE);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    }
}

void lever(){
    int t = 0;
    int time = 1000;
    while(digital(LEVER) != 1 && t < time){
        create_drive_direct(-100, -100);
        msleep(1);
        t++;
        if(t == time){
            stop();
            msleep(100);
            drive(-200, 400);
            turn_left(120, 30);
        }
    }
    stop();
    if(t == time){
        lever();
    }
}

void interpolateServoByTime(int port, int start, int end, int time){
    unsigned long endTime = systime() + time;
    while(systime() < endTime){
        set_servo_position(port, (end - start) * (1.0 - (((double)(endTime - systime())) / ((double)time))) + start);
        msleep(1);
    }
    set_servo_position(port, end);
}

///////////////////////////////////////////////////////////////////////CODE////////////////////////////////////////////////////////////////////////////////
void setup(){
    create_connect();
    create_full();
    enable_servo(CLAW);
    enable_servo(ARM);
    enable_servo(ET_SERVO);
    set_servo_position(CLAW, CLAW_CLOSE);
    msleep(100);
    interpolate(ET_SERVO, ET_RUN, 40); 
    interpolate(ARM, 0, 30);
    interpolate(ET_SERVO, ET_START, 40);
    msleep(100);
}

void init(){
    drive(-100, 500);
    turn_right(100, 2000);
    turn_right(150, 1040);
    //drive(150, 350);
}


void firstAndSecondSupplies(){
    set_servo_position(ET_SERVO, ET_SHOW);
    msleep(250); 
    interpolate(ARM, ARM_DOWN, 150);
    set_servo_position(CLAW, CLAW_OPEN);
    drive(350, 750);
    interpolate(CLAW, CLAW_CLOSE + 20, 100);
    
 
    
    
    
    //old
    drive(-340, 450); //300 - 500 
    turn_right(320, 200); //300 - 150 //310 - 140
    drive(350, 1050); //300 - 1100 //320 - 1040
    turn_right(130, 2500); //200 - 1700 
    
    //drive(320, 300);
    set_servo_position(CLAW, CLAW_OPEN_PEOPLE);
    interpolate(ARM, ARM_UP, 100); 
    //drive(-300, 320);
    driveToLine(-250, -30);
    
}

void thirdSupplies(){
    drive(-300, 160);
    turn_right(300, 570);
    //drive(-200, 650);
    //driveToLine(-300, -20);
    //drive(-200, 200);
    //turn_right(200, 920);
    
    interpolate(ARM, 1875, 80); 
    interpolate(CLAW, CLAW_OPEN, 70);
    line_follow(970);  
    interpolate(ARM, ARM_DOWN, 80); 
    interpolate(CLAW, CLAW_CLOSE, 60);
    interpolate(ARM, ARM_UP, 70); 
    turn_left(150, 1250);
    drive(200, 400);
    interpolate(ARM, ARM_DOWN, 40); 
    //interpolate(CLAW, CLAW_OPEN, 70);
    set_servo_position(CLAW, CLAW_OPEN);
    msleep(100);
    interpolate(ARM, ARM_UP, 90);  //drops 3rd supplies
}

void fourthSupplies(){   
    driveToLine(-300, -20);
    drive(-200, 200);
    turn_right(300, 570);
    
    line_follow_fast(530);
    interpolate(ARM, ARM_DOWN, 80); 
    line_follow_fast(530);
    //lfet(1);
    drive(300, 200);
    interpolate(CLAW, CLAW_CLOSE, 75);
    interpolate(ARM, ARM_UP, 70); //grabs 4th supplies
	back_follow(200); 
    turn_left(200, 1000);
    drive(200, 460);
    interpolate(ARM, ARM_DOWN, 40); 
    //interpolate(CLAW, CLAW_OPEN_PEOPLE+20, 70);
    set_servo_position(CLAW, CLAW_OPEN_PEOPLE + 150);
    msleep(100);
    interpolate(ARM, ARM_UP, 90);
    turn_right(200, 150);
    drive(-250, 600);
    driveToLine(300, 50);
    drive(-150, 300);
    turn_right(200, 880); 
    set_servo_position(ET_SERVO, ET_SHOW); 
    line_follow(600); 
    /*turn_right(200, 250);
    
    interpolate(ARM, ARM_DOWN - 100, 120);
    turn_left(200, 900);
    interpolate(ARM, 0, 220);
    turn_right(300, 320);
    interpolate(ET_SERVO, ET_SHOW, 80);
    back_follow(150); //directly onto the second poms
    msleep(10000);*/
    
}

void firstPoms(){ //UNCOMENT DRIVE AND TURN AND BACKFOLLOW
    lfet(1);
    //line_follow(450); //directly onto the second poms
    turn_right(300, 240);
    set_servo_position(ET_SERVO, ET_RUN);
    set_servo_position(CLAW, CLAW_OPEN_PEOPLE);
    interpolate(ARM, ARM_DOWN, 120);
    drive(300, 870); //300, 850
    interpolate(CLAW, CLAW_CLOSE, 90);
    set_servo_position(ARM, ARM_DOWN - 100);
    create_drive_direct(300, 300); // drive(-300, 840); 
    while(get_create_lcliff_amt() > THRESHOLD){
        msleep(5); 
    }
    create_drive_direct(-300, -300);
    msleep(150);
    //drive(-300, 840); 
    stop();
    turn_left(300, 300); 
}

void secondPoms(){ 
    interpolate(ET_SERVO, ET_SHOW, 130);
    back_follow(180); 
    lfet(-1);
    back_follow(280);
    set_servo_position(ET_SERVO, ET_RUN); 
    turn_right(300, 230); //300, 230
    set_servo_position(ARM, ARM_DOWN);
    set_servo_position(CLAW, CLAW_OPEN_PEOPLE);
    drive(300, 830);
    interpolate(CLAW, CLAW_CLOSE, 90);   
    create_drive_direct(300, 300); // drive(-300, 820); 
    while(get_create_lcliff_amt() > THRESHOLD){
        msleep(5); 
    }
    create_drive_direct(-300, -300);
    msleep(150);
    //drive(-300, 840); 
    stop();

    interpolate(ARM, ARM_UP-1000, 90); 
    set_servo_position(ET_SERVO, ET_SHOW);
    turn_left(300, 300); 
    bf_fast(900); //1460
}

void pomDump(){
    back_until_bump();
    line_follow(80);
    //back_follow(410);
    turn_left(200, 930);
	drive(-100, 1100);
	interpolate(ARM, ARM_UP - 20, 50);
    create_drive_direct(-70, -70);
    interpolateServoByTime(CLAW, CLAW_CLOSE, CLAW_OPEN + 70, 1300); 
    stop(); 
    msleep(50);
    interpolate(ARM, ARM_UP - 200, 70);
}

void thirdPoms(){

    turn_right(300, 550);
    
    //new middle pom grab
	line_follow_fast(2900); 
    lfet(-1);  
    back_follow(240);
    turn_right(300, 240);
    set_servo_position(CLAW, CLAW_OPEN + 100); 
    set_servo_position(ET_SERVO, ET_RUN);
    interpolate(ARM, ARM_DOWN, 120); 
    drive(300, 900);
    interpolate(CLAW, CLAW_CLOSE, 90); 
    create_drive_direct(300, 300); // drive(-300, 840); 
    while(get_create_lcliff_amt() > THRESHOLD){
        msleep(5); 
    }
    create_drive_direct(-300, -300);
    msleep(270);
    //drive(-300, 840); 
    stop();

   // drive(-300, 850); 
    interpolate(ARM, ARM_DOWN - 100, 90);
    turn_left(300, 260);
    
    // forth pom grab after middle
    set_servo_position(ET_SERVO, ET_SHOW);
    back_follow(300); 
    lfet(-1);
    back_follow(100); 
    set_servo_position(ET_SERVO, ET_RUN); 
    turn_right(300, 250);//300, 190
    set_servo_position(ARM, ARM_DOWN);
    set_servo_position(CLAW, CLAW_OPEN_PEOPLE);
    //interpolate(ARM, ARM_DOWN, 90); 
    drive(300, 810);
    interpolate(CLAW, CLAW_CLOSE, 90);   
    //drive(-300, 800); 
    create_drive_direct(300, 300); // drive(-300, 840); 
    while(get_create_lcliff_amt() > THRESHOLD){
        msleep(5); 
    }
    create_drive_direct(-300, -300);
    msleep(150);
    stop();
    interpolate(ARM, ARM_UP - 1000, 90); 
    set_servo_position(ET_SERVO, ET_SHOW);
    turn_left(300, 300); 
    interpolate(ET_SERVO, ET_RUN, 70); 
}

void pomDump2(){
    bf_fast(260); 
    pomDump();
  
}

void fifthpoms(){
    turn_right(300, 550);
    /* forthpoms original code*/
    //bf_fast(100); //500 
   // line_follow(2000);
    //msleep(8000);
    //bf_fast(400); //1000
    back_until_bump(); 
    drive(100, 50); 
    turn_right(300, 295);
    set_servo_position(ET_SERVO, ET_RUN);
    interpolate(ARM, ARM_DOWN, 90); 
    interpolate(CLAW, CLAW_OPEN, 90); 
    drive(300, 700);
    interpolate(CLAW, CLAW_CLOSE, 90); 
    drive(-300, 700); 
    interpolate(ARM, ARM_UP - 1000, 90);
    turn_left(300, 295);
}

void pomDump3(){
    line_follow(800);
	back_follow(380);
    pomDump();
    set_servo_position(CLAW, 230);
    drive(-250, 200);
    interpolate(ARM, ARM_DOWN, 70); 
    drive(250, 450);
    interpolate(CLAW, CLAW_CLOSE - 200, 70); 
    drive(300, 500);  /// 700
    //turn_left(200, 350);
    set_servo_position(CLAW, CLAW_OPEN);
    //interpolate(ARM, 1900, 80); 
    //set_servo_position(CLAW, CLAW_CLOSE);
    //driveToLine(-250, -30);
    drive(-350, 600);
    turn_right(300, 570);
    line_follow_fast(500);
    interpolate(ARM, ARM_UP, 100); 
    interpolate(CLAW, CLAW_OPEN, 100); 
}

void gasValve(){
    line_follow_fast(400);
    back_follow(250);
    back_until_bump(); 
    msleep(200);
    line_follow(860);
    msleep(100); //
    turn_right(300, 600);
    interpolate(ARM, ARM_DOWN - 270, 120);
    set_servo_position(CLAW, CLAW_OPEN);
    driveToLine(250, 30);
    drive(250, 410);
    interpolate(CLAW, CLAW_MID + 450, 150);
    drive(-300, 70);
    interpolate(ARM, 890, 100);
    driveToLine(-250, -30);
    turn_right(300, 1000);
    interpolate(ARM, ARM_DOWN - 20, 90);
    drive(300, 900);
    set_servo_position(CLAW, CLAW_OPEN);
    //msleep(50);
}

void powerLines(){
    //drive(-200, 400);
    driveToLine(-250, -30);
    set_servo_position(CLAW, CLAW_CLOSE);
    interpolate(ARM, 0, 270);
    drive(250, 600);
    turn_left(300, 605);
    drive(350, 680);
    drive(-300, 720);
    turn_left(250, 700);
    interpolate(ET_SERVO, ET_SHOW - 30, 60);
    //turn_right(150, 100);
    drive(-150, 1280);
    interpolate(ET_SERVO, ET_RUN + 650, 40);
    //drive(-150, 300);
    
    turn_right(70, 150);
    //drive(120, 200);
    create_drive_direct(-45, -240);
    msleep(450);
    stop();
    
    
    //drive(200, 400);
    interpolate(ET_SERVO, get_servo_position(ET_SERVO) + 260, 20);
    msleep(100);
    interpolate(ET_SERVO, get_servo_position(ET_SERVO) + 70, 20);
    turn_left(250, 750);
    drive(300, 180);
    turn_right(300, 280);
    driveToLine(300, 35);
    
    //interpolate(ET_SERVO, get_servo_position(ET_SERVO) + 70, 20);
    drive(-300, 250);
    turn_right(300, 250);
    drive(300, 750);
    create_drive_direct(-200, 0);
    msleep(800);
    stop();
    //drive(200, 100);
    interpolate(ET_SERVO, get_servo_position(ET_SERVO) - 270, 40);
    //turn_left(100, 100);
    //interpolate(ET_SERVO, ET_SHOW, 150); 
}

int main(){
    setup();

    /*
    interpolate(ET_SERVO, ET_SHOW, 60);
    msleep(1000);
    lfet(1);
    return 0;
    */
    
    init();
    wait_for_light(LIGHT_PORT);
    /*while(left_button() == 0 && right_button() == 0){
        msleep(1); 
    }*/
    shut_down_in(119);
    float x = systime();
    firstAndSecondSupplies();
    printf("FIRST AND SECOND SUPPLIES: %d\n", (int) (systime() - x));
    //msleep(4000); 
    thirdSupplies();
    //msleep(2000); 
    fourthSupplies();
    printf("FINISH DROPPPING SUPPLIES: %d\n", (int) (systime() - x));
    
    interpolate(ET_SERVO, ET_SHOW, 60); 
    set_servo_position(CLAW, CLAW_OPEN_PEOPLE + 150);
    msleep(100);
    interpolate(ARM, ARM_UP, 90); 
    
    firstPoms(); 
    secondPoms(); 
    pomDump();
    thirdPoms(); 
    pomDump2();
    fifthpoms();
    pomDump3();
    printf("DROPPED IN BIN: %d\n", (int) (systime() - x));
    //otherBin();
    gasValve();
    powerLines();
    printf("CODE DONE:%d\n", (int) (systime() - x));
    return 0;
}

