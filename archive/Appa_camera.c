#include <kipr/botball.h>

#define TRUE 1
#define FALSE 0

#define LEFT_MOTOR 3
#define RIGHT_MOTOR 0

#define CLAW_PORT 2
#define CLAW_INIT 1025
#define CLAW_OPEN 750
#define CLAW_CLOSE 450

#define RAKE_PORT 3
#define RAKE_DOWN 300
#define RAKE_UP 1670
#define RAKE_MIDDLE 435

#define HAT_BACK_LEFT 2
#define HAT_BACK_RIGHT 4
#define HAT_FRONT 5

#define LEVER 5
#define ET_LEFT 1
#define ET_RIGHT 0

#define LIGHT_PORT 3

// FUNCTIONS ---------------------------------------------------------------------------------------------------
void drive(int left, int right) {
   motor(LEFT_MOTOR, left * 0.95);
   motor(RIGHT_MOTOR, right * -1); 
}

void interpolateServo(int port, int start, int end, int time) {
    int endTime = systime() + time;
    while(systime() < endTime) {
    	set_servo_position(port, (int) ((end - start) * (1.0 - (((double) (endTime - systime())) / time)) + start));
        msleep(1);
    }
}

#define WHITE 200
#define BLACK 2000
#define THRESHOLD_FRACTION 0.2

void driveToLineWithSpeed(int speed) {
	int tophatLeft = analog(HAT_BACK_LEFT);
    int tophatRight = analog(HAT_BACK_RIGHT);
    int isTophatLeftWhite = (tophatLeft - WHITE) / (BLACK - WHITE) < 0.5;
    int isTophatRightWhite = (tophatRight - WHITE) / (BLACK - WHITE) < 0.5;
    while(isTophatLeftWhite || isTophatRightWhite) {
        drive(isTophatLeftWhite ? speed : -speed, isTophatRightWhite ? speed : -speed);
        msleep(1);
        tophatLeft = analog(HAT_BACK_LEFT);
        tophatRight = analog(HAT_BACK_RIGHT);
        isTophatLeftWhite = (tophatLeft - WHITE) / (BLACK - WHITE) < 0.5;
        isTophatRightWhite = (tophatRight - WHITE) / (BLACK - WHITE) < 0.5;
    }
}

#define SWHITE 3250
#define SBLACK 4000

void lineFollow(int left, int right, int time) {
	long endTime = systime() + time;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    while (systime() < endTime) {
        drive((percent > 0.5 ? 1 : (percent / 0.5)) * left, (percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * right);
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    }
}

void lineFollow180(int left, int right, int time) {
	long endTime = systime() + time;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    while (systime() < endTime) {
        drive((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, (percent > 0.5 ? 1 : (percent / 0.5)) * right);
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    }
}

float average(int values[], int size) {
	int i = 0;
    float sum = 0;
    //printf("[");
    for (i = 0; i < size; i++) {
        //printf("%f, ", (float) values[i]);
    	sum += (float) values[i];
    }
    //printf("]");
    //printf("[sum=%f, size=%d]", sum, size);
    return sum / ((float) size);
}

float stdSquared(int values[], int size) {
    float mean = average(values, size);
    int i = 0;
    float sum = 0;
    for (i = 0; i < size; i++) {
        float diff = ((float) values[i]) - mean;
        //printf("%f %f %d %f - ", diff, sum, values[i], mean);
        sum += diff * diff;
    }
    return sum / ((float) (size - 1));
}

void driveTillEtTemp(int left, int right, int threshold) {
    int arraySize = 50;
    int etValues[arraySize];
    int currentIndex = -1;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    long afterTime = systime() + 100; //1 second after current time
    while (systime() < afterTime || stdSquared(etValues, sizeof(etValues)/sizeof(int)) > threshold) {
        drive((percent > 0.5 ? 1 : (percent / 0.5)) * left, (percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * right);
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        
        currentIndex = (currentIndex + 1) % arraySize;
    	etValues[currentIndex] = analog(ET_RIGHT);
    }
}

void driveTillEt(int left, int right, int threshold, int sign) {
    int arraySize = 50;
    int etValues[arraySize];
    int currentIndex = -1;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    int counter = 0;
    printf("Start: %d, ", analog(ET_RIGHT));
    while (counter <= arraySize || (sign * (stdSquared(etValues, sizeof(etValues)/sizeof(int)) - threshold)) < 0) {
        drive((percent > 0.5 ? 1 : (percent / 0.5)) * left, (percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * right);
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        
        currentIndex = (currentIndex + 1) % arraySize;
    	etValues[currentIndex] = analog(ET_RIGHT);
        printf("%d -> ", analog(ET_RIGHT));
        counter++;
    }
    printf("Done\n");
}

/*void driveTillEt180(int left, int right, int threshold, int sign) {
	int arraySize = 50;
    int etValues[arraySize];
    int currentIndex = -1;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    long afterTime = systime() + 1000; //1 second after current time
    int counter = 0;
    printf("Start 180: %d, ", analog(ET_LEFT));
    drive(0, 0);
    while (systime() < afterTime || (sign * (stdSquared(etValues, sizeof(etValues)/sizeof(int)) - threshold)) < 0) {
        if (systime() >= afterTime) {
        	drive((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, (percent > 0.5 ? 1 : (percent / 0.5)) * right);
        	//printf("|%f| -> ", stdSquared(etValues, sizeof(etValues)/sizeof(int)));
            //printf("counter=%d -> ", counter);
        }
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        currentIndex = (currentIndex + 1) % arraySize;
    	etValues[currentIndex] = analog(ET_LEFT);
        //printf("%d, ", analog(ET_LEFT));
        counter++;
    }
    printf("Done\n");
}*/

void driveTillEt180(int left, int right, int threshold, int sign) {
	int arraySize = 50;
    int etValues[arraySize];
    int currentIndex = -1;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    int counter = 0;
    printf("Start 180: %d, ", analog(ET_LEFT));
    drive(0, 0);
    while (counter <= arraySize || (sign * (stdSquared(etValues, sizeof(etValues)/sizeof(int)) - threshold)) < 0) {
        drive((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, (percent > 0.5 ? 1 : (percent / 0.5)) * right);
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        currentIndex = (currentIndex + 1) % arraySize;
    	etValues[currentIndex] = analog(ET_LEFT);
        printf("%d, ", analog(ET_LEFT));
        counter++;
    }
    printf("Done\n");
}

void driveToLine(int direction) {
    driveToLineWithSpeed(100 * direction);
    drive(-20 * direction, -20 * direction);
    msleep(500);
    driveToLineWithSpeed(15 * direction);
}

void driveTillLever() {
    drive(100, 100);
    int threshold = 100; // 100 milliseconds
    long startTime = -1;
    while (1) {
        msleep(1);
        if (digital(LEVER) == 1) {
        	if (startTime == -1) {
            	startTime = systime();
            } else {
            	if (systime() - startTime > threshold) {
                	break;
                }
            }
        } else {
        	startTime = -1;
        }
    }
}

int getOrder(int timeout){
    long burning[] = {0, 0};
    long time = systime();
    while(systime() - time < timeout){
        updateCamera()
        int burningy = get_object_center(0,0).y;
        int area = get_object_area(0, 0);
        if(burningy > 60) {
            burning[0] += area * area;
        } else {
            burning[1] += area * area;
        }
        printf("%d-> ", burningy);
        msleep(150);
    }
    
    if(burning[0] > burning[1]) {
        return 0;
    } else {
        return 1;
    }
    
    camera_close();
	/*
    if (burning < 60){
         array[BURNING_ZONE] = 1;
    }
    else if(burning > 100){
        array[BURNING_ZONE] = 3;
    }
    else{
        array[BURNING_ZONE] = 1;
    }
    */
    //camera_close();
}

// END OF FUNCTIONS -----------------------------------------------------------------------------------------------------

void init(){
	enable_servo(RAKE_PORT);
    enable_servo(CLAW_PORT);
	set_servo_position(RAKE_PORT, RAKE_MIDDLE);
    set_servo_position(CLAW_PORT, CLAW_CLOSE);
}
int peopleCount = 0;
void getPeople(){
    driveTillLever();
    drive(0, 0);
    if (peopleCount != 1) {
    	interpolateServo(RAKE_PORT, RAKE_UP, RAKE_DOWN, 200); 
    }
    drive(0, 0);
	msleep(100); 
    drive(-100, -100);
    msleep(1100); 
    drive(0, 0);
//going in a second time
    if (peopleCount != 1) {
    	interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_UP, 200);
    }
    drive(0,0); 
    driveTillLever();
    drive(-60, -60);
    msleep(250);
    drive(0, 0);
   	interpolateServo(CLAW_PORT, CLAW_OPEN, CLAW_CLOSE, 500);
    driveToLine(-1);
    peopleCount++;
}
void firstSkyBridge(){
    driveToLine(1);
    drive(100, 100);
    msleep(700);
    driveToLine(1);
    drive(-100, -100);
    msleep(300);
//turns onto center line
    drive(100, -100);
    msleep(800);
    lineFollow180(100, 100, 600);
    driveTillEt180(50, 50, 950, 1);
    lineFollow180(100, 100, 1100);
    lineFollow180(100, 100, 1000);
    drive(-100, -100);
    msleep(1200);
// pushes away supplies
    drive(-100, 100);
    msleep(900);
    drive(0, 0);
	msleep(100);
    interpolateServo(CLAW_PORT, CLAW_INIT, CLAW_OPEN, 500);
    getPeople(); 
// grabs first set of people
}

void secondSkyBridge(){
    drive(-100, -100);
    msleep(540);
    drive(-100, 100);
    msleep(800);
// lines up with center line
    lineFollow(100, 100, 1700);
    driveTillEt(50, 50, 200, -1);
    drive(-100, -100);
    msleep(300); // parameter
    drive(100, -100);
    msleep(750);
    driveToLine(1);
//alines self to grab people
    drive(0, 0);
    //interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_UP, 200);
    drive(0,0);
	msleep(100);
    interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN, 500);
    getPeople();
//grabs all the people
    interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_MIDDLE, 200); 
}

void firstMedicalTrip(int direction){
    drive(-100, 0);
    msleep(1600);
    drive(0,0);
    interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN+200, 200);
    drive(0,0); 
    lineFollow(100, 100, 4200);
    driveTillEt(50, 50, 200, 1);
    if(direction == 0) {
        lineFollow(100, 100, 2800);
    	drive(100, 100);
    	msleep(650);
    } else if (direction == 1) {
        drive(100, -100);
        msleep(250);
        drive(100, 100);
        msleep(3400);
    }
//first dump to the medical center
    drive(0, 0);
    interpolateServo(CLAW_PORT, CLAW_OPEN+200, CLAW_OPEN + 300, 1000);
    drive(-100, -100);
    msleep(1600);
    drive(0, 0);
    interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_UP, 500);
    if(direction == 0) {
    	drive(-100, 100);
    	msleep(1650);
    } else if (direction == 1) {
        drive(0, 0);
        interpolateServo(CLAW_PORT, CLAW_OPEN + 300, CLAW_CLOSE, 500);
        drive(100, -100);
    	msleep(300);
        drive(-100, -100);
        msleep(1000);
        drive(100, -100);
        msleep(1100);
        drive(0, 0);
        interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN + 300, 200);
    }
    drive(0, 0);
    interpolateServo(RAKE_PORT, RAKE_UP, RAKE_DOWN, 500);
    //drives back and makes a 180 to go to third sky bridge
}

void thirdSkyBridge(){
    lineFollow180(100, 100, 500);
    drive(0, 0);
    interpolateServo(CLAW_PORT, CLAW_OPEN + 300, CLAW_OPEN, 200);
    driveTillEt180(50, 50, 200, -1);
    lineFollow180(100, 100, 800);
    driveTillEt180(50, 50, 200, 1);
    lineFollow180(100, 100, 800);
    drive(-100, 100);
    msleep(800);
//alignes itself with people in the third sky bridge
    drive(0, 0);
    interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_UP, 500);
    driveToLine(1);
    drive(0, 0);
    getPeople();
//grabs all of the people in the third sky bridge
    drive(0, 0);
    msleep(100);
    drive(-100, 0);
    msleep(1700);
    lineFollow(100, 100, 3000);
    drive(100, -100);
    msleep(800);
    driveToLine(1);
    drive(0,0);
    msleep(100); 
    //interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_UP, 200); 
//alines itself to grab people from fourth sky bridge
}

void fourthSkyBridge(int direction){
    drive(0, 0);
    interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN, 500);
    getPeople(); 
//grabs people from the fourth sky bridge
    drive(-100, 0);
    msleep(1700);
    interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_MIDDLE, 200); 
    drive(0,0);
    msleep(100);
    if (direction == 0){
        lineFollow(100, 100, 1800); 
        interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN + 600, 500);
        drive(100, 100);
        msleep(500);
	} else if (direction == 1){
        drive(100, -100);
        msleep(350);
        drive(0, 0);
        interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN + 300, 500);
        drive(100, 100);
        msleep(2700);
        drive(50, -50);
        msleep(300);
        drive(100, 100);
        msleep(300);
	}
	// push citizens in
 }



int main() {
	init();  
    wait_for_light(3);
    shut_down_in(119); 
    drive(0,0); 
    interpolateServo(RAKE_PORT, RAKE_MIDDLE, RAKE_UP, 1000);
    interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_INIT, 1000);
    firstSkyBridge();
    secondSkyBridge();  
    firstMedicalTrip(0);
    thirdSkyBridge();
    fourthSkyBridge(1); 
    return 0;
}
