#include <kipr/botball.h>

#define CLAW_SERVO 2
#define CLAW_SQUEEZE 25
#define CLAW_CLOSE 100
#define CLAW_PARALLEL 160
#define CLAW_SLIP 180
#define CLAW_OPENCUBE 350
#define CLAW_90 1300

#define LEFT_MOTOR 0
#define RIGHT_MOTOR 1

#define TOPHAT_LEFT 1
#define TOPHAT_RIGHT 0

int TOPHAT_BLACK = 3000;
int TOPHAT_GREY_LEFT, TOPHAT_WHITE_LEFT, TOPHAT_GREY_RIGHT, TOPHAT_WHITE_RIGHT;

double normalize(double minA, double maxA, double minB, double maxB, double input);

int pollSensor(int iterations, int sensorPort);

void setDrive(int left, int right);

int interpolateServoPosition(int start, int end, double percentage);
void interpolateServo(int port, int start, int end, int time);
void interpolateServos(int port, int start, int end, int port2, int start2, int end2, int time);

void lineFollowRightSensor(int timeout, int speed, int delay);
void lineFollowLeftSensor(int timeout, int speed, int delay);
void driveToLine(int speed, int delay, int timeout, int multiplier);
void driveToLineFast(int speed, int timeout, int multiplier);


int main() {
    //setup:																			//--------------------------------------------
    enable_servos();
    
    TOPHAT_WHITE_LEFT = pollSensor(20, TOPHAT_LEFT);
    TOPHAT_WHITE_RIGHT = pollSensor(20, TOPHAT_RIGHT);
    TOPHAT_GREY_LEFT = 3 * TOPHAT_WHITE_LEFT;
    TOPHAT_GREY_RIGHT = 3 * TOPHAT_WHITE_RIGHT;			    							//set white sensor value based on current sensor readings
    
    interpolateServo(CLAW_SERVO, get_servo_position(CLAW_SERVO), CLAW_PARALLEL, 1000);	//set initial servo position
    return 0; 
    //end setup																			//--------------------------------------------
}


int main_old() {
	//setup:																			//--------------------------------------------
    enable_servos();
    
    TOPHAT_WHITE_LEFT = pollSensor(20, TOPHAT_LEFT);
    TOPHAT_WHITE_RIGHT = pollSensor(20, TOPHAT_RIGHT);
    TOPHAT_GREY_LEFT = 3 * TOPHAT_WHITE_LEFT;
    TOPHAT_GREY_RIGHT = 3 * TOPHAT_WHITE_RIGHT;			    							//set white sensor value based on current sensor readings
    
    interpolateServo(CLAW_SERVO, get_servo_position(CLAW_SERVO), CLAW_PARALLEL, 1000);	//set initial servo position
    
    //end setup			
	
    driveToLineFast(1200, 8000, 1);
    setDrive(1000, 400);
    
    int counter;
    while (pollSensor(5, TOPHAT_LEFT) > 2*TOPHAT_WHITE_LEFT) {
        msleep(10);
        counter += 10;
    }
    while (pollSensor(5, TOPHAT_LEFT) < TOPHAT_BLACK) {
        msleep(10);
        counter += 10;
    }
    
    setDrive(500, 500);
    msleep(counter/4);
    
    setDrive(400, 1000);
    while (pollSensor(5, TOPHAT_RIGHT) < TOPHAT_BLACK) {
        msleep(10);
    }
    
    setDrive(0, 0);
    interpolateServo(CLAW_SERVO, CLAW_SLIP, CLAW_OPENCUBE, 250);
    setDrive(500, 500);
    interpolateServo(CLAW_SERVO, CLAW_SLIP, CLAW_CLOSE, 500);
    setDrive(200, 200);
    interpolateServo(CLAW_SERVO, CLAW_CLOSE, CLAW_SQUEEZE, 250);
    
    ao();
    return 0;

    interpolateServo(CLAW_SERVO, CLAW_SLIP, CLAW_SQUEEZE, 250);
    setDrive(-1000, -1000);
    msleep(1200);
    return 0;
    
    setDrive(1200, -1200);
    msleep(1200);
    setDrive(1200, 0);
    interpolateServo(CLAW_SERVO, CLAW_SQUEEZE, CLAW_CLOSE, 50);
    lineFollowLeftSensor(2000, 1000, 25);
    return 0;
}
    


int main_startingbox1() {
    //setup:																			//--------------------------------------------
    enable_servos();
    
    TOPHAT_WHITE_LEFT = pollSensor(20, TOPHAT_LEFT);
    TOPHAT_WHITE_RIGHT = pollSensor(20, TOPHAT_RIGHT);
    TOPHAT_GREY_LEFT = 3 * TOPHAT_WHITE_LEFT;
    TOPHAT_GREY_RIGHT = 3 * TOPHAT_WHITE_RIGHT;			    							//set white sensor value based on current sensor readings
    
    interpolateServo(CLAW_SERVO, get_servo_position(CLAW_SERVO), CLAW_CLOSE, 1000);	    //set initial servo position
    
    //end setup.																		//--------------------------------------------

    //starting box
    setDrive(1200, 1200);																//drive straight
    msleep(1200);
    setDrive(250, 250);
    interpolateServo(CLAW_SERVO, CLAW_90, CLAW_CLOSE, 2000);							//decelerate and grab firefighter on black line
    
    driveToLine(-500, 10, 1500, 1);
    setDrive(500, 500);																
    msleep(1000);
    setDrive(-1200, 1200);																
    msleep(1250);																		//drive backwards + point turn to face firefighter tube*/

    driveToLine(-500, 10, 5000, 1);														//align robot with inside of left side of starting box
    
    setDrive(1200, 1200);
    msleep(800);
    driveToLineFast(1200, 5000, 1);														//drive robot until it reaches corner of fire station / align with line
    
	return 0;
}


double normalize(double minA, double maxA, double minB, double maxB, double input) {
    if (input > maxA) input = maxA;
    if (input < minA) input = minA;
    return (double) ((minB)) + (double) ((maxB-minB) * (input-minA) / (maxA-minA));
}

int pollSensor(int iterations, int sensorPort) {
    int sum = 0;
    int n = iterations;
    
    while (n > 0) {
        sum += analog(sensorPort);
        --n;
    }
    
    return sum / iterations;
}       

void setDrive(int left, int right) {
    mav(LEFT_MOTOR, -left*0.95);
    mav(RIGHT_MOTOR, right);
}

int interpolateServoPosition(int start, int end, double percentage) {
    return start + (end - start) * percentage;
}

void interpolateServo(int port, int start, int end, int time) {
	unsigned long endTime = systime() + time;
	while (systime() < endTime) {
		set_servo_position(port, interpolateServoPosition(start, end,
			1.0f - (((double)(endTime - systime())) / ((double)time))));
		msleep(1);
	}
    set_servo_position(port, end);
}

/*void topHat(int speed){
	setDrive(speed, speed);
    while(analog(0) < TOPHAT_THRESHOLD && analog(1) < TOPHAT_THRESHOLD){
		if (analog(0) > TOPHAT_THRESHOLD){  //right tophat
        	motor(RIGHT_MOTOR, 0);
        }
        if (analog(1) > TOPHAT_THRESHOLD){ //left tophat
        	motor(LEFT_MOTOR, 0);
        } 
        msleep(1);
    }
}*/

void lineFollowRightSensor(int timeout, int speed, int delay){
    unsigned long endTime = systime() + timeout;
    unsigned long time;
    
	while ((time = systime()) < endTime) {
        double tophat = normalize(TOPHAT_WHITE_RIGHT, TOPHAT_BLACK, 0, 1, analog(TOPHAT_RIGHT));
        if (systime() % 6 == 0) {
            printf("%d -> %.2f\n", analog(TOPHAT_RIGHT), tophat);
        }

        double right = (1.0f-tophat) > 0.5f ? (1.0f-tophat) : 0.5f;
        double left = tophat > 0.5f ? tophat : 0.5f;
        setDrive(left*speed, right*speed);
        msleep(delay);
    }
    
    setDrive(0, 0);
}


void lineFollowLeftSensor(int timeout, int speed, int delay){
    unsigned long endTime = systime() + timeout;
    unsigned long time;
    
	while ((time = systime()) < endTime) {
        double tophat = normalize(TOPHAT_WHITE_LEFT, TOPHAT_BLACK, 0, 1, analog(TOPHAT_LEFT));
        if (systime() % 6 == 0) {
            printf("%d -> %.2f\n", analog(TOPHAT_LEFT), tophat);
        }

        double left = (1.0f-tophat) > 0.5f ? (1.0f-tophat) : 0.5f;
        double right = tophat > 0.5f ? tophat : 0.5f;
        setDrive(left*speed, right*speed);
        msleep(delay);
    }
    
    setDrive(0, 0);
}


void driveToLine(int speed, int delay, int timeout, int multiplier) { //it is reversed
	while (timeout > 0) {
        double tophatLeft = round(normalize(TOPHAT_WHITE_LEFT, TOPHAT_BLACK, 1, 0, pollSensor(1, TOPHAT_LEFT)));
        double tophatRight = round(normalize(TOPHAT_WHITE_RIGHT, TOPHAT_BLACK, 1, 0, pollSensor(1, TOPHAT_RIGHT)));
        if (tophatLeft < 0.1f && tophatRight < 0.1f) break;
        setDrive(speed * tophatLeft, speed * tophatRight);
        msleep(delay);
        timeout -= delay;
    }
    setDrive(0, 0);
}

void driveToLineFast(int speed, int timeout, int multiplier) {
	driveToLine(speed, 1, timeout, multiplier);
    setDrive(-speed/2, -speed/2);
    msleep(300);
    driveToLine(speed/8, 1, timeout, multiplier);
}

void interpolateServos(int port, int start, int end,
                       int port2, int start2, int end2, int time) {
    unsigned long endTime = systime() + time;
    while (systime() < endTime) {
        set_servo_position(port, interpolateServoPosition(start, end, 
        	1.0 - (((double)(endTime - systime())) / ((double)time))));
        set_servo_position(port2, interpolateServoPosition(start2, end2,
        	1.0 - (((double)(endTime - systime())) / ((double)time))));
        msleep(1);
    }
    set_servo_position(port, end);
    set_servo_position(port2, end2);
}