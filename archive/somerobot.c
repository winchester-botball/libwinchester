#include <kipr/botball.h>
#include <kipr/botball.h>

//servos
#define PRIMARY_PORT 3
#define SECONDARY_PORT 2

//servo positions
#define PRIMARY_OUT 2000
#define SECONDARY_IN 2025
#define PRIMARY_IN 25
#define SECONDARY_OUT 50

//motors
#define LEFT_DATE 3
#define RIGHT_DATE 0

//sensors
#define THRESHOLD 2000
#define LIGHT_SENSOR 0

//fields

int isLeft;

float getBatteryCharge(){
    return 100.0 * (float)get_create_battery_charge() / (float) get_create_battery_capacity();
}

void drive(int lpower, int rpower){
    create_drive_direct(-1*rpower, -1*lpower);
}
int interpolateServoPosition(int start, int end, double percentage) {
    return start + (end - start) * percentage;
}
int decellerateServoPosition(int start, int end, double percentage) {
    return start + (end - start) * pow(percentage, 0.25);
}
    
void decellerateServo(int port, int start, int end, int time){
    unsigned long endTime = systime() + time;
    while (systime() < endTime) {
        set_servo_position(port, decellerateServoPosition(start, end,
                                                          1.0 - (((double)(endTime - systime())) / ((double)time))));
    msleep(1);
    }
    set_servo_position(port, end); 
}

void interpolateServo(int port, int start, int end, int time) {
    unsigned long endTime = systime() + time;
    while (systime() < endTime) {
        set_servo_position(port, interpolateServoPosition(start, end,
                                                          1.0 - (((double)(endTime - systime())) / ((double)time))));
        msleep(1);
    }
    set_servo_position(port, end);
}

void interpolateServos(int port, int start, int end,
                       int port2, int start2, int end2, int time) {
    unsigned long endTime = systime() + time;
    while (systime() < endTime) {
        set_servo_position(port, interpolateServoPosition(start, end, 
                                                          1.0 - (((double)(endTime - systime())) / ((double)time))));
        set_servo_position(port2, interpolateServoPosition(start2, end2,
                                                           1.0 - (((double)(endTime - systime())) / ((double)time))));
        msleep(1);
    }
    set_servo_position(port, end);
    set_servo_position(port2, end2);
}
void init() {
    printf("Connecting to Create...\n");
    create_connect();
    create_full();
    printf("Battery Charge: %f%%\n", getBatteryCharge());
}
int getSign(int n, int tolerance){
	if (n > tolerance) return 1;
    else if (n < -tolerance) return -1;
    else return 0;
}
void driveToLine(int power, int adjustPower, int timeout){
    int leftSign = getSign(THRESHOLD - get_create_lcliff_amt(), 0);
    int rightSign = getSign(THRESHOLD - get_create_rcliff_amt(), 0);
    while(!(leftSign == 1 && rightSign == 1)){
    	drive(-1 * rightSign * power, -1 * leftSign * power);
        msleep(1);
        leftSign = getSign(THRESHOLD - get_create_lcliff_amt(), 0);
        rightSign = getSign(THRESHOLD - get_create_rcliff_amt(), 0);
        if (leftSign == 1 || rightSign == 1){
        	power = adjustPower;
        }
    }
    drive(0, 0);
}

void driveToLine2(int dir){
    int threshold = 2000;
    int ldir = dir;
    int rdir = dir;
    while(!(get_create_rcliff_amt() > threshold && get_create_lcliff_amt() > threshold)){
        drive(ldir, rdir);
        if(get_create_rcliff_amt() > threshold){
            rdir = 0; 
        }
        if(get_create_lcliff_amt() > threshold){
            ldir = 0;   
        }
        msleep(1);
    }
}


/*void driveToWhite(int power, int adjustPower){
    int leftSign = getSign(THRESHOLD - get_create_lcliff_amt(), 0);
    int rightSign = getSign(THRESHOLD - get_create_rcliff_amt(), 0);
    //int current = systime();
    //int final = systime() + timeout;
    while(leftSign == 0 || rightSign == 0){
    	drive(-1 * rightSign * power, -1 * leftSign * power);
        msleep(1);
        leftSign = getSign(THRESHOLD - get_create_lcliff_amt(), 0);
        rightSign = getSign(THRESHOLD - get_create_rcliff_amt(), 0);
        if (leftSign == 1 || rightSign == 1){
        	power = adjustPower;
        }
        //current = systime();
    }
    //if (current > final) printf("Shit\n");
	drive(0, 0);
}
*/

void deployArm() {
    drive(0, 0);
    drive(200, 0);
    interpolateServo(PRIMARY_PORT, PRIMARY_IN,  PRIMARY_IN/4, 150);
    drive(0, 0);
    interpolateServo(PRIMARY_PORT, PRIMARY_IN/4,  PRIMARY_OUT, 450);
    msleep(500);
    interpolateServo(SECONDARY_PORT, SECONDARY_IN, SECONDARY_OUT, 100);
    drive(100, 100);
    msleep(300);
    drive(0, 0);
    
    /*interpolateServo(PRIMARY_PORT, PRIMARY_IN, PRIMARY_OUT, 1000);
    msleep(5000);
    interpolateServo(SECONDARY_PORT, SECONDARY_IN, SECONDARY_OUT, 100);
    */
}
void doDates() {
    drive(0, 0);
	motor(RIGHT_DATE, -100);
    motor(LEFT_DATE, 100);
    //drive(100, 100);
    //msleep(300);
    drive(0, 0);
}

void starting_box(){
	int left;
    int right;
    while((left = left_button()) == 0 && (right = right_button()) == 0){
    	msleep(50);
    }
	isLeft = left == 1 ? 1 : 0;
    printf("State: %d\n", isLeft);
}

int main(){ 
    init();
    //wait_for_light(LIGHT_SENSOR);
    shut_down_in(119);
    drive(100, 100);
    msleep(300);
    drive(250, -250);
    msleep(580);
    driveToLine(200, 20, 4000);
    //driveToLine2(300);
    drive(400, 400);
    msleep(350);
    drive(-250, 250);
    msleep(460);
    drive(300, 300);
    msleep(500);
    drive(-250, 250);
    msleep(290);
    drive(450, 450);
    msleep(300);
    driveToLine(200, 20, 9999);
    drive(300, 300);
    msleep(600);
    set_servo_position(SECONDARY_PORT, SECONDARY_IN);
    set_servo_position(PRIMARY_PORT, PRIMARY_IN);
    enable_servos();
    drive(0, 0);
    msleep(1000);
    drive(100, 100);
    msleep(1500);
    
    doDates();
    
    drive(50, 50);
    msleep(300);
    drive(0, 0);
    msleep(2500); //used to be 5000
    
    drive(-80, 80);
    msleep(200);
    drive(0, 0);
    drive(25, 25);
    msleep(25);
    drive(0, 0);
    msleep(4200);
    
    drive(80, -80);
    msleep(400);
    drive(0, 0);
    drive(25, 25);
    msleep(25);
    drive(0, 0);
    msleep(1000);
    
    drive(-200, -200);
    msleep(800);
    drive(0, -200);
    msleep(150);
    drive(200, 200);
    msleep(250);
    deployArm();
    return 0;
}