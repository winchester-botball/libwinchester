#include <kipr/botball.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define TIMEOUT_ERR -5000
#define DATA_AMNT 100
#define HOST 0
#define CLIENT 1
#define PORT 636
#define BACKLOG 1

#define TRUE 1
#define FALSE 0

#define LEFT_MOTOR 3
#define RIGHT_MOTOR 0

#define CLAW_PORT 2
#define CLAW_INIT 1025
#define CLAW_OPEN 750
#define CLAW_CLOSE 450

#define RAKE_PORT 3
#define RAKE_DOWN 300
#define RAKE_UP 1670
#define RAKE_MIDDLE 435

#define HAT_BACK_LEFT 2
#define HAT_BACK_RIGHT 4
#define HAT_FRONT 5

#define LEVER 5
#define ET_LEFT 1
#define ET_RIGHT 0

#define LIGHT_PORT 3

static const char ssid[] = "3887-wallaby";
static const char password[] = "2f9fdc00";

int burningSide = 0;

int side = HOST;

int last_index = 0;
int data[DATA_AMNT];

//server variables
struct sockaddr_in server;
struct sockaddr_in dest;
int status, client_fd;
socklen_t size = sizeof(struct sockaddr_in);

//client variables
struct sockaddr_in server_info;
struct hostent *host_ent;
int socket_fd;

int initializeCommunications(int mode)
{
    //1 = client, 0 = host
    //1 = client, 0 = host
    //char text[140];
    side = mode;
    //if network setup files do not yet exist, install them

    //set up scripts
    system("sudo mkdir -p /home/root/wifi_setup");
    FILE *f = fopen("/home/root/wifi_setup/client_on.sh", "w");
    fprintf(f,"wpa_cli ter\nkillall hostapd\nwpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant-wlan0.conf\necho \"Client is now on.\"");
    fclose(f);
    system("chmod +x /home/root/wifi_setup/client_on.sh");

    f = fopen("/home/root/wifi_setup/host_on.sh", "w");
    fprintf(f,"wpa_cli ter\nkillall hostapd\nwpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant-wlan0.conf\n/usr/bin/python /usr/bin/wifi_configurator.py\necho \"Host is now on.\"");
    fclose(f);
    system("chmod +x /home/root/wifi_setup/host_on.sh");
    
    if (mode == CLIENT)
    {
        system("cd /;./home/root/wifi_setup/client_on.sh");
        return 0;
    }
    else if (mode == HOST)
    {
        system("cd /;./home/root/wifi_setup/host_on.sh");
        
        //set up socket server. sockets are much more efficient than ssh+touch lol
        socket_fd = socket(AF_INET, SOCK_STREAM, 0);
        
        struct hostent *he = gethostbyname("192.168.125.1");
        
        memset(&server, 0, sizeof(server));
        memset(&dest, 0, sizeof(dest));
        
        server.sin_family      = AF_INET;
        server.sin_port        = htons(PORT);
        //server.sin_addr.s_addr = INADDR_ANY; //DOES NOT WORK! NEEDS TO LISTEN ON SPECIFIC IP ADDRESS (OF COURSE)
        bcopy((char *)he->h_addr, (char *)&server.sin_addr.s_addr, he->h_length); //assigns an ip address to listen on
        
        
        bind(socket_fd, (struct sockaddr *)&server, sizeof(struct sockaddr));
        
        return 0;
           
    }
    else
    {
        //mode is not recognized
        return -1;
    }
}

int waitForConnection()
{
    if (side == HOST)
    {
        listen(socket_fd, BACKLOG);
        if ((client_fd = accept(socket_fd, (struct sockaddr *)&dest, &size)) == -1)
        {
            printf("Failure accepting connection.\n");
            return -1;
        }
        printf("Connection successfully made.\n");
        return 0;
    }
    else
    { return -1;}
    
}

int connectToWallaby(const char ssid[], const char psk[]) //returns -1 if connection fails, -2 if not in client mode
{
    if (side == HOST) return -2; //instead of doing this, maybe run client_on.sh
    //remove existing networks
    int i;
    
    char cmd[50];
    char set_ssid[75];
    char set_psk[75];
    
    FILE *fp;
    char response[1000];
    char temp[1000];
    char status[10];
    
    int timer = 0;
    int end = 0;

    for (i = 0; i < 10; i++)
    {
        sprintf(cmd, "wpa_cli remove_network %d", i);
        printf("Cmd: %s\n", cmd);
        system(cmd);
    }
    printf("Done... \n");
    msleep(5000);
    console_clear();
    
    //connect to given wallaby
    printf("Connecting...\n");
    system("wpa_cli add_network");
    sprintf(set_ssid, "wpa_cli set_network 0 ssid '\"%s\"'", ssid);
    sprintf(set_psk, "wpa_cli set_network 0 psk '\"%s\"'", psk);
    
    printf("Done... \n");
    msleep(5000);
    console_clear();
    
    system(set_ssid);
    system(set_psk);
    system("wpa_cli enable_network 0");
    system("wpa_cli save");
    
    //wait for connection (not sure if this works yet)
    while (!end)
    {
        printf("Processing...\n");
        strcpy(response,"");
        fp = popen("wpa_cli status", "r");
        while (fgets(temp,1000,fp))
        {
            sprintf(response,"%s%s",response,temp);
        }
        strcpy(temp,strstr(response,"wpa_state="));
        memcpy(status, &response[((int)(temp-response))+10],9); //places the status into var status
        status[9] = '\0';
        if (!strcmp(status, "COMPLETED"))
        {
            end = 1;
        }
        if (timer >= 10000)
        {
            //timed out
            end = 2;
        }
        timer += 100;
        printf("End Status: %d\n", end);
        msleep(1000);
        //console_clear();
    }
    printf("Success: end=%d\n", end);
    //if connection succeeded, assign IP addresses
    //then connect via sockets
    if (end == 1)
    {        
        system("dhclient wlan0");
        
        host_ent  = gethostbyname("192.168.125.1");
        socket_fd = socket(AF_INET, SOCK_STREAM, 0);

        memset(&server_info, 0, sizeof(server_info));
        
        server_info.sin_family = AF_INET;
        server_info.sin_port   = htons(PORT);
        //server_info.sin_addr   = *((struct in_addr *)host_ent->h_addr);
        bcopy((char *)host_ent->h_addr, (char *)&server_info.sin_addr.s_addr, host_ent->h_length);
        
        int counter = 0;
        while (connect(socket_fd, (struct sockaddr *)&server_info, sizeof(struct sockaddr)) < 0)
        {
            //printf("faefjaiwef", socket_fd, (struct sockaddr *)&server_info, sizeof(struct sockaddr));
            printf("Connection failed; attempt=%d; errno=%d\n", counter, errno);
            msleep(1000);
            counter++;
            if (counter > 10) {
                printf("Attempts failed - Exiting\n");
            	return -1;
            }
        }
        printf("Success\n");
        return 0;
    }
    return -1;
}

// FUNCTIONS ---------------------------------------------------------------------------------------------------
void drive(int left, int right) {
   motor(LEFT_MOTOR, left * 0.95);
   motor(RIGHT_MOTOR, right * -1); 
}

void interpolateServo(int port, int start, int end, int time) {
    int endTime = systime() + time;
    while(systime() < endTime) {
    	set_servo_position(port, (int) ((end - start) * (1.0 - (((double) (endTime - systime())) / time)) + start));
        msleep(1);
    }
}

#define WHITE 200
#define BLACK 2000
#define THRESHOLD_FRACTION 0.2

void driveToLineWithSpeed(int speed) {
	int tophatLeft = analog(HAT_BACK_LEFT);
    int tophatRight = analog(HAT_BACK_RIGHT);
    int isTophatLeftWhite = (tophatLeft - WHITE) / (BLACK - WHITE) < 0.5;
    int isTophatRightWhite = (tophatRight - WHITE) / (BLACK - WHITE) < 0.5;
    while(isTophatLeftWhite || isTophatRightWhite) {
        drive(isTophatLeftWhite ? speed : -speed, isTophatRightWhite ? speed : -speed);
        msleep(1);
        tophatLeft = analog(HAT_BACK_LEFT);
        tophatRight = analog(HAT_BACK_RIGHT);
        isTophatLeftWhite = (tophatLeft - WHITE) / (BLACK - WHITE) < 0.5;
        isTophatRightWhite = (tophatRight - WHITE) / (BLACK - WHITE) < 0.5;
    }
}

#define SWHITE 3500
#define SBLACK 4000

void lineFollow(int left, int right, int time) {
	long endTime = systime() + time;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    while (systime() < endTime) {
        drive((percent > 0.5 ? 1 : (percent / 0.5)) * left, (percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * right);
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    }
}

void lineFollow180(int left, int right, int time) {
	long endTime = systime() + time;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    while (systime() < endTime) {
        drive((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, (percent > 0.5 ? 1 : (percent / 0.5)) * right);
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    }
}

float average(int values[], int size) {
	int i = 0;
    float sum = 0;
    //printf("[");
    for (i = 0; i < size; i++) {
        //printf("%f, ", (float) values[i]);
    	sum += (float) values[i];
    }
    //printf("]");
    //printf("[sum=%f, size=%d]", sum, size);
    return sum / ((float) size);
}

float stdSquared(int values[], int size) {
    float mean = average(values, size);
    int i = 0;
    float sum = 0;
    for (i = 0; i < size; i++) {
        float diff = ((float) values[i]) - mean;
        //printf("%f %f %d %f - ", diff, sum, values[i], mean);
        sum += diff * diff;
    }
    return sum / ((float) (size - 1));
}

void driveTillEtTemp(int left, int right, int threshold) {
    int arraySize = 50;
    int etValues[arraySize];
    int currentIndex = -1;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    long afterTime = systime() + 100; //1 second after current time
    while (systime() < afterTime || stdSquared(etValues, sizeof(etValues)/sizeof(int)) > threshold) {
        drive((percent > 0.5 ? 1 : (percent / 0.5)) * left, (percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * right);
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        
        currentIndex = (currentIndex + 1) % arraySize;
    	etValues[currentIndex] = analog(ET_RIGHT);
    }
}

void driveTillEt(int left, int right, int threshold, int sign) {
    int arraySize = 50;
    int etValues[arraySize];
    int currentIndex = -1;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    int counter = 0;
    printf("Start: %d, ", analog(ET_RIGHT));
    while (counter <= arraySize || (sign * (stdSquared(etValues, sizeof(etValues)/sizeof(int)) - threshold)) < 0) {
        drive((percent > 0.5 ? 1 : (percent / 0.5)) * left, (percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * right);
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        
        currentIndex = (currentIndex + 1) % arraySize;
    	etValues[currentIndex] = analog(ET_RIGHT);
        printf("%d -> ", analog(ET_RIGHT));
        counter++;
    }
    printf("Done\n");
}

/*void driveTillEt180(int left, int right, int threshold, int sign) {
	int arraySize = 50;
    int etValues[arraySize];
    int currentIndex = -1;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    long afterTime = systime() + 1000; //1 second after current time
    int counter = 0;
    printf("Start 180: %d, ", analog(ET_LEFT));
    drive(0, 0);
    while (systime() < afterTime || (sign * (stdSquared(etValues, sizeof(etValues)/sizeof(int)) - threshold)) < 0) {
        if (systime() >= afterTime) {
        	drive((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, (percent > 0.5 ? 1 : (percent / 0.5)) * right);
        	//printf("|%f| -> ", stdSquared(etValues, sizeof(etValues)/sizeof(int)));
            //printf("counter=%d -> ", counter);
        }
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        currentIndex = (currentIndex + 1) % arraySize;
    	etValues[currentIndex] = analog(ET_LEFT);
        //printf("%d, ", analog(ET_LEFT));
        counter++;
    }
    printf("Done\n");
}*/

void driveTillEt180(int left, int right, int threshold, int sign) {
	int arraySize = 50;
    int etValues[arraySize];
    int currentIndex = -1;
    int tophat = analog(HAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    int counter = 0;
    printf("Start 180: %d, ", analog(ET_LEFT));
    drive(0, 0);
    while (counter <= arraySize || (sign * (stdSquared(etValues, sizeof(etValues)/sizeof(int)) - threshold)) < 0) {
        drive((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, (percent > 0.5 ? 1 : (percent / 0.5)) * right);
    	msleep(1);
        tophat = analog(HAT_FRONT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        currentIndex = (currentIndex + 1) % arraySize;
    	etValues[currentIndex] = analog(ET_LEFT);
        printf("%d, ", analog(ET_LEFT));
        counter++;
    }
    printf("Done\n");
}

void driveToLine(int direction) {
    driveToLineWithSpeed(100 * direction);
    drive(-20 * direction, -20 * direction);
    msleep(500);
    driveToLineWithSpeed(15 * direction);
}

void driveTillLever() {
    drive(100, 100);
    int threshold = 100; // 100 milliseconds
    long startTime = -1;
    while (1) {
        msleep(1);
        if (digital(LEVER) == 1) {
        	if (startTime == -1) {
            	startTime = systime();
            } else {
            	if (systime() - startTime > threshold) {
                	break;
                }
            }
        } else {
        	startTime = -1;
        }
    }
}
// END OF FUNCTIONS -----------------------------------------------------------------------------------------------------

void init(){
	enable_servo(RAKE_PORT);
    enable_servo(CLAW_PORT);
	set_servo_position(RAKE_PORT, RAKE_MIDDLE);
    set_servo_position(CLAW_PORT, CLAW_CLOSE);
}

int peopleCount = 0;
void getPeople(){
    driveTillLever();
    drive(0, 0);
    interpolateServo(RAKE_PORT, RAKE_UP, RAKE_DOWN, 200); 
    drive(0, 0);
	msleep(100); 
    drive(-100, -100);
    msleep(1100); 
    drive(0, 0);
//going in a second time
    interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_UP, 200);
    drive(0,0); 
    driveTillLever();
    drive(0,0); 
    drive(-40, -40);
    msleep(100);
    drive(0, 0);
   	interpolateServo(CLAW_PORT, CLAW_OPEN, CLAW_CLOSE, 500);
    if (peopleCount == 2) {
    	msleep(21000);
    }
	driveToLine(-1);
    peopleCount++;
}
void firstSkyBridge(){
    driveToLine(1);
    drive(100, 100);
    msleep(700);
    driveToLine(1);
    drive(-100, -100);
    msleep(300);
//turns onto center line
    drive(100, -100);
    msleep(800);
    lineFollow180(100, 100, 600);
    driveTillEt180(50, 50, 950, 1);
    lineFollow180(100, 100, 1100);
    lineFollow180(100, 100, 1200);
    drive(-100, -100);
    msleep(1200);
// pushes away supplies
    drive(-100, 100);
    msleep(900);
    drive(0, 0);
	msleep(100);
    interpolateServo(CLAW_PORT, CLAW_INIT, CLAW_OPEN, 500);
    getPeople(); 
// grabs first set of people
}

void secondSkyBridge(){
    drive(-100, -100);
    msleep(540);
    drive(-100, 100);
    msleep(800);
// lines up with center line
    lineFollow(100, 100, 1700);
    driveTillEt(50, 50, 400, -1);
    drive(-100, -100);
    msleep(250); // parameter
    drive(100, -100);
    msleep(750);
    driveToLine(1);
//alines self to grab people
    drive(0, 0);
    //interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_UP, 200);
    drive(0,0);
	msleep(100);
    interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN, 500);
    getPeople();
//grabs all the people
    drive(0, 0);
    msleep(6000);
    interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_MIDDLE, 200); 
}

void firstMedicalTrip(int direction){
    drive(-100, 0);
    msleep(1600);
    lineFollow(100, 100, 3800);
    driveTillEt(50, 50, 400, 1);
    if(direction == 0) {
        lineFollow(100, 100, 2800);
    	drive(100, 100);
    	msleep(550);
    } else if (direction == 1) {
        drive(100, -100);
        msleep(250);
        drive(100, 100);
        msleep(3400);
    }
//first dump to the medical center
    drive(0, 0);
    interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN + 300, 1000);
    drive(-100, -100);
    msleep(1500);
    drive(0, 0);
    interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_UP, 500);
    if(direction == 0) {
    	drive(-100, 100);
    	msleep(1650);
    } else if (direction == 1) {
        drive(0, 0);
        interpolateServo(CLAW_PORT, CLAW_OPEN + 300, CLAW_CLOSE, 500);
        drive(100, -100);
    	msleep(300);
        drive(-100, -100);
        msleep(1000);
        drive(100, -100);
        msleep(1100);
        drive(0, 0);
        interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN + 300, 200);
    }
    drive(0, 0);
    interpolateServo(RAKE_PORT, RAKE_UP, RAKE_DOWN, 500);
    //drives back and makes a 180 to go to third sky bridge
}

void thirdSkyBridge(){
    lineFollow180(100, 100, 500);
    drive(0, 0);
    interpolateServo(CLAW_PORT, CLAW_OPEN + 300, CLAW_OPEN, 200);
    driveTillEt180(50, 50, 400, -1);
    lineFollow180(100, 100, 800);
    driveTillEt180(50, 50, 400, 1);
    lineFollow180(100, 100, 800);
    drive(-100, 100);
    msleep(800);
//alignes itself with people in the third sky bridge
    drive(0, 0);
    interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_UP, 500);
    driveToLine(1);
    drive(0, 0);
    getPeople();
//grabs all of the people in the third sky bridge
    drive(0, 0);
    msleep(100);
    drive(-100, 0);
    msleep(1700);
    lineFollow(100, 100, 3000);
    drive(100, -100);
    msleep(800);
    driveToLine(1);
    drive(0,0);
    msleep(100); 
    //interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_UP, 200); 
//alines itself to grab people from fourth sky bridge
}

void fourthSkyBridge(int direction){
    drive(0, 0);
    interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN, 500);
    getPeople(); 
//grabs people from the fourth sky bridge
    drive(-100, 0);
    msleep(1700);
    interpolateServo(RAKE_PORT, RAKE_DOWN, RAKE_MIDDLE, 200); 
    drive(0,0);
    msleep(100);
    if (direction == 0){
        lineFollow(100, 100, 1800); 
        interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN + 600, 500);
        drive(100, 100);
        msleep(500);
	} else if (direction == 1){
        drive(100, -100);
        msleep(350);
        drive(0, 0);
        interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN + 300, 500);
        drive(100, 100);
        msleep(2700);
        drive(50, -50);
        msleep(300);
        drive(100, 100);
        msleep(300);
	}
	// push citizens in
 }

void connectWallaby() {
    printf("Connecting to ssid=%s, pass=%s: ", ssid, password);
    printf("%d\n", connectToWallaby(ssid, password));
    printf("Reading Keep Alive\n");
    int value = -1;
    int status = read(socket_fd, &value, sizeof(int));
    printf("Signal = %d; Status = %d; errno = %d/%s\n", value, status, errno, strerror(errno));
    printf("Waiting for signal... ");
    value = -1;
    status = read(socket_fd, &value, sizeof(int));
    printf("Signal = %d; Status = %d; errno = %d/%s\n", value, status, errno, strerror(errno));
    burningSide = value - 1000;
}

void checkConnection() {
    thread t = thread_create(connectWallaby);
    thread_start(t);
}


int main() {
    printf("Initializing Communication: ");
    printf("%d\n", initializeCommunications(1));
    checkConnection();
	init();
    wait_for_light(3);
    shut_down_in(119);
    drive(0, 0);
    interpolateServo(RAKE_PORT, RAKE_MIDDLE, RAKE_UP, 1000);
    msleep(1000);
    interpolateServo(CLAW_PORT, CLAW_CLOSE, CLAW_INIT, 1000);
    firstSkyBridge();
    secondSkyBridge();  
    firstMedicalTrip(burningSide);
    thirdSkyBridge();
    fourthSkyBridge(burningSide);
    return 0;
}