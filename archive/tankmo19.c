#include <kipr/botball.h>

// Constants (const int is better than define lmao)
const int SERVO_PORT = 0;
const int SERVO_UP = 2047-25;
const int SERVO_FLAT = 1200;
const int SERVO_DOWN = 0;
const int SERVO_TIME_MS = 500;

const int WHITE = 3250;
const int GREY = 3700;
const int BLACK = 4050;

const int LEFT_WHEEL_PORT = 1;
const int RIGHT_WHEEL_PORT = 0;
const int TOPHAT_PORT = 5;
const int LIGHT_PORT = 0;

int accel_x_baseline;
int gyro_y_baseline;
int gyro_z_baseline;

// Forward Declarations
typedef struct sensor sensor;
typedef int(*reading_impl)();
void setup();
void servo_up();
void servo_down();
void interpolate_servo(int port, int pos, int ms);
void drive(float left, float right, int sec);
int manualDrive(int left, int right, int sec);
void driveToLine(int speed, int tilt);
void driveToMiddleLine(int speed, int tilt);
void lineFollow(int speed, int time);
void gyro_fudge(float* left, float* right, int sensitivity);
void driveUntilUnstuck(int left, int right);
void perfectTurnLeft();
void perfectTurnRight();
void arcTurnLeft();
void arcTurnRight();
void tankmo();
void waleetka();
int take_reading(sensor* sens);
int tophat();

typedef struct sensor {
    int readings[10];
    int counter;
    void *func;
} sensor;

sensor sens_tophat = {.func=&tophat};
sensor sens_gyroz = {.func=&gyro_z};
sensor sens_accelx = {.func=&accel_x};

int take_reading(sensor* sens) {
    reading_impl sense = (reading_impl)sens->func;
    int newReading = sense();
    sens->readings[sens->counter] = newReading;
    
    int i = 0, sum = 0;
    while (i < 10) {
        sum += sens->readings[i];
        i += 1;
    }
    
    sens->counter = (sens->counter + 1) % 10;
    return sum/10;
}

void brandon() {
    msleep(250);
    drive(70, 70, 6000);
    driveToLine(70, 0);
    perfectTurnLeft(0);
    drive(70, 70, 3750);
    driveToMiddleLine(70, 0);
    drive(70, 70, 700);
    perfectTurnRight(0);
}

void tanishk() {
    msleep(15*1000);
    drive(-70, -70, 1500);
    drive(70, 70, 500);
    perfectTurnRight(0);
    drive(70, 70, 5200);
    driveToLine(70, 0);
    perfectTurnLeft(0);
    drive(70, 70, 3600);
    driveToMiddleLine(70, 0);
    drive(70, 70, 500);
    perfectTurnRight(0);
}

void jason() {
    msleep(15*1000);
    perfectTurnRight(0);
    drive(70, 70, 7000);
    driveToLine(70, 0);
    perfectTurnLeft(0);
    drive(70, 70, 3600);
    driveToMiddleLine(70, 0);
    drive(70, 70, 500);
    perfectTurnRight(0);
}

void spinner(int iterations) {
    motor(LEFT_WHEEL_PORT, 40);
    motor(RIGHT_WHEEL_PORT, 90);
    set_servo_position(SERVO_PORT, SERVO_UP);
    while (iterations > 0) {
    	interpolate_servo(SERVO_PORT, SERVO_FLAT, 200);
        interpolate_servo(SERVO_PORT, SERVO_UP, 500);
        iterations -= 1;
    }
    
    motor(LEFT_WHEEL_PORT, 0);
    motor(RIGHT_WHEEL_PORT, 0);
}

int main()
{
    setup();
    
    wait_for_light(LIGHT_PORT);
    shut_down_in(119);

    //tanishk();
    brandon();
    //jason();
    
    
    //middle line to other side
    lineFollow(70, 6000);
    drive(-70, -70, 2000);
    arcTurnLeft(500);
    drive(70, 70, 2000);
    tankmo();
    
    drive(50, 70, 1500);
   	arcTurnLeft(0);
    perfectTurnLeft(0);
    
    drive(-70, -70, 5500);
    perfectTurnLeft(0);

    drive(-50, -50, 10000);
    spinner(10);
    return 0;
}

void waleetka() {
    int counter = 20;
    set_servo_position(SERVO_PORT, 2045);
    while (counter > 0) {
        interpolate_servo(SERVO_PORT, 1650, 100);
        set_servo_position(SERVO_PORT, 2045);
        msleep(450);
        --counter;
    }
}

void driveUntilUnstuck(int left, int right) {
    motor(LEFT_WHEEL_PORT, -left);
    motor(RIGHT_WHEEL_PORT, right);
    
    while (abs(take_reading(&sens_accelx)) - accel_x_baseline < 100) {
        msleep(25);
    }
}

void driveUntilStuck(int left, int right) {
    motor(LEFT_WHEEL_PORT, -left);
    motor(RIGHT_WHEEL_PORT, right);
    
    while (abs(take_reading(&sens_accelx)) - accel_x_baseline > 30) {
        msleep(25);
    }
}
                      
int tophat() {
   return analog(TOPHAT_PORT);
}

void lineFollow(int speed, int time) {
	long endTime = systime() + time;
    int tophat = take_reading(&sens_tophat);
    float percent = ((double) (tophat - GREY)) / ((double) (BLACK - GREY));
    while (systime() < endTime) {
        motor(LEFT_WHEEL_PORT, -(percent < 0.5f ? percent*2 : 1) * speed);
    	motor(RIGHT_WHEEL_PORT, (percent > 0.5f ? (1.0f-percent)*2.0f : 1) * speed);
    	msleep(5);
        tophat = take_reading(&sens_tophat);
        percent = ((double) (tophat - GREY)) / ((double) (BLACK - GREY));
    }
}

void arcTurnLeft(int tilt) {
    printf("%i ", tilt);
    int tiltTarget = 45500+tilt;
    motor(LEFT_WHEEL_PORT, 0);
    motor(RIGHT_WHEEL_PORT, 50);
    
    int counter = 1;
    while (tiltTarget > 0) {
        motor(LEFT_WHEEL_PORT, 0);
    	motor(RIGHT_WHEEL_PORT, 50);
        tiltTarget -= (take_reading(&sens_gyroz)-gyro_z_baseline);
        msleep(10);
    }
    
    drive(0, 0, 100);
}
    
void arcTurnRight(int tilt) {
    printf("%i ", tilt);
    int tiltTarget = -44200+tilt;
    motor(LEFT_WHEEL_PORT, 50);
    motor(RIGHT_WHEEL_PORT, 0);
    
    int counter = 1;
    while (tiltTarget > 0) {
        motor(LEFT_WHEEL_PORT, 0);
    	motor(RIGHT_WHEEL_PORT, 50);
        tiltTarget -= (take_reading(&sens_gyroz)-gyro_z_baseline);
        msleep(10);
    }
    
    drive(0, 0, 100);
}

void perfectTurnLeft(int tilt) {
    printf("%i ", tilt);
    int tiltTarget = 45500+tilt;
    motor(LEFT_WHEEL_PORT, 50);
    motor(RIGHT_WHEEL_PORT, 50);
    
    int counter = 1;
    while (tiltTarget > 0) {
        motor(LEFT_WHEEL_PORT, 50);
    	motor(RIGHT_WHEEL_PORT, 50);
        tiltTarget -= (take_reading(&sens_gyroz)-gyro_z_baseline);
        msleep(10);
    }
    
    drive(0, 0, 100);
}

void perfectTurnRight(int tilt) {
    int tiltTarget = -45250+tilt;
    motor(LEFT_WHEEL_PORT, -50);
    motor(RIGHT_WHEEL_PORT, -50);
    
    int counter = 1;
    while (tiltTarget < 0) {
        motor(LEFT_WHEEL_PORT, -50);
    	motor(RIGHT_WHEEL_PORT, -50);
        tiltTarget -= (take_reading(&sens_gyroz)-gyro_z_baseline);
        msleep(10);
    }
        
    drive(0, 0, 100);
}

void gyro_fudge(float* left, float* right, int sensitivity) {
    float tilt = take_reading(&sens_gyroz) - gyro_z_baseline;
    //printf("%.2f", tilt);
    (*right) -= tilt/sensitivity;
    (*left) += tilt/sensitivity;
    if ((*right) > 100.0f) (*right) = 100.0f;
    if ((*left) > 100.0f) (*left) = 100.0f;
}

void driveToMiddleLine(int speed, int tilt) {
	float left = speed, right = speed;
    motor(LEFT_WHEEL_PORT, -left);
    motor(RIGHT_WHEEL_PORT, right);

    int reading = 0, counter = 1;
    float percent = 1.00f, decay = 1.00f;
    while (reading < BLACK && abs(percent*250) > 0) {
        if (counter % 3 == 0) {
            gyro_fudge(&left, &right, 50*decay);
            reading = take_reading(&sens_tophat);
        	percent = 0.5f - ((float)(reading-GREY) / (float)(BLACK-GREY));
        	decay *= 0.99f;
            percent *= decay;
        }
        else take_reading(&sens_gyroz);
        
        motor(LEFT_WHEEL_PORT, -percent*left);
    	motor(RIGHT_WHEEL_PORT, percent*right);
        msleep(5);
        counter += 1;
    }
    
    int direction = (tilt > 0 ? 1 : -1);
    drive(direction*50, -direction*50, direction*tilt/18);
}


void driveToLine(int speed, int tilt) {
    float left = speed, right = speed;
    motor(LEFT_WHEEL_PORT, -left);
    motor(RIGHT_WHEEL_PORT, right);

    int reading = 0, counter = 1;
    float percent = 1.00f, decay = 1.00f;
    while (reading < BLACK && abs(percent*250) > 0) {
        if (counter % 3 == 0) {
            gyro_fudge(&left, &right, 50*decay);
            reading = take_reading(&sens_tophat);
            percent = 0.5f - ((float)(reading-WHITE) / (float)(BLACK-WHITE));
            decay *= 0.99f;
            percent *= decay;
        }
        else take_reading(&sens_gyroz);
        
        motor(LEFT_WHEEL_PORT, -percent*left);
    	motor(RIGHT_WHEEL_PORT, percent*right);
        msleep(5);
        counter += 1;
    }
    
    int direction = (tilt > 0 ? 1 : -1);
    drive(direction*50, -direction*50, direction*tilt/18);
}

void tankmo() {
    drive(50, 50, 3000);      
    drive(-70, -70, 666);
    
    interpolate_servo(SERVO_PORT, SERVO_DOWN, 1000);
    msleep(1200);
    interpolate_servo(SERVO_PORT, SERVO_UP, 500);
    msleep(1000);
    
        
    drive(100, 100, 1500);
    driveUntilUnstuck(100, 100);
    drive(-70, -70, 3000);
}
        
int manualDrive(int left, int right, int ms) {
    int tilt = 0;
    motor(LEFT_WHEEL_PORT, -left);
    motor(RIGHT_WHEEL_PORT, right);
    while (ms > 0) {
        tilt += take_reading(&sens_gyroz) - gyro_z_baseline;
        ms -= 10;
        msleep(10);
    }
    
    motor(LEFT_WHEEL_PORT, 0);
    motor(RIGHT_WHEEL_PORT, 0);
    msleep(100);
    
    return tilt;
}

void drive(float left, float right, int ms) {
    int targetTime = systime() + ms;
    motor(LEFT_WHEEL_PORT, -left);
    motor(RIGHT_WHEEL_PORT, right);
    int counter = 0;
    while (systime() < targetTime) {
        if (counter % 6 == 0) {
            gyro_fudge(&left, &right, 50);
            motor(LEFT_WHEEL_PORT, -left);
    		motor(RIGHT_WHEEL_PORT, right);
        }
        
        else take_reading(&sens_gyroz);
        msleep(5);
        ++counter;
    }
    
    motor(LEFT_WHEEL_PORT, 0);
    motor(RIGHT_WHEEL_PORT, 0);
    msleep(100);
}

void setup(){
    set_servo_position(SERVO_PORT, SERVO_UP);
    enable_servo(SERVO_PORT);
    msleep(200);
    gyro_calibrate();
    accel_calibrate();
    msleep(200);
    
    int t = 1000 + systime();
    while (systime() < t) {
    	take_reading(&sens_tophat);
        take_reading(&sens_gyroz);
        take_reading(&sens_accelx);
        msleep(10);
    }
    
    gyro_z_baseline = take_reading(&sens_gyroz);
    accel_x_baseline = take_reading(&sens_accelx);
}

void interpolate_servo(int port, int pos, int ms){
    long startTime = systime();
    long endTime = startTime + ms;
    int startPos = get_servo_position(port);
    while(systime()<endTime) {
        long t = (int) (systime()-startTime);
        set_servo_position(port, startPos + ((pos-startPos)*t/ms));
        msleep(5);
    }
}

void servo_up(){
    interpolate_servo(SERVO_PORT,SERVO_UP,SERVO_TIME_MS);
}

void servo_down(){
    interpolate_servo(SERVO_PORT,SERVO_DOWN,SERVO_TIME_MS);
}