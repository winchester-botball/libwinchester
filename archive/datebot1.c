#include <kipr/botball.h>

/*
*Wiring Order:
*
*/


//TODO: tree_grab_bin doesn't grab bin

//---------------booleans---------------
#define TRUE 1
#define FALSE 0

//----------------motors----------------
#define LEFT_WHEEL					1
#define RIGHT_WHEEL					0

#define TANKMO_MOTOR				2

#define FRISBEE_WHEEL				3


//----------------servos----------------
#define CLAPPERS_PORT				0
#define CLAPPERS_OPEN				1270 //was 1225 before to ensure date bin can pass through
#define CLAPPERS_OPEN_FULL			950
#define CLAPPERS_CLOSED				1450 //1350

#define CLAW_PORT					1
#define CLAW_UP						0
#define CLAW_FLICK_FRISBEE			600
#define CLAW_DROP					900
#define CLAW_LEVEL					950
#define CLAW_DATES					1300
#define CLAW_START					1680
#define CLAW_DOWN					2040

#define BIN_CLAW_PORT				2
#define BIN_CLAW_CLOSED				1300
#define BIN_CLAW_PIPE				1080
#define BIN_CLAW_MID				700
#define BIN_CLAW_OPEN				0

/*#define TANKMO_PORT				3
#define TANKMO_UP					0
#define TANKMO_DOWN					2040
#define TANKMO_BIN					600*/

//-----------------------sensors---------------------------
#define LEFT_TOPHAT					1
#define RIGHT_TOPHAT				0
#define ET_SENSOR					4
#define LIGHT_SENSOR				5


//#define BUTTON_SENSOR				0

#define TOPHAT_THRESHOLD 2500


void setup(){
    printf("DATEBOT V1\n");
    printf("WALLABY BATTERY CHARGE: %f%%\n", 100*(double)power_level());
    set_servo_position(CLAPPERS_PORT, CLAPPERS_OPEN_FULL);
    set_servo_position(CLAW_PORT, CLAW_START);
    set_servo_position(BIN_CLAW_PORT, BIN_CLAW_MID);
    //not a servo anymore//set_servo_position(TANKMO_PORT, TANKMO_UP);
    //enable_servo(CLAW_PORT);
    //enable_servo(CLAPPERS_PORT);
    enable_servos();
}

void interpolateServoByTime(int port, int start, int end, int time){
    unsigned long endTime = systime()+time;
    while(systime()<endTime){
        set_servo_position(port, (end-start)*(1.0-(((double)(endTime-systime()))/((double)time)))+start);
        msleep(1);
    }
    set_servo_position(port, end);
}

void dualInterpolate(int portA, int portB, int startA, int startB, int endA, int endB, int time){
    unsigned long endTime = systime()+time;
    while(systime()<endTime){
        set_servo_position(portA, (endA-startA)*(1.0-(((double)(endTime-systime()))/((double)time)))+startA);
        set_servo_position(portB, (endB-startB)*(1.0-(((double)(endTime-systime()))/((double)time)))+startB);
        msleep(1);
    }
    set_servo_position(portA, endA);
    set_servo_position(portB, endB);
}

void drive(int leftspd, int rightspd, int time){
    float fudge = (leftspd < 0 ? 1 : 0.97);
    int dx = 150;	//Speed gain per increment	
    int dt = 25;		//Time per increment
    
    int accelSpeed = 0;
    int leftExtraTime = 0;
    int rightExtraTime = 0;
    
    while (time > dt) {  
        accelSpeed += dx;
        int left = fmin(abs(leftspd), accelSpeed);
        int right = fmin(abs(rightspd), accelSpeed);
        if (left != abs(leftspd)) 	leftExtraTime 	+= 	dt * (abs(leftspd) - left) 	 / abs(leftspd);
        if (right != abs(rightspd)) rightExtraTime 	+= 	dt * (abs(rightspd) - right) / abs(rightspd);
        left = (leftspd < 0 ? -left : left);
        right  = (rightspd < 0 ? -right : right);
   		mav(LEFT_WHEEL, left*fudge);
    	mav(RIGHT_WHEEL, -right);
    	msleep(dt - 2);		//Calculations take ~2msec
        time -= dt;
    }
    
    mav(LEFT_WHEEL, leftspd*fudge);
    mav(RIGHT_WHEEL, -rightspd);
    msleep(fmin(leftExtraTime, rightExtraTime) + time);
    
    if (leftExtraTime > rightExtraTime) {
        mav(LEFT_WHEEL, leftspd*fudge);
    	mav(RIGHT_WHEEL, 0);
        msleep(leftExtraTime - rightExtraTime);
    }
    else {
        mav(LEFT_WHEEL, 0);
    	mav(RIGHT_WHEEL, -rightspd);
        msleep(rightExtraTime - leftExtraTime);
    }

    freeze(LEFT_WHEEL);
    freeze(RIGHT_WHEEL);
    msleep(10);
}

void drive_direct(int speed, int time){
    float fudge = (speed < 0 ? 0.98 : 1); 
    mav(LEFT_WHEEL, speed*fudge);
    mav(RIGHT_WHEEL, -speed);
    msleep(time);
    mav(LEFT_WHEEL, 0);
    mav(RIGHT_WHEEL, 0);
    msleep(10);
}

void driving(int leftspd, int rightspd){
    float fudge = (leftspd < 0 ? 1 : 0.97);
    mav(LEFT_WHEEL, leftspd*fudge);
    mav(RIGHT_WHEEL, -rightspd);
}

void turn_left(int speed, int time){
    mav(LEFT_WHEEL, -speed*0.98);
    mav(RIGHT_WHEEL, -speed);
    msleep(time);
    freeze(LEFT_WHEEL);
    freeze(RIGHT_WHEEL);
    msleep(10);
}

void turn_right(int speed, int time){
    mav(LEFT_WHEEL, speed);
    mav(RIGHT_WHEEL, speed);
    msleep(time);
    mav(LEFT_WHEEL, 0);
    mav(RIGHT_WHEEL, 0);
    msleep(10);
}

void frisbee_spin(int time){
    mav(FRISBEE_WHEEL, -1500);
    msleep(time);
    mav(FRISBEE_WHEEL, 0);
    //msleep(200);
}


void claw_grab(){
    interpolateServoByTime(BIN_CLAW_PORT, BIN_CLAW_OPEN, BIN_CLAW_CLOSED, 1000);
}

void alignToLine(int speed){
    int tophatLeft = 1;
    int tophatRight = 1;
    int time = systime();
    
    while (tophatLeft != 0 || tophatRight != 0) {
        int tophatValueLeft = analog(LEFT_TOPHAT);
        int tophatValueRight = analog(RIGHT_TOPHAT);

        tophatLeft = (tophatValueLeft < TOPHAT_THRESHOLD ? 1 : 0);
        tophatRight = (tophatValueRight < TOPHAT_THRESHOLD ? 1 : 0);

        driving(speed * tophatLeft, speed * tophatRight);
        msleep(1);
        
        if (systime() - time > 5000) break;
    }
    
    drive(-speed, -speed, 150);
    speed /= 5;
    
    tophatLeft = 1;
    tophatRight = 1;
    
    int leftTime = 0;
    int rightTime = 0;
    
    time = systime();
    
    while (tophatLeft != 0 || tophatRight != 0) {
        int tophatValueLeft = analog(LEFT_TOPHAT);
        int tophatValueRight = analog(RIGHT_TOPHAT);

        tophatLeft = (tophatValueLeft < TOPHAT_THRESHOLD ? 1 : 0);
        tophatRight = (tophatValueRight < TOPHAT_THRESHOLD ? 1 : 0);

        driving(speed * tophatLeft, speed * tophatRight);
        msleep(1);
        if (tophatLeft == 1) leftTime++;
        if (tophatRight == 1) rightTime++;
        if (systime() - time > 5000) break;
    }
    
    driving(0, 0);
    
    int turnTime = leftTime - rightTime;
    printf("\t%i %s", turnTime, "msecs left\n");
    
    turnTime = 420 * atan2(turnTime, 210);
    
    if (turnTime < 0) {
		drive(0, speed, -turnTime);
    }
    else {
   		drive(speed, 0, turnTime);
    }
    
    return;
}

void lineFollow(int speed, int timeout, int delay){
    while (timeout > 0){
        int tophatValueLeft = analog(LEFT_TOPHAT);
        int tophatValueRight = analog(RIGHT_TOPHAT);
        printf("%d %d -> ", tophatValueLeft, tophatValueRight);
        int tophatLeft = (tophatValueLeft < TOPHAT_THRESHOLD ? 1 : -1);
        int tophatRight = (tophatValueRight < TOPHAT_THRESHOLD ? 1 : -1);
        driving(-speed * tophatRight, -speed * tophatLeft);					//jason's drives backwards
        msleep(delay);
        timeout -= delay;
        if(tophatLeft == tophatRight){timeout -= delay;}
    }
    drive_direct(0, 10);
}

void tankmo_claw_move(int spd, int time){
    mav(TANKMO_MOTOR, spd);
    msleep(time);
    mav(TANKMO_MOTOR, 0);
    msleep(200);
}

void tankmo_claw_moving(int spd){
    mav(TANKMO_MOTOR, spd);
}
    
void tankmo_claw_stop(){
    mav(TANKMO_MOTOR, 0);
    msleep(200);
}

//the code common to every tree which rakes the poms, spins the frisbee, etc
void tree_grab_base(){
    interpolateServoByTime(CLAW_PORT, CLAW_LEVEL, CLAW_DATES, 200);
    
    drive(-1500, -1500, 750);
    set_servo_position(CLAW_PORT, CLAW_DROP);
    msleep(300);
    
    driving(1500, 1500);
    interpolateServoByTime(CLAW_PORT, CLAW_DROP, CLAW_LEVEL, 300);
    msleep(500);
    driving(0, 0);
    frisbee_spin(1600);
    
    interpolateServoByTime(CLAW_PORT, CLAW_LEVEL, CLAW_DATES, 200);
    
    drive(-1500, -1500, 750);
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
}

//do the tree, leave the bin
void tree_grab(){
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_OPEN_FULL);
    
    driving(1500, 1500);
    msleep(250);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_OPEN, 750);
    driving(0, 0);
    
    tree_grab_base();
}

//do the tree, grab the bin
void tree_grab_bin() {
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_CLOSED);
    
    driving(1500, 1500);
    msleep(700);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_CLOSED, CLAPPERS_OPEN, 300);
    driving(0, 0);

    tree_grab_base();
    
    driving(1500, 1500);
    msleep(600);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN, CLAPPERS_CLOSED, 200);
    driving(0, 0);
    msleep(1050);
    drive(-1500, -1500, 1250);
}   

//similar to tree_grab() with wide open claw to catch bin
void tree_grab_first_tree(){
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_OPEN_FULL);
    msleep(500);
    
    driving(1500, 1500);
    msleep(150);
    interpolateServoByTime(CLAW_PORT, CLAW_LEVEL, CLAW_FLICK_FRISBEE, 50);
    interpolateServoByTime(CLAW_PORT, CLAW_FLICK_FRISBEE, CLAW_LEVEL, 100);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_CLOSED, 600);
    
    driving(1000, 1000);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_CLOSED, CLAPPERS_OPEN, 250);
    
    driving(0, 0);
    tree_grab_base();
}

//similar to tree_grab_bin() with wide open claw to grab bin
void tree_grab_second_tree(){
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_OPEN_FULL);
    
    driving(1500, 1500);
    msleep(750);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_CLOSED, 400);

    driving(0, 0);
    msleep(100); //300
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_CLOSED, CLAPPERS_OPEN, 200);
    
    driving(0, 0);
    tree_grab_base();
    
    driving(1500, 1500);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN, CLAPPERS_CLOSED, 200);
    msleep(700);
    driving(0, 0);
    msleep(200); //350
    drive(-1500, -1500, 1250);
    
    driving(1500, 1500);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_CLOSED, CLAPPERS_OPEN, 250);
    msleep(400);
    driving(0, 0);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN, CLAPPERS_CLOSED, 250);
    drive(-1500, -1500, 650);
}

void tree_grab_fifth_tree() {
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_OPEN_FULL);
    
    driving(1500, 1500);
    msleep(700);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_OPEN, 300);
    driving(0, 0);

    tree_grab_base();
    
    driving(1500, 1500);
    msleep(600);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN, CLAPPERS_CLOSED, 200);
    driving(0, 0);
    msleep(300);
    drive(-1500, -1500, 1250);
}

void tree_grab_last_tree(){
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_CLOSED);
    
    driving(1500, 1500);
    msleep(700);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_CLOSED, CLAPPERS_OPEN, 300);
    driving(0, 0);
    
    interpolateServoByTime(CLAW_PORT, CLAW_LEVEL, CLAW_DATES, 200);
    
    drive(-1500, -1500, 750);
    set_servo_position(CLAW_PORT, CLAW_DROP);
    msleep(300);
    
    driving(1500, 1500);
    interpolateServoByTime(CLAW_PORT, CLAW_DROP, CLAW_LEVEL, 300);
    msleep(600);
    driving(0, 0);
    frisbee_spin(1600);
    
    interpolateServoByTime(CLAW_PORT, CLAW_LEVEL, CLAW_DATES, 200);
    
    drive(-1500, -1500, 750);
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
}


int main(){
    //wait_for_light(LIGHT_SENSOR);
    //shut_down_in(121);
    setup();
    
    int time = systime();
    /*drive(1200, -300, 2000);
    printf("%i", systime() - time);
    driving(-1200, 300);
    msleep(2000);
    driving(0, 0);
    msleep(3000);
    return 0;*/
    
    set_servo_position(CLAW_PORT, CLAW_UP);
    /*drive_direct(1500, 2000);
    msleep(2000);
    drive_direct(-1500, 2000);*/
    turn_right(1500, 825*1.057);
    msleep(5000);
    turn_left(1500, 795*1.057);
    msleep(2000);
    return 0;
    
    //test program
    turn_right(1500, 830*1.057);
    msleep(2000);
    turn_left(1500, 800*1.057);
    //alignToLine(1000);
    return 0;
    
    //turn testing
    /*int onBlack = (analog(LEFT_TOPHAT) > TOPHAT_THRESOLD ? 1 : 0);
    
    while (onBlack) {
        
    
    int swaps = 3;*/
    
    
    drive(1500, 1500, 4000);
    msleep(2000);
    turn_left(1500, 850);
    msleep(1000);
    turn_right(1500, 850);
    msleep(1000);
    drive(-1500, -1500, 4000);
    msleep(1000);
    return 0;

    return 0;
