#include <kipr/botball.h>
#define CLAW_PORT 0
#define CLAW_OPEN 1200
#define CLAW_START 750
#define CLAW_CITIZENS 750
#define CLAW_CLOSE 550
#define CLAW_CUBE 750

#define WRIST_PORT 2
#define WRIST_DOWN 1500
#define WRIST_SKY_MID_CUBE 900 
#define WRIST_SKY_MID 1300
#define WRIST_SKY_SIDE 750
#define WRIST_CUBE 1450
#define WRIST_CITIZENS 0
#define WRIST_UP 0

#define PULLEY_PORT 0
#define PULLEY_START 3000
#define PULLEY_UP 4060
#define PULLEY_SIDE 3940
#define PULLEY_DOWN 1200
#define PULLEY_CUBE 2900
#define PULLEY_CITIZENS 3000 // Actually slightly above bridges because we motor down
#define PULLEY_BEFORE_CITIZENS 3700

#define ARM_PORT 1
#define ARM_UP 650
#define ARM_READY 800
#define ARM_DOWN 1650

#define NET_PORT 3
#define NET_CLOSE 400
#define NET_OPEN 1400


#define CAMERA_THRESHOLD 10



// Analog Sensors
#define LINEAR_PORT 4
#define ET_PORT 0
#define TOPHAT_PORT 1
#define LIGHT_PORT 5
// Digital Sensors
#define LEVER_PORT 0

//Boolean
#define TRUE 1
#define FALSE 0



#define THRESHOLD 2000// this needs to be changed back to 2000 for black tape instead of the dark grey

void drive(int left, int right){ 
    create_drive_direct(left, right);
}

void stop(){
    drive(0, 0);
}

#define IRWHITE 2800 + 400
#define IRBLACK 600 - 400

#define SWHITE 3300 - 100
#define SBLACK 4000 + 100

void lineFollow(int left, int right, int time) {
    long endTime = systime() + time;
    int tophat = analog(TOPHAT_PORT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    printf("lineFollow: ");
    while (systime() < endTime) {
        drive(-(percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, -(percent > 0.5 ? 1 : (percent / 0.5)) * right);
        msleep(1);
        tophat = analog(TOPHAT_PORT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        printf("%d, ", analog(ET_PORT));
        //printf("%f->", percent);
    }
    printf("\n");
}
void fastLineFollow(int left, int right, int time) {
    long endTime = systime() + time;
    int tophat = analog(TOPHAT_PORT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    printf("lineFollow: ");
    while (systime() < endTime) {
        drive(-(percent < 0.5 ? 1 : 0.8) * left, -(percent > 0.5 ? 1 : 0.8) * right);
        msleep(1);
        tophat = analog(TOPHAT_PORT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        printf("%d, ", analog(ET_PORT));
        //printf("%f->", percent);
    }
    printf("\n");
}


void lineFollow180(int left, int right, int time) {
    long endTime = systime() + time;
    int ir = get_create_rfcliff_amt();
    double percent = ((double) (ir - IRWHITE)) / ((double) (IRBLACK - IRWHITE));
    printf("linefollow180: ");
    while (systime() < endTime) {
        drive(((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left) * 0.95, ((percent > 0.5 ? 1 : (percent / 0.5)) * right) * 0.85);
        msleep(1);
        ir = get_create_rfcliff_amt();

        printf("%d, ", analog(ET_PORT));
        percent = ((double) (ir - IRWHITE)) / ((double) (IRBLACK - IRWHITE));
    }
    printf("\n");
}

float average(float values[], int size) {
    int i = 0;
    float sum = 0;
    for (i = 0; i < size; i++) {
        sum += values[i];
    }
    return sum / ((float) size);
}

float stdSquared(float values[], int size) {
    float mean = average(values, size);
    int i = 0;
    float sum = 0;
    for (i = 0; i < size; i++) {
        float diff = values[i] - mean;
        sum += diff * diff;
    }
    return sum / ((float) (size - 1));
}


#define ONOFF -1
#define OFFON 1


#define ET_THRESHOLD 625

void driveTillET(int left, int right, int sign) {
    int arraySize = 50;
    float etValues[arraySize];
    int currentIndex = -1;
    int tophat = analog(TOPHAT_PORT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    int counter = 0;
    printf("Start: %d, ", analog(ET_PORT));
    while (counter <= arraySize || (sign * (stdSquared(etValues, sizeof(etValues)/sizeof(int)) - ET_THRESHOLD)) < 0) {
        drive(-(percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, -(percent > 0.5 ? 1 : (percent / 0.5)) * right);
        msleep(1);
        tophat = analog(TOPHAT_PORT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        currentIndex = (currentIndex + 1) % arraySize;
        float currentET = 100000.0 * powf((float)analog(TOPHAT_PORT), (float) -.652);
    	etValues[currentIndex] = currentET;
        printf("%f -> ", currentET);
        counter++;
    }
    printf("Done\n");
}

void driveTillET180(int left, int right, int sign) {
    int arraySize = 50;
    float etValues[arraySize];
    int currentIndex = -1;
    int ir = get_create_rfcliff_amt();
    double percent = ((double) (ir - IRWHITE)) / ((double) (IRBLACK - IRWHITE));
    int counter = 0;
    printf("Start: %d, ", analog(ET_PORT));
    while (counter <= arraySize || (sign * (stdSquared(etValues, sizeof(etValues)/sizeof(int)) - ET_THRESHOLD)) < 0) {
        drive(((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left), ((percent > 0.5 ? 1 : (percent / 0.5)) * right) * 0.85);
        msleep(1);
        ir = get_create_rfcliff_amt();
        percent = ((double) (ir - IRWHITE)) / ((double) (IRBLACK - IRWHITE));
        currentIndex = (currentIndex + 1) % arraySize;
        float currentET =  100000.0 *(float) powf(analog(TOPHAT_PORT),(float) -.652);
    	etValues[currentIndex] =  currentET;
        printf("%f -> ", currentET);
        counter++;
    }
    printf("Done\n");
}


void driveTillET180Short(int left, int right, int sign) {
    int ir = get_create_rfcliff_amt();
    int et = analog(ET_PORT);
    double percent = ((double) (ir - IRWHITE)) / ((double) (IRBLACK - IRWHITE));
    printf("driveTillET180: %d, ", et);
    while (sign * (et - 1500) < 0) {
        drive(((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left), ((percent > 0.5 ? 1 : (percent / 0.5)) * right) * 0.85);
        msleep(1);
        ir = get_create_rfcliff_amt();
        et = analog(ET_PORT);
        percent = ((double) (ir - IRWHITE)) / ((double) (IRBLACK - IRWHITE));
        printf("%d, ", et);
    }
    printf("Done\n");
}

int getSign(int n, int tolerance){
    if (n > tolerance) {
        return 1;
    } else if (n < -tolerance) {
        return -1;
    } else {
        return 0;
    }
}

void driveToLineTimeout(int power, int adjustPower, int timeout){
    long endTime = systime() + timeout;
    int leftPower = power;
    int rightPower = power;
    int leftSign = getSign(THRESHOLD - get_create_lcliff_amt(), 0);
    int rightSign = getSign(THRESHOLD - get_create_rcliff_amt(), 0);
    while((!(leftSign == 1 && rightSign == 1)) && (systime() < endTime)) {
        drive(leftSign * leftPower, rightSign * rightPower);
        msleep(1);
        leftSign = getSign(THRESHOLD - get_create_lcliff_amt(), 0);
        rightSign = getSign(THRESHOLD - get_create_rcliff_amt(), 0);
        if (leftSign == 1){
            leftPower = adjustPower;
        }
        if(rightSign == 1) {
            rightPower = adjustPower;
        }
        if(leftPower == adjustPower && rightPower == adjustPower){
            break;
        }
    }
    drive(0, 0);
}

void driveToLine(int power, int adjustPower){
    int leftPower = power;
    int rightPower = power;
    int leftSign = getSign(THRESHOLD - get_create_lcliff_amt(), 0);
    int rightSign = getSign(THRESHOLD - get_create_rcliff_amt(), 0);
    while(!(leftSign == 1 && rightSign == 1)) {
        drive(leftSign * leftPower, rightSign * rightPower);
        msleep(1);
        leftSign = getSign(THRESHOLD - get_create_lcliff_amt(), 0);
        rightSign = getSign(THRESHOLD - get_create_rcliff_amt(), 0);
        if (leftSign == 1){
            leftPower = adjustPower;
        }
        if(rightSign == 1) {
            rightPower = adjustPower;
        }
        if(leftPower == adjustPower && rightPower == adjustPower){
            break;
        }
    }
    drive(0, 0);
}

int workingCamera = 0;

void flagCamera() {
    if(camera_update()==1){
        printf("Camera is working\n");
        workingCamera = 1;
    }
}

void checkCamera() {
    thread t = thread_create(flagCamera);
    thread_start(t);
}

int updateCamera() {
    if(workingCamera) {
        return camera_update();
    } else {
        return 0;
    }
}

void checkStatus(int status, char failureMessage []){
    if(status == 0) {
        //printf(failureMessage);
    }
}

void warmupCamera(int interval, int timeout){
    printf("Warmup Camera: ");
    long time = systime();
    while(systime() - time < timeout){
        checkStatus(updateCamera(), "Fail -> ");
        msleep(interval);
    }
    printf("Done\n");
}

void interpolateServoByTime(int port, int start, int end, int time){
    unsigned long endTime = systime() + time;
    while(systime() < endTime){
        set_servo_position(port, (end - start) * (1.0 - (((double)(endTime - systime())) / ((double)time))) + start);
        msleep(1);
    }
    set_servo_position(port, end);
}

void interpolatePulleyWithTolerance2(int power, int target, int tolerance) {
    int currentLinear = analog(LINEAR_PORT);
    int prevLinear = currentLinear;
    int sign = getSign(currentLinear - target, tolerance);
    int currentSign = sign;

    int warmup = 1000;
    int pushIndex = 0;
    int deltaArray[500] = {[0 ... 499] = 9999};
    int arraySize = (sizeof(deltaArray) / sizeof(int*));
    int totalDelta = deltaArray[0] * arraySize;

    motor(PULLEY_PORT, sign * power);
    while (currentSign == sign) {
        msleep(1);
        prevLinear = currentLinear;
        currentLinear = analog(LINEAR_PORT);
        if (warmup > 0) {
            warmup -= 1;
        } else {
            totalDelta -= deltaArray[pushIndex];
            deltaArray[pushIndex] = abs(prevLinear - currentLinear);
            totalDelta += deltaArray[pushIndex];
            pushIndex = (pushIndex + 1) % arraySize;
            if (totalDelta < 1 * arraySize) {
                printf("==========[Breaking]========== %d LOL\n", totalDelta);
                //motor(PULLEY_PORT, 0);
                //interpolatePulleyWithTolerance(power, target, tolerance);
                break;
            }
        }
        currentSign = getSign(currentLinear - target, tolerance);
    }
    motor(PULLEY_PORT, 0);
}

void interpolatePulleyWithTolerance(int power, int target, int tolerance) {
    int currentLinear = analog(LINEAR_PORT);
    int prevLinear = currentLinear;
    int sign = getSign(currentLinear - target, tolerance);
    int currentSign = sign;

    int warmup = 1000;
    int pushIndex = 0;
    int deltaArray[500] = {[0 ... 499] = 9999};
    int arraySize = (sizeof(deltaArray) / sizeof(int*));
    int totalDelta = deltaArray[0] * arraySize;

    motor(PULLEY_PORT, sign * power);
    while (currentSign == sign) {
        msleep(1);
        prevLinear = currentLinear;
        currentLinear = analog(LINEAR_PORT);
        if (warmup > 0) {
            warmup -= 1;
        } else {
            totalDelta -= deltaArray[pushIndex];
            deltaArray[pushIndex] = abs(prevLinear - currentLinear);
            totalDelta += deltaArray[pushIndex];
            pushIndex = (pushIndex + 1) % arraySize;
            if (totalDelta < 1 * arraySize) {
                printf("==========[Breaking]========== %d\n", totalDelta);
                motor(PULLEY_PORT, -sign * power);
                msleep(arraySize);
                //motor(PULLEY_PORT, 0);
                //interpolatePulleyWithTolerance(power, target, tolerance);
                break;
            }
        }
        currentSign = getSign(currentLinear - target, tolerance);
    }
    motor(PULLEY_PORT, 0);
}

void interpolatePulley(int target) {
    if (target < 2500) {
        interpolatePulleyWithTolerance(100, 2500, 0);
        motor(PULLEY_PORT, 100);
        msleep(500 * (((float) (2500 - target)) / ((float)(2500 - PULLEY_DOWN))));
    } else if (target > 4000) {
        interpolatePulleyWithTolerance(100, 4000, 0);
        motor(PULLEY_PORT, -100);
        msleep(500 * (((float) (target - 4000)) / ((float)(PULLEY_UP - 4000))));
    } else {
        interpolatePulleyWithTolerance(100, target, 0);
    }
    motor(PULLEY_PORT, 0);
}

void resetPosition() {
    drive(0, 0);
    motor(PULLEY_PORT, -100);
    msleep(500);
    drive(250, -250);
    msleep(700);
    motor(PULLEY_PORT, 0);
    interpolateServoByTime(WRIST_PORT, get_servo_position(WRIST_PORT), WRIST_SKY_MID, 500);
    
    drive(0, 0);
    interpolatePulleyWithTolerance2(100, PULLEY_UP, 0);
    drive(250, -250);
    msleep(300);
    drive(0,0);
    driveToLine(200, 40);
}

void checkET(int left, int right, int time){
    interpolatePulley(PULLEY_UP);
    interpolateServoByTime(ARM_PORT, get_servo_position(ARM_PORT), ARM_UP, 300);
    long endTime = systime() + time;
    int tophat = analog(TOPHAT_PORT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    printf("ET: ");
    while (systime() < endTime) {
        drive(-(percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, -(percent > 0.5 ? 1 : (percent / 0.5)) * right);
        msleep(1);
        tophat = analog(TOPHAT_PORT);
        percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        printf("%d, ", analog(ET_PORT));
        //printf("%f->", percent);
    }
    printf("\n");
}


void setup() {
    create_connect();
    create_full();

    printf("Create Battery %f%%\n", ((float)get_create_battery_charge()) / ((float)get_create_battery_capacity()) * 100.0);
    printf("Charging: %s\n", get_create_battery_charging_state() ? "true" : "false");

    enable_servos();
    printf("Enabled Servos\n");
    
    
    drive(0, 0);
    interpolateServoByTime(NET_PORT, get_servo_position(NET_PORT), NET_OPEN, 200);
    interpolateServoByTime(CLAW_PORT, get_servo_position(CLAW_PORT), CLAW_START, 200);
    interpolatePulley(PULLEY_START);
    interpolateServoByTime(ARM_PORT, get_servo_position(ARM_PORT), ARM_READY, 500);
    interpolateServoByTime(NET_PORT, NET_OPEN, NET_CLOSE, 200);
    interpolateServoByTime(WRIST_PORT, get_servo_position(WRIST_PORT), WRIST_DOWN, 200);
    msleep(1000);
}

void full_setup() {
    set_servo_position(WRIST_PORT, 1000);
    interpolatePulley(PULLEY_UP);
    set_servo_position(NET_PORT, NET_OPEN);
    set_servo_position(CLAW_PORT, CLAW_START);
    set_servo_position(ARM_PORT, ARM_UP); // Low Pos is 300
    msleep(500);
    set_servo_position(WRIST_PORT, WRIST_DOWN);
    set_servo_position(CLAW_PORT, CLAW_CLOSE);
    interpolateServoByTime(NET_PORT, get_servo_position(NET_PORT), NET_OPEN, 200);
    interpolateServoByTime(CLAW_PORT, get_servo_position(CLAW_PORT), CLAW_CLOSE, 200);
    interpolatePulley(PULLEY_START);
    interpolateServoByTime(ARM_PORT, get_servo_position(ARM_PORT), ARM_READY, 200);
    interpolateServoByTime(NET_PORT, NET_OPEN, NET_CLOSE, 200);
    interpolateServoByTime(WRIST_PORT, get_servo_position(WRIST_PORT), WRIST_DOWN, 200);
}

#define CHANNEL 0

int getObjectX(int threshold) {
	checkStatus(updateCamera(), "Failed to Update Camera - getObjectX()\n");
    int totalX = 0;
    int totalArea = 0;
    int i = 0;
    for (i = 0; i < get_object_count(CHANNEL); i++) {
        int centerX = get_object_center(CHANNEL, i).x;
    	int area = get_object_area(CHANNEL, i);
        if (area >= threshold) {
            totalX += centerX * area;
            totalArea += area;
            printf("Obj[x=%d, area=%d] -> ", centerX, area);
        }
    }
    printf("ObjectX[avg=%f]\n", totalArea == 0 ? -1 : ((double) totalX) / ((double) totalArea));
    if (totalArea == 0) {
    	return -1;
    } else {
    	return ((double) totalX) / ((double) totalArea);
    }
}


int getObjectX2(int threshold) {
	checkStatus(updateCamera(), "Failed to Update Camera - getObjectX()\n");
    int totalX = 0;
    int totalArea = 0;
    int i = 0;
    for (i = 0; i < get_object_count(CHANNEL); i++) {
        int centerX = get_object_center(CHANNEL, i).x;
    	int area = get_object_area(CHANNEL, i);
        if (area >= threshold && centerX > 30 && centerX < 130) {
            totalX += centerX * area;
            totalArea += area;
            printf("Obj[x=%d, area=%d] -> ", centerX, area);
        }
    }
    printf("ObjectX[avg=%f]\n", totalArea == 0 ? -1 : ((double) totalX) / ((double) totalArea));
    if (totalArea == 0) {
    	return -1;
    } else {
    	return ((double) totalX) / ((double) totalArea);
    }
}

int getOrder(){
    int threshold = CAMERA_THRESHOLD;
    drive(100, -100);
    msleep(190);
    drive(0, 0);
    // search mid & right
    msleep(300);
    warmupCamera(0, 300);
    checkStatus(updateCamera(), "Failed to Update Camera - getObjectX()\n");
    int totalXMidright = 0;
    int totalAreaMidright = 0;
    int i = 0;
    for (i = 0; i < get_object_count(CHANNEL); i++) {
        int centerX = get_object_center(CHANNEL, i).x;
    	int area = get_object_area(CHANNEL, i);
        if (area >= threshold) {
            totalXMidright += centerX * area;
            totalAreaMidright += area;
            printf("Obj[x=%d, area=%d] -> ", centerX, area);
        }
    }
    printf("ObjectX[avg=%f]\n", totalAreaMidright == 0 ? -1 : ((double) totalXMidright) / ((double) totalAreaMidright));
    double averageXMidright = (totalAreaMidright == 0 ? -1 : ((double) totalXMidright) / ((double) totalAreaMidright));
    drive(-200, 200);
    msleep(450);
    drive(0, 0);
    // search left
    msleep(300);
    warmupCamera(0, 300);
    checkStatus(updateCamera(), "Failed to Update Camera - getObjectX()\n");
    int totalXLeft = 0;
    int totalAreaLeft = 0;
    i = 0;
    for (i = 0; i < get_object_count(CHANNEL); i++) {
        int centerX = get_object_center(CHANNEL, i).x;
    	int area = get_object_area(CHANNEL, i);
        if (area >= threshold && centerX > 30 && centerX < 130) {
            totalXLeft += centerX * area;
            totalAreaLeft += area;
            printf("Obj[x=%d, area=%d] -> ", centerX, area);
        }
    }
    printf("ObjectX[avg=%f]\n", totalAreaLeft == 0 ? -1 : ((double) totalXLeft) / ((double) totalAreaLeft));
    
    double averageXLeft = (totalAreaLeft == 0 ? -1 : ((double) totalXLeft) / ((double) totalAreaLeft));
    drive(100, -100);
    msleep(710);
    drive(0, 0);
    int midRight = (int) averageXMidright;
    int left = (int) averageXLeft;
    printf("[midRight=%d, left=%d]\n", midRight, left);
    
    if (totalAreaLeft > totalAreaMidright) {
    	return 0;
    } else {
    	return midRight < 60 ? 1 : 2;
    }
    //return (left != -1) ? 0 : (midRight < 60 ? 1 : 2);
}

int getOrderMedical() {
    drive(0, 0);
    printf("getOrderMedical()\n");
    return getObjectX(0) < 70 ? 0 : 1;
}

int startingBox() {
    drive(0, 0);
    printf("Opening Camera -> ");
    checkStatus(camera_open_black(), "Failure to open Camera -> ");
    checkCamera();
    printf("Done\n");
    interpolateServoByTime(NET_PORT, NET_CLOSE, NET_OPEN, 200);
    interpolateServoByTime(WRIST_PORT, WRIST_DOWN, WRIST_SKY_MID, 200);
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN, 200);
    interpolatePulley(PULLEY_UP);
    interpolateServoByTime(ARM_PORT, ARM_READY, ARM_UP, 100);
    drive(-200, -200);
    msleep(900); // Was 1200. Needs to be variable.
    //look at medical centers + send to other robot
    drive(0, 0);
    warmupCamera(0,300);
    int tempMedical = getOrderMedical();
    drive(200, -200);
    msleep(700);
    driveToLine(200, 20); // Move to bounding box
    return tempMedical;
}

void midSky() {
    drive(-200, -200); // Drive up to skyscraper
    motor(PULLEY_PORT, 20); //lower the pulley a little bit so that it can align
    msleep(420);
    motor(PULLEY_PORT, 0);
    drive(0, 0);
    drive(50, -50);
    msleep(70);
    drive(0,0);
    
    
    interpolateServoByTime(CLAW_PORT, CLAW_OPEN, CLAW_CLOSE, 400); // Grab Botguy
    msleep(300);
    interpolateServoByTime(WRIST_PORT, WRIST_SKY_MID, WRIST_UP, 300); // Lifts Wrist
    motor(PULLEY_PORT, -100);
    msleep(300);
    motor(PULLEY_PORT, 0);
    drive(-50, 50);
    msleep(70);
    driveToLine(-200, -50); // Back up to line
    drive(300, 300);
    msleep(333);
    drive(-300, 300);
    msleep(1267);
    drive(150, 150);
    msleep(300);
    driveToLine(200, 20);
    motor(PULLEY_PORT, 100);
    drive(-150, -150);
    msleep(500);
    motor(PULLEY_PORT, 0);
    drive(0, 0);
    interpolatePulley(PULLEY_START);
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN, 400);
}

void leftSky() {
    drive(-150, 150);
    motor(PULLEY_PORT, -20); // Motor up
    interpolateServoByTime(WRIST_PORT, WRIST_SKY_MID, WRIST_SKY_SIDE, 400);
    msleep(100);
    motor(PULLEY_PORT, 0);
    drive(0, 0);
    interpolatePulleyWithTolerance(50, PULLEY_SIDE, 0);
    drive(-200, -200);
    msleep(300);
    drive(0, 0);
    interpolateServoByTime(CLAW_PORT, CLAW_OPEN, CLAW_CLOSE, 400); // Grab Mayor
    interpolatePulley(PULLEY_UP);
    drive(200, 200);
    msleep(200);
    drive(-210, 210);
    msleep(1450);
    driveToLine(200, 20);
    motor(PULLEY_PORT, 60); // motor down
    drive(-150, -150);
    msleep(500);
    drive(150, -150);
    msleep(200);
    motor(PULLEY_PORT, 0);
    drive(0, 0);
    interpolatePulley(PULLEY_START);
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN, 500);
    drive(-150, 150);
    msleep(200);
}

void rightSky() {
    drive(150, -150);
    motor(PULLEY_PORT, -20);
    interpolateServoByTime(WRIST_PORT, WRIST_SKY_MID, WRIST_SKY_SIDE, 400);
    msleep(110);
    motor(PULLEY_PORT, 0);
    drive(0, 0);
    interpolatePulleyWithTolerance(50, PULLEY_SIDE, 0);
    drive(-200, -200);
    msleep(500);
    drive(0, 0);
    interpolateServoByTime(CLAW_PORT, CLAW_OPEN, CLAW_CLOSE, 400); // Grab Mayor
    //interpolateServoByTime(WRIST_PORT, WRIST_SKY_SIDE, WRIST_UP, 500);
    interpolatePulley(PULLEY_UP);
    drive(200, 200);
    msleep(500);
    drive(210, -210);
    msleep(1200);
    driveToLine(200, 20);
    motor(PULLEY_PORT, 60);
    drive(-150, -150);
    msleep(500);
    //drive(-150, 150);
    //msleep(200);
    motor(PULLEY_PORT, 0);
    drive(0, 0);
    interpolatePulley(PULLEY_START);
    interpolateServoByTime(CLAW_PORT, CLAW_CLOSE, CLAW_OPEN, 500);
    //drive(150, -150);
    //msleep(200);
    drive(0, 0);
}

void grabCitizens() {
    if (get_servo_position(NET_PORT) == NET_CLOSE) {
    	drive(0, 0);
    	interpolateServoByTime(NET_PORT, NET_CLOSE, NET_OPEN, 400);
    }
    driveToLineTimeout(200, 15, 6000);
    long endTime = systime() + 10000; // 10 second timeout
    drive(-200, -200);
    while ((digital(LEVER_PORT) == FALSE) && (systime() < endTime)) {
        msleep(1);
    }
    drive(0, 0);
    if (analog(LINEAR_PORT) > PULLEY_CITIZENS + 500) {
    	interpolatePulley(PULLEY_CITIZENS + 500);
    }
    interpolatePulleyWithTolerance(50, PULLEY_CITIZENS, 0);
    motor(PULLEY_PORT, 50); // Goes down
    msleep(100);
    motor(PULLEY_PORT, 0);
    interpolateServoByTime(NET_PORT, NET_OPEN, NET_CLOSE, 250);
    driveToLine(-200, -20);
    drive(200, 200);
    msleep(350);
    motor(PULLEY_PORT, -100);
    msleep(250);
    motor(PULLEY_PORT, 0);
    drive(0, 0);
    interpolatePulley(PULLEY_UP);
}

void dropCitizens(int medical) {
	//BURNING CLOSE TO START BOX
    drive(0, 0);
    if(medical == 1){ //go to non-burning one
        drive(200, -200);
        msleep(300);
  
        drive(-200, -200);
        msleep(500);
    	drive(-200, 200);
    	msleep(300);
    
    	drive(-300, -300);
    	msleep(400);
        drive(-50, 50);
        msleep(100);
    	drive(0, 0);
    	interpolateServoByTime(ARM_PORT, ARM_DOWN, ARM_UP, 300);
        drive(50, -50);
        msleep(100);
        //lineFollow(200,200,100);
    }
    //BURNING FAR
    if(medical == 0){
    	drive(-100, -300);
    	msleep(1050);
    	drive(0,0);
    	drive(-200, -200);
    	msleep(500);
    	drive(0, 0);
 
    	interpolateServoByTime(ARM_PORT, ARM_DOWN, ARM_UP, 500);
          
    	drive(200, 200);
    	msleep(400);
    
    	drive(0, 200);
    	msleep(1000);
    }
}

void dropCitizens2(int medical){
	drive(300, 300);
    msleep(300);
    drive(-300, 300);
    msleep(700);
    if(medical ==  1){
        drive(-300, -300);
        msleep(600);
        drive(80, -80);
        msleep(100);
    }
    if (medical == 0){
        drive(-100,-400);
        msleep(1000);
        drive(-300,-300);
        msleep(350);
    }
    

}

int main(){
    printf("Monstrosity 13\n");
    create_connect();
    create_full();

    printf("Create Battery %f%%\n", ((float)get_create_battery_charge()) / ((float)get_create_battery_capacity()) * 100.0);
    printf("Charging: %s\n", get_create_battery_charging_state() ? "true" : "false");

    enable_servos();
    
    full_setup();
    
    //wait_for_light(LIGHT_PORT);
    //setup();
    
    long startTime = systime();
    
    drive(0, 0);
    shut_down_in(119);
    printf("Start\n");
    msleep(1000);

    drive(0, 0);

    int medical = 0;
    int skyscrapers = 0;
    
    medical = startingBox();

    skyscrapers = getOrder();
	camera_close();
    drive(-200, -200);
    msleep(400);
    driveToLine(200, 20); // Drive to middle line

    printf("Burning Skyscraper: %d\n", skyscrapers);
    printf("Burning Medical: %d\n", medical); // 0 is closer to starting box, 1 is further

    //skyscrapers = 1; // Temporarily force burning building to zone 2
    //skyscrapers = 1;
    drive(0,0);
    msleep(300);
    if(skyscrapers==0) {
        midSky();
        resetPosition();
        rightSky();
    }
    if(skyscrapers==1) {
        leftSky();
        resetPosition();
        rightSky();
    }
    if(skyscrapers==2) {
        midSky();
        resetPosition();
        leftSky();
    }
    drive(0, 0);
    msleep(300);
    driveToLine(-250, -30);
    drive(250, 250);
    //msleep(200);
    motor(PULLEY_PORT, -100);
    msleep(400);
    drive(0,0);
    drive(250, -250);
    interpolateServoByTime(CLAW_PORT, CLAW_OPEN, CLAW_CITIZENS, 300);
    interpolateServoByTime(WRIST_PORT, get_servo_position(WRIST_PORT), WRIST_CITIZENS, 300);
    msleep(50);
    motor(PULLEY_PORT, 0);
    drive(0, 0);
    msleep(500);
    interpolatePulley(PULLEY_UP);
	lineFollow180(150, 150, 2700);
    driveTillET180(150, 150, ONOFF); //building right
    //drive(100, 100);
    //msleep(100);
    drive(0, 0); // wait for other bot
    msleep(4000);
    drive(300, -50);
    msleep(950);
    drive(0, 0);
    msleep(300);
    driveToLine(200, 10);
    drive(300, 300);
    msleep(400);
    interpolateServoByTime(ARM_PORT, ARM_UP, ARM_DOWN, 400);
    drive(0, 0);
    interpolatePulley(PULLEY_BEFORE_CITIZENS);
    grabCitizens();
    drive(0, 0);
    drive(-300, 50);
    msleep(950);
    drive(0, 0);
    lineFollow(200, 200, 1100); // grab right-center
    driveTillET(150, 150, -1);
    lineFollow(200, 200, 840);
    drive(350, 0);
    msleep(1150);
    drive(0, 0);
    motor(PULLEY_PORT, 40);
    msleep(200);
    driveToLine(300, 50);
    drive(300, 300);
    msleep(600);
    motor(PULLEY_PORT, 0);
    drive(0, 0);
    interpolatePulley(PULLEY_BEFORE_CITIZENS);
    grabCitizens();
    drive(200, 200);
    msleep(100);
    drive(-300, 50);
    msleep(950);
    lineFollow(150, 150, 500);
    //lineFollow(300, 300, 2500);
    fastLineFollow(250, 250, 1800);
    lineFollow(150, 150, 500);
    
    driveTillET(150, 150, -1);
    lineFollow(200, 200, 70);

    medical = 0; // Temporary Force
    dropCitizens(medical);
    
    //lineFollow180(150, 150, 200);
    
    drive(0, 0);
    msleep(500);
    driveTillET180Short(150, 150, OFFON);
    drive(0, 0);
    // Get Blue cube
    drive(-200, 200);
    interpolateServoByTime(NET_PORT, NET_CLOSE, NET_OPEN, 275);
    interpolateServoByTime(WRIST_PORT, WRIST_CITIZENS, WRIST_CUBE, 275);
    drive(-200, -200);
    interpolateServoByTime(CLAW_PORT, CLAW_CITIZENS, CLAW_OPEN, 200);
    motor(PULLEY_PORT, 30); // motor down
    msleep(1300);
    motor(PULLEY_PORT, 0);
    drive(-200, -200);
    msleep(500);
    drive(0, 0);
    interpolatePulleyWithTolerance(40, PULLEY_CUBE, 0);
    interpolateServoByTime(CLAW_PORT, CLAW_OPEN, CLAW_CUBE, 200);
    msleep(200);
    interpolatePulley(3100);
    drive(200, 200);
    motor(PULLEY_PORT, -100);
    interpolateServoByTime(WRIST_PORT, WRIST_CUBE, WRIST_SKY_MID_CUBE, 300);
    msleep(400);
    msleep(300);
    motor(PULLEY_PORT, 0);
    drive(0,0);
    interpolatePulley(PULLEY_UP);
    drive(-200, 200);
    msleep(500);
    driveToLine(-200, -50);
    drive(300, 300);
    msleep(400);
    drive(200, -200);
    msleep(900);
    lineFollow180(150, 150, 400);
    driveTillET180(150, 150, 1);
    //skyscrapers = 0;
    if (skyscrapers == 0){
        lineFollow180(150, 150, 400);
        driveTillET180(150, 150, -1);
    	drive(-200,-200);
    	msleep(400);
        drive(200, -200);
        msleep(900);
    	drive(200,200);
    	msleep(100);
        drive(0, 0);
        driveToLine(300, 20);
        drive(150, 150);
        msleep(200);
        drive(0, 0);
        motor(PULLEY_PORT, 100); //lower the pulley a little bit so that it can align
        msleep(200);
        motor(PULLEY_PORT, 50);
        msleep(300);
    	motor(PULLEY_PORT, 0);
     	msleep (500);
        
        interpolateServoByTime(CLAW_PORT, CLAW_CUBE, CLAW_OPEN, 500);
    	interpolatePulley(PULLEY_UP);
    	drive(-200,-200);
    	msleep(250);
   		drive(-200, 200);
        msleep(1050);
        //lineFollow180(150, 150, 1400);
        driveTillET180(100, 100, ONOFF);
        drive(0, 0);
    }
    if(skyscrapers == 1){
        //lineFollow180(150, 150, 300);
        //driveTillET180(150, 150, -1);
        lineFollow180(150, 150, 3300);
        driveTillET180(150, 150, -1);
        drive(0, 0);
        lineFollow180(200,200,200);
        drive(200, -200);
        msleep(900);
        drive(0, 0);
        driveToLine(200, 20);
        drive(-200, -200);
        msleep(500);
        drive(0, 0);
        msleep(500);
        motor(PULLEY_PORT, 50);
        msleep(300);
    	motor(PULLEY_PORT,0);
     	msleep (500);
        interpolateServoByTime(CLAW_PORT, CLAW_CUBE, CLAW_OPEN, 500);
      	//interpolatePulley(PULLEY_UP);
    	driveToLine(-150, -20);
        drive(200, 200);
        msleep(250);
   		drive(-200, 200);
        msleep(1050);
        drive(0, 0);
        lineFollow(200, 200, 1300);
        driveTillET(100, 100, OFFON);
        drive(0, 0);
    }
    if (skyscrapers == 2){
        lineFollow180(300, 300, 3900);
        driveTillET180(150, 150, ONOFF);
    	drive(-200, -200);
    	msleep(200);
        drive(300, -300);
        msleep(600);
    	drive(300, 300);
    	motor(PULLEY_PORT, 100); //lower the pulley a little bit so that it can align
        msleep(200);
        motor(PULLEY_PORT, 0);
        driveToLine(200, 20);
        drive(0, 0);
        motor(PULLEY_PORT, 50);
        msleep(300);
    	motor(PULLEY_PORT, 0);
     	msleep(500);
        interpolateServoByTime(CLAW_PORT, CLAW_CUBE, CLAW_OPEN, 400);
    	interpolatePulley(PULLEY_UP);
        drive(200, 200);
        msleep(130);
   		drive(-200, 200);
        msleep(1050);
        lineFollow(150, 150, 200);
        lineFollow(300, 300, 2600);
        lineFollow(150, 150, 500);
        drive(0, 0);
        driveTillET(150, 150, OFFON);
    }
    drive(200, 200);
    msleep(70);
    drive(350, 0);
    msleep(950);
    driveToLine(200, 20);
    drive(200, 200);
    interpolateServoByTime(CLAW_PORT, CLAW_OPEN, CLAW_CITIZENS, 300);
    interpolateServoByTime(NET_PORT, NET_OPEN, NET_CLOSE, 400);
    interpolateServoByTime(WRIST_PORT, WRIST_SKY_MID_CUBE, WRIST_CITIZENS, 300);
    drive(0, 0);
    motor(PULLEY_PORT, 50);
    interpolateServoByTime(ARM_PORT, ARM_UP, ARM_DOWN, 500);
    motor(PULLEY_PORT, 0);
    interpolatePulley(PULLEY_BEFORE_CITIZENS);
    grabCitizens(); // grab left-center
    drive(200, 200);
    msleep(100);
    drive(-300, 50);
    msleep(950);
    drive(0, 0);
    
    lineFollow(150, 150, 300);
    lineFollow(300, 300, 450);
    lineFollow(150, 150, 300);
    driveTillET(150, 150, ONOFF);
    lineFollow(200, 200, 670);
    drive(350, 0);
    msleep(1150);
    driveToLine(200, 20);
    drive(200, 200);
    motor(PULLEY_PORT, 40);
    msleep(1000);
    motor(PULLEY_PORT, 0);
    
    drive(0, 0);
    interpolatePulley(PULLEY_BEFORE_CITIZENS);
    grabCitizens(); // left
    dropCitizens2(medical);
    
    printf("End Time: %lu\n", systime() - startTime);
    
    drive(0, 0);
    msleep(999999);
    
    return 0;
}
