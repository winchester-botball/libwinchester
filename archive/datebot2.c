#include <kipr/botball.h>

/*
*Wiring Order:
*
*/


//TODO: tree_grab_bin doesn't grab bin

//---------------booleans---------------
#define TRUE 1
#define FALSE 0

//----------------motors----------------
#define LEFT_WHEEL					1
#define RIGHT_WHEEL					0

#define TANKMO_MOTOR				2

#define FRISBEE_WHEEL				3


//----------------servos----------------
#define CLAPPERS_PORT				0
#define CLAPPERS_OPEN				1280 //was 1225 before to ensure date bin can pass through
#define CLAPPERS_DEATH				650
#define CLAPPERS_OPEN_FULL			1020
#define CLAPPERS_TANKMO				1160
#define CLAPPERS_CLOSED				1460 //1350
#define CLAPPERS_VERY_CLOSED		1600

#define CLAW_PORT					1
#define CLAW_UP						0
#define CLAW_FLICK_FRISBEE			550 //600
#define CLAW_DROP					900
#define CLAW_LEVEL					950
#define CLAW_DATES					1200
#define CLAW_START					1680
#define CLAW_DOWN					2040

#define BIN_CLAW_PORT				2
#define BIN_CLAW_CLOSED				1350
#define BIN_CLAW_PIPE				1080
#define BIN_CLAW_MID				700
#define BIN_CLAW_OPEN				0

/*#define TANKMO_PORT				3
#define TANKMO_UP					0
#define TANKMO_DOWN					2040
#define TANKMO_BIN					600*/

//-----------------------sensors---------------------------
#define LEFT_TOPHAT					1
#define RIGHT_TOPHAT				0
#define ET_SENSOR					4
#define LIGHT_SENSOR				5


//#define BUTTON_SENSOR				0

//#define TOPHAT_THRESHOLD 2500
#define TOPHAT_THRESHOLD_1 2600
#define TOPHAT_THRESHOLD_0 2000

double gyroBias = 0.0;

void setup(){
    printf("DATEBOT V1\n");
    printf("WALLABY BATTERY CHARGE: %f%%\n", 100*(double)power_level());
    set_servo_position(CLAPPERS_PORT, CLAPPERS_TANKMO);
    set_servo_position(CLAW_PORT, CLAW_START);
    set_servo_position(BIN_CLAW_PORT, BIN_CLAW_MID);
    //not a servo anymore//set_servo_position(TANKMO_PORT, TANKMO_UP);
    //enable_servo(CLAW_PORT);
    //enable_servo(CLAPPERS_PORT);
    enable_servos();
}

void interpolateServoByTime(int port, int start, int end, int time){
    unsigned long endTime = systime()+time;
    while(systime()<endTime){
        set_servo_position(port, (end-start)*(1.0-(((double)(endTime-systime()))/((double)time)))+start);
        msleep(1);
    }
    set_servo_position(port, end);
}

void dualInterpolate(int portA, int portB, int startA, int startB, int endA, int endB, int time){
    unsigned long endTime = systime()+time;
    while(systime()<endTime){
        set_servo_position(portA, (endA-startA)*(1.0-(((double)(endTime-systime()))/((double)time)))+startA);
        set_servo_position(portB, (endB-startB)*(1.0-(((double)(endTime-systime()))/((double)time)))+startB);
        msleep(1);
    }
    set_servo_position(portA, endA);
    set_servo_position(portB, endB);
}

void driving(int leftspd, int rightspd){
    float fudge = (leftspd < 0 ? 0.98 : 1);
    mav(LEFT_WHEEL, leftspd*fudge);
    mav(RIGHT_WHEEL, -rightspd);
}

//WIP
//Fudge factors not updated

/*void drive(int leftspd, int rightspd, int time){
    float rfudge = (rightspd < 0 ? 0.935 : 0.955);
    int dx = 300;	//Speed gain per increment	
    int dt = 25;	//Time per increment
    
    int accelSpeed = 0;
    int leftExtraTime = 0;
    int rightExtraTime = 0;
    
    while (time > dt) {  
        accelSpeed += dx;
        int left = fmin(abs(leftspd), accelSpeed);
        int right = fmin(abs(rightspd), accelSpeed);
        if (left != abs(leftspd)) 	leftExtraTime 	+= 	dt * (abs(leftspd) - left) 	 / abs(leftspd);
        if (right != abs(rightspd)) rightExtraTime 	+= 	dt * (abs(rightspd) - right) / abs(rightspd);
        left = (leftspd < 0 ? -left : left);
        right  = (rightspd < 0 ? -right : right);
   		mav(LEFT_WHEEL, left*fudge);
    	mav(RIGHT_WHEEL, -right);
    	msleep(dt - 2);		//Calculations take ~2msec
        time -= dt;
    }
    
    mav(LEFT_WHEEL, leftspd*fudge);
    mav(RIGHT_WHEEL, -rightspd);
    msleep(fmin(leftExtraTime, rightExtraTime) + time);
    
    if (leftExtraTime > rightExtraTime) {
        mav(LEFT_WHEEL, leftspd*fudge);
    	mav(RIGHT_WHEEL, 0);
        msleep(leftExtraTime - rightExtraTime);
    }
    else {
        mav(LEFT_WHEEL, 0);
    	mav(RIGHT_WHEEL, -rightspd);
        msleep(rightExtraTime - leftExtraTime);
    }

    freeze(LEFT_WHEEL);
    freeze(RIGHT_WHEEL);
    msleep(10);
}*/

void drive(int leftspd, int rightspd, int time){
    float fudge = (leftspd < 0 ? 0.98 : 1);
    mav(LEFT_WHEEL, leftspd*fudge);
    mav(RIGHT_WHEEL, -rightspd);// *0.9
    msleep(time*1.057);
    mav(LEFT_WHEEL, 0);
    mav(RIGHT_WHEEL, 0);
    msleep(10);
}

void drive_direct(int leftspd, int rightspd, int time){
    float fudge = (leftspd < 0 ? 0.98 : 1);
    mav(LEFT_WHEEL, leftspd*fudge);
    mav(RIGHT_WHEEL, -rightspd);
    msleep(time);
    mav(LEFT_WHEEL, 0);
    mav(RIGHT_WHEEL, 0);
    msleep(10);
}

void turn_left(int speed, int time){
    mav(LEFT_WHEEL, -speed*0.98);
    mav(RIGHT_WHEEL, -speed);
    msleep(time*1.057);
    mav(LEFT_WHEEL, 0);
    mav(RIGHT_WHEEL, 0);
    msleep(10);
}
    
void turn_right(int speed, int time){
    mav(LEFT_WHEEL, speed);
    mav(RIGHT_WHEEL, speed);
    msleep(time*1.057);
    mav(LEFT_WHEEL, 0);
    mav(RIGHT_WHEEL, 0);
    msleep(10);
}

void left_90() {
    turn_left(1500, 795);
    //turn_left(1500, 910);
}

void right_90() {
    turn_right(1500, 825);
    //turn_right(1500, 920);
}

void frisbee_spin(int time){
    mav(FRISBEE_WHEEL, -1500);
    msleep(time);
    mav(FRISBEE_WHEEL, 0);
    //msleep(200);
}


void claw_grab(){
    interpolateServoByTime(BIN_CLAW_PORT, BIN_CLAW_OPEN, BIN_CLAW_CLOSED, 1000);
}

void alignToLine(int speed){
    int tophatLeft = 1;
    int tophatRight = 1;
    int time = systime();
    
    while (tophatLeft != 0 || tophatRight != 0) {
        int tophatValueLeft = analog(LEFT_TOPHAT);
        int tophatValueRight = analog(RIGHT_TOPHAT);

        tophatLeft = (tophatValueLeft < TOPHAT_THRESHOLD_0 ? 1 : 0);
        tophatRight = (tophatValueRight < TOPHAT_THRESHOLD_1 ? 1 : 0);

        driving(speed * tophatLeft, speed * tophatRight);
        msleep(1);
        
        if (systime() - time > 5000) break;
    }
    
    drive_direct(-speed, -speed, 250);
    speed /= 5;
    
    tophatLeft = 1;
    tophatRight = 1;
    
    int leftTime = 0;
    int rightTime = 0;
    
    time = systime();
    
    while (tophatLeft != 0 && tophatRight != 0) {
        int tophatValueLeft = analog(LEFT_TOPHAT);
        int tophatValueRight = analog(RIGHT_TOPHAT);

        tophatLeft = (tophatValueLeft < TOPHAT_THRESHOLD_0 ? 1 : 0);
        tophatRight = (tophatValueRight < TOPHAT_THRESHOLD_1 ? 1 : 0);

        driving(speed * tophatLeft, speed * tophatRight);
        msleep(1);
        if (tophatLeft == 1) leftTime++;
        if (tophatRight == 1) rightTime++;
        if (systime() - time > 5000) break;
    }
    
    while (tophatLeft != tophatRight) {
        int tophatValueLeft = analog(LEFT_TOPHAT);
        int tophatValueRight = analog(RIGHT_TOPHAT);

        tophatLeft = (tophatValueLeft < TOPHAT_THRESHOLD_0 ? 1 : -1);
        tophatRight = (tophatValueRight < TOPHAT_THRESHOLD_1 ? 1 : -1);

        driving(speed * tophatLeft, speed * tophatRight);
        msleep(1);
        if (tophatLeft == 1) leftTime++;
        if (tophatRight == 1) rightTime++;
        if (systime() - time > 5000) break;
    }
    
    driving(0, 0);
    
    int turnTime = leftTime - rightTime;
    printf("\t%i %s", turnTime, "msecs left\n");
    
    /*if (turnTime < 0) {
		drive_direct(0, speed, -turnTime);
    }
    else {
   		drive_direct(speed, 0, turnTime);
    }*/
    
    driving(0, 0);
    return;
}

void lineFollow(int speed, int timeout, int delay){
    while (timeout > 0){
        int tophatValueLeft = analog(LEFT_TOPHAT);
        int tophatValueRight = analog(RIGHT_TOPHAT);
        printf("%d %d -> ", tophatValueLeft, tophatValueRight);
        int tophatLeft = (tophatValueLeft < TOPHAT_THRESHOLD_0 ? -1 : 1);
        int tophatRight = (tophatValueRight < TOPHAT_THRESHOLD_1 ? -1 : 1);
        driving(speed * tophatRight, speed * tophatLeft);					//jason's drives backwards
        msleep(delay);
        timeout -= delay;
        if(tophatLeft == tophatRight){timeout -= delay;}
    }
    drive_direct(0, 0, 10);
}

void tankmo_claw_move(int spd, int time){
    mav(TANKMO_MOTOR, spd);
    msleep(time);
    mav(TANKMO_MOTOR, 0);
    msleep(200);
}

void tankmo_claw_moving(int spd){
    mav(TANKMO_MOTOR, spd);
}
    
void tankmo_claw_stop(){
    mav(TANKMO_MOTOR, 0);
    msleep(200);
}

//the code common to every tree which rakes the poms, spins the frisbee, etc
void tree_grab_base(){
    drive(0, 1000, 100);
    interpolateServoByTime(CLAW_PORT, CLAW_LEVEL, CLAW_DATES, 200);
    
    drive(-1150, -1150, 1100);				//~750 backwards at 1500 speed
    set_servo_position(CLAW_PORT, CLAW_DROP);
    msleep(250);
    
    driving(1500, 1500);
    interpolateServoByTime(CLAW_PORT, CLAW_DROP, CLAW_LEVEL, 275);
    msleep(450);
    driving(800, 800);
    msleep(100);
    driving(0, 0);
    //driving(-100, 100);
    frisbee_spin(1300);
    
    driving(0, 0);
    interpolateServoByTime(CLAW_PORT, CLAW_LEVEL, CLAW_DATES, 200);
    
    drive(-1150, -1150, 1100);			//~750 backwards at 1500 speed
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
}

//do the tree, leave the bin
void tree_grab(){
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_OPEN_FULL);
    
    driving(1500, 1500);
    msleep(1000);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_OPEN, 250);
    driving(0, 0);
    
    tree_grab_base();
    drive(-1500, -1500, 250);
}

//do the tree, grab the bin
void tree_grab_bin() {
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_CLOSED);
    
    driving(1500, 1500);
    msleep(950);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_CLOSED, CLAPPERS_OPEN, 300);
    driving(0, 0);

    tree_grab_base();
    
    driving(1500, 1500);
    msleep(600);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN, CLAPPERS_CLOSED, 200);
    driving(0, 0);
    msleep(1050);
    drive(-1500, -1500, 1250);
}   

//similar to tree_grab() with wide open claw to catch bin
void tree_grab_first_tree(){
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_OPEN_FULL);
    msleep(200); //300
  
    driving(1500, 1500);
    msleep(400);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_CLOSED, 300);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_CLOSED, CLAPPERS_OPEN, 250);
    msleep(250);
    
    driving(0, 0);
    tree_grab_base();
}

//similar to tree_grab_bin() with wide open claw to grab bin
void tree_grab_second_tree(){
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_OPEN_FULL);
    
    driving(1500, 1500);
    msleep(700);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_CLOSED, 300);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_CLOSED, CLAPPERS_OPEN, 200);
    tree_grab_base();
    
    driving(1500, 1500);
    //interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN, CLAPPERS_CLOSED, 200);
    msleep(900);
    driving(0, 0);
    msleep(125); //200
    drive(-1500, -1500, 1250);
    
    driving(1500, 1500);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_CLOSED, CLAPPERS_OPEN, 250);
    msleep(250);
    driving(0, 0);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN, CLAPPERS_CLOSED, 250);
    drive(-1500, -1500, 400);
}

void tree_grab_fifth_tree() {
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_OPEN_FULL);
    
    driving(1500, 1500);
    msleep(1200);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_OPEN, 250);
    driving(0, 0);
    tree_grab_base();
    
    driving(1500, 1500);
    msleep(400);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN, CLAPPERS_CLOSED, 400);
    driving(0, 0);
    msleep(200);
    drive(-1500, -1500, 1000);
}

void tree_grab_last_tree(){
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_OPEN);
    
    drive(1500, 1500, 1400);
    
    interpolateServoByTime(CLAW_PORT, CLAW_LEVEL, CLAW_DATES, 200);
    
    drive(-1100, -1100, 1100);
    set_servo_position(CLAW_PORT, CLAW_DROP);
    msleep(300);
    
    driving(1500, 1500);
    interpolateServoByTime(CLAW_PORT, CLAW_DROP, CLAW_LEVEL, 300);
    msleep(600);
    driving(0, 0);
    frisbee_spin(800);
    
    interpolateServoByTime(CLAW_PORT, CLAW_LEVEL, CLAW_DATES, 200);
    
    drive(-1100, -1100, 1100);
    set_servo_position(CLAW_PORT, CLAW_LEVEL);
}

void gyroCal() {
    int i = 0, sum = 0;
    while (i < 50) {
        sum += gyro_z();
        msleep(1);
        ++i;
    }
    
    gyroBias = sum/50.0;
}

void victory_dance() {
    mav(LEFT_WHEEL, 1500);
    mav(RIGHT_WHEEL, 1500);
    set_servo_position(CLAPPERS_PORT, CLAPPERS_OPEN_FULL);
    int time = systime();
    
    tankmo_claw_move(900, 300);
    
    while (systime() - time < 20000) {
        tankmo_claw_moving(850);
        interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_VERY_CLOSED, 300);
        tankmo_claw_moving(-970);
        interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_VERY_CLOSED, CLAPPERS_OPEN_FULL, 300);
    }
    
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_DEATH, 2000);
    
    while (systime() - time < 30000) {
        tankmo_claw_moving(850);
        msleep(300);
        tankmo_claw_moving(-970);
        msleep(300);
    }
    
    mav(LEFT_WHEEL, 0);
    mav(RIGHT_WHEEL, 0);
}
        

void gyroTurn(int left, int right, int degrees) {
	int theta = 0;
    int kiprDeg = 58000*degrees/90;
    int time = 0;
    mav(LEFT_WHEEL, left);
    mav(RIGHT_WHEEL, -right);
    
    while (theta < kiprDeg) {
        msleep(10);
        theta += abs(gyro_z() - gyroBias)*10;
    }
    
    mav(LEFT_WHEEL, 0);
    mav(RIGHT_WHEEL, 0);
}

void gyroDrive(int speed, int time) {
    int startTime = seconds();
    double theta = 0;
    
    while (seconds() - startTime < (time / 1000.0)) {
        double correction = pow(theta*90/580000, 3);
        if (speed > 0) {
            mav(LEFT_WHEEL, speed*(1.0000+correction));
    		mav(RIGHT_WHEEL, -speed*(1.0000-correction));
        }
        else {
            mav(LEFT_WHEEL, speed*(1.0000-correction));
    		mav(RIGHT_WHEEL, -speed*(1.0000+correction));
        }
        msleep(10);
        theta += (gyro_z() - gyroBias) * 10;
    }
    
    
}

int main(){
    wait_for_light(LIGHT_SENSOR);
    shut_down_in(120.5);
    setup();
    gyroCal();

    //test program
    
    /*drive(1500, 1500, 4000);
    msleep(2000);
    turn_left(1500, 800);
    msleep(1000);
    turn_right(1500, 800);
    msleep(1000);
    drive(-1500, -1500, 4000);
    msleep(1000);
    return 0;*/
    
    int time = systime();
    
    //start of first half
    drive(-1500, -1500, 550);
    left_90();
    drive(1500, 1500, 300); //450
    alignToLine(1000);
    drive(1500, 1500, 420);
    right_90();
    driving(-1500, -1500);
    msleep(700);
    interpolateServoByTime(CLAW_PORT, CLAW_START, CLAW_LEVEL, 500);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_OPEN, 250);
    msleep(350); //550
    driving(0, 0);
    alignToLine(-1000);
    tankmo_claw_move(900, 555);
    drive(-1500, -1500, 3000);
    //drive(1500, 1500, 700);
    //turn_right(1500, 400);
    //turn_left(1500, 400);
    //drive(-1500, -1500, 1000);
    
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s Out of Starting Box\n");
    
    tankmo_claw_move(900, 700);
    msleep(500);
    
    tankmo_claw_move(-1200, 875);
    msleep(250);
    
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN, CLAPPERS_TANKMO, 200); //250
    
    driving(-1500, -1500);
    interpolateServoByTime(CLAW_PORT, CLAW_LEVEL, CLAW_DOWN, 375); //600
    msleep(3125);
    left_90();
    left_90();
    driving(-1500, -1500);
    msleep(2900);
    interpolateServoByTime(CLAW_PORT, CLAW_DOWN, CLAW_UP, 600);
    
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s Tankmo\n");
    
    drive(-1500, -1500, 250);
	drive(1500, 1500, 1100);
    tankmo_claw_move(900, 555);
    
    turn_left(1250, 1100);
    
    turn_right(1250, 1100 + 600);

    turn_left(1250, 650);
    tankmo_claw_move(-900, 575);
    
    drive(-1500, -1500, 2000);
    drive(1500, 1500, 1100);
    
    turn_right(1250, 500);
    drive(-1500, -1500, 800);
    turn_right(1250, 395);
    driving(1500, 1500);
    msleep(3000);
    driving(-1500, -1500);
    msleep(1000);
    
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s Rip Date Bins\n");
    
    //cubes or tram
    turn_left(1500, 750);
    alignToLine(1500);
    drive(1500, 1500, 1000);
    turn_right(1500, 850);
    
    lineFollow(1500, 3500, 20);
    turn_left(1500, 720);
    drive(1500, 1500, 250);
    drive(-1500, -1500, 150);
    turn_right(1500, 800);
    
    lineFollow(1500, 2500, 20);
    turn_left(1500, 1000);
    drive(1000, 1000, 250);
    interpolateServoByTime(CLAPPERS_PORT, CLAPPERS_OPEN_FULL, CLAPPERS_OPEN, 750);
    alignToLine(-750);
    drive(-1000, -1000, 2500);
    
    drive(-1000, -1000, 350);
    tankmo_claw_move(900, 300);
    tankmo_claw_moving(40);
    drive(1500, 1500, 2000);
    tankmo_claw_move(-900, 375);
    
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s Rip Cubes, ready to blow\n");
    
    alignToLine(1500);
    drive(1500, 1500, 2000);
    victory_dance();
    
    printf("%.2f", (systime() - time)/1000.0);
    printf("%s", "s Death\n");

    return 0;
}