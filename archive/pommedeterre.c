#include <kipr/botball.h>

#define CLAW_SERVO 0
#define CLAW_SQUEEZE 25
#define CLAW_CLOSEFIRM 40
#define CLAW_CLOSEFOAM 75
#define CLAW_CLOSESUPPLIES 125
#define CLAW_PARALLEL 160
#define CLAW_SLIP 250
#define CLAW_SUPPLIES 240
#define CLAW_OPENCUBE 400  
#define CLAW_OPENWIDE 555
#define CLAW_45 700
#define CLAW_90 1300

#define LEFT_MOTOR 1
#define RIGHT_MOTOR 0

#define TOPHAT_LEFT 1
#define TOPHAT_RIGHT 0
#define ET_SENSOR 5

#define POINT_TURN_MSLEEP 1525
#define ARC_TURN_MSLEEP 2400

#define ET_BUILDING_THRESHOLD 600

//getting ambulance (clear out the supplies while moving) --> firefighters --> firetruck 

int TOPHAT_GREY_LEFT, TOPHAT_WHITE_LEFT, TOPHAT_GREY_RIGHT, TOPHAT_WHITE_RIGHT;
int TOPHAT_BLACK_LEFT = 2500, TOPHAT_BLACK_RIGHT = 3500;
int TOPHAT_BLACK_WHITE_THRESHOLD_LEFT, TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT;
int TOPHAT_BLACK_GREY_THRESHOLD_LEFT, TOPHAT_BLACK_GREY_THRESHOLD_RIGHT;

//double normalize(double minA, double maxA, double minB, double maxB, double input);
double normalize(double minA, double maxA, double minB, double maxB, double input) {
    if (input > maxA) input = maxA;
    if (input < minA) input = minA;
    return (double) ((minB)) + (double) ((maxB-minB) * (input-minA) / (maxA-minA));
}

int pollSensor(int iterations, int sensorPort) {
    int sum = 0;
    int n = iterations;
    
    while (n > 0) {
        sum += analog(sensorPort);
        --n;
    }
    
    return sum / iterations;
}       

void drive(int left, int right) {
    mav(LEFT_MOTOR, -left*15*0.80);
    mav(RIGHT_MOTOR, right*15);
}

void gyroTurn(int left, int right, int deg) {
    float target = deg * (3.1416f / 180.0f) + get_compass_angle();
    printf("Target %.2f\n", target);
    
    int speedLeft = 0, speedRight = 0;
    
    if (deg > 0) {        
        if (target > 3.14f) {
            while (get_compass_angle() > 0) {
            	drive(speedLeft, speedRight);
           		msleep(1);
                if (abs(speedLeft) < abs(left)) speedLeft += 1 * abs(left)/left;
                if (abs(speedRight) < abs(right)) speedRight += 1 * abs(right)/right;
        	}
            
            drive(0, 0);
            target *= (180.0f / 3.1416f);
            target = -180 + (((int)target) % 180);
            target *= (3.1416f / 180.0f);
            
            printf("New Target %.2f\n", target);
            
            while (get_compass_angle() > 0) {
            	drive(speedLeft, speedRight);
            	msleep(1);
                if (abs(speedLeft) < abs(left)) speedLeft += 1 * abs(left)/left;
                if (abs(speedRight) < abs(right)) speedRight += 1 * abs(right)/right;
        	}
            
            while (get_compass_angle() < target) {
            	drive(speedLeft, speedRight);
            	msleep(1);
                if (abs(speedLeft) < abs(left)) speedLeft += 1 * abs(left)/left;
                if (abs(speedRight) < abs(right)) speedRight += 1 * abs(right)/right;
        	}
        }
        
        else {
        	while (get_compass_angle() < target) {
            	drive(speedLeft, speedRight);
           		msleep(1);
                if (abs(speedLeft) < abs(left)) speedLeft += 1 * abs(left)/left;
                if (abs(speedRight) < abs(right)) speedRight += 1 * abs(right)/right;
        	}
        }
        
        drive(0, 0);
        msleep(100);

        speedLeft /= 3;
        speedRight /= 3;
        while (get_compass_angle() > target) {
            drive(speedLeft/3, speedRight/3);
            msleep(1);
            if (abs(speedLeft) < abs(left)) speedLeft += -1 * abs(left)/left;
            if (abs(speedRight) < abs(right)) speedRight += -1 * abs(right)/right;
        }
    }
    
    else {      
        if (target < -3.14f) {
            while (get_compass_angle() < 0) {
            	drive(speedLeft, speedRight);
           		msleep(1);
                if (abs(speedLeft) < abs(left)) speedLeft += 1 * abs(left)/left;
                if (abs(speedRight) < abs(right)) speedRight += 1 * abs(right)/right;
        	}
            
            drive(0, 0);
            target *= (180.0f / 3.1416f);
            target = 180 - (-((int)target) % 180);
            target *= (3.1416f / 180.0f);
            
            printf("New Target %.2f\n", target);
            
            while (get_compass_angle() < 0) {
            	drive(speedLeft, speedRight);
           		msleep(1);
                if (abs(speedLeft) < abs(left)) speedLeft += 1 * abs(left)/left;
                if (abs(speedRight) < abs(right)) speedRight += 1 * abs(right)/right;
        	}

            while (get_compass_angle() > target) {
            	drive(speedLeft, speedRight);
           		msleep(1);
                if (abs(speedLeft) < abs(left)) speedLeft += 1 * abs(left)/left;
                if (abs(speedRight) < abs(right)) speedRight += 1 * abs(right)/right;
        	}
        }
        
        else {
            while (get_compass_angle() > target) {
            	drive(speedLeft, speedRight);
           		msleep(1);
                if (abs(speedLeft) < abs(left)) speedLeft += 1 * abs(left)/left;
                if (abs(speedRight) < abs(right)) speedRight += 1 * abs(right)/right;
        	}
        }
        
        drive(0, 0);
        msleep(100);

        speedLeft /= 3;
        speedRight /= 3;
        while (get_compass_angle() < target) {
            drive(speedLeft/3, speedRight/3);
            msleep(1);
            if (abs(speedLeft) < abs(left)) speedLeft += -1 * abs(left)/left;
            if (abs(speedRight) < abs(right)) speedRight += -1 * abs(right)/right;
        }
    }
    
    drive(0, 0);
}

int interpolateServoPosition(int start, int end, double percentage) {
    return start + (end - start) * percentage;
}

void interpolateServo(int port, int start, int end, int time) {
	unsigned long endTime = systime() + time;
	while (systime() < endTime) {
		set_servo_position(port, interpolateServoPosition(start, end,
			1.0f - (((double)(endTime - systime())) / ((double)time))));
		msleep(1);
	}
    set_servo_position(port, end);
}

void lineFollowRightSensor(int timeout, int speed){
    unsigned long endTime = systime() + timeout;
    unsigned long time;
    
	while ((time = systime()) < endTime) {
        double tophat = normalize(TOPHAT_WHITE_RIGHT, TOPHAT_BLACK_RIGHT, 0, 1, analog(TOPHAT_RIGHT));

        double right = (1.0f-tophat) > 0.5f ? (1.0f-tophat) : 0.5f;
        double left = tophat > 0.5f ? tophat : 0.5f;
        drive(left*speed*2, right*speed*2);
        msleep(1);
    }
    
    drive(0, 0);
}


void lineFollowLeftSensor(int timeout, int speed){
    unsigned long endTime = systime() + timeout;
    unsigned long time;
    
	while ((time = systime()) < endTime) {
        double tophat = normalize(TOPHAT_WHITE_LEFT, TOPHAT_BLACK_LEFT, 0, 1, analog(TOPHAT_LEFT));

        double left = (1.0f-tophat) > 0.5f ? (1.0f-tophat) : 0.5f;
        double right = tophat > 0.5f ? tophat : 0.5f;
        drive(left*speed*2, right*speed*2);
        msleep(1);
    }
    
    drive(0, 0);
}

void lineFollowLeftSensorInsideEdge(int timeout, int speed) {
    unsigned long endTime = systime() + timeout;
    unsigned long time;
    
	while ((time = systime()) < endTime) {
        double tophat = normalize(TOPHAT_WHITE_LEFT, TOPHAT_BLACK_LEFT, 0, 1, analog(TOPHAT_LEFT));
        double right = (1.0f-tophat) > 0.5f ? (1.0f-tophat) : 0.5f;
        double left = tophat > 0.5f ? tophat : 0.5f;
        drive(left*speed*2, right*speed*2);
        msleep(1);
    }
    
    drive(0, 0);
}

void lineFollowLeftSensorET(int timeout, int speed){
	int readings[5];
    int counter = 0;
    int sum = 0;

    unsigned long startTime = systime();
    
    while (counter < 5) {
        readings[counter] = analog(ET_SENSOR);
        sum += readings[counter];
        ++counter;
        msleep(10);
    }
    
	while (systime() - startTime < timeout) {
        sum -= readings[counter % 5];
        readings[counter % 5] = analog(ET_SENSOR);
        sum += readings[counter % 5];
        
        if (sum/5 < ET_BUILDING_THRESHOLD) {
        	break;
        }
            
        ++counter;
        
        double tophat = normalize(TOPHAT_WHITE_LEFT, TOPHAT_BLACK_LEFT, 0, 1, analog(TOPHAT_LEFT));
        double left = (1.0f-tophat) > 0.5f ? (1.0f-tophat) : 0.5f;
        double right = tophat > 0.5f ? tophat : 0.5f;
        drive(left*speed*2, right*speed*2);        
        msleep(10);
    }
    
    drive(0, 0);
}

void lineFollowRightSensorET(int timeout, int speed){
	int readings[5];
    int counter = 0;
    int sum = 0;

    unsigned long startTime = systime();
    
    while (counter < 5) {
        readings[counter] = analog(ET_SENSOR);
        sum += readings[counter];
        ++counter;
        msleep(10);
    }
    
	while (systime() - startTime < timeout) {
        sum -= readings[counter % 5];
        readings[counter % 5] = analog(ET_SENSOR);
        sum += readings[counter % 5];
        
        if (sum/5 < ET_BUILDING_THRESHOLD) {
        	break;
        }
            
        ++counter;
        
        double tophat = normalize(TOPHAT_WHITE_LEFT, TOPHAT_BLACK_RIGHT, 0, 1, analog(TOPHAT_RIGHT));
        double right = (1.0f-tophat) > 0.5f ? (1.0f-tophat) : 0.5f;
        double left = tophat > 0.5f ? tophat : 0.5f;
        drive(left*speed*2, right*speed*2);
        msleep(10);
    }
    
    drive(0, 0);
}

void alignLeftSensorToLine(int speed) {
    if (pollSensor(2, TOPHAT_LEFT) < TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        drive(speed, 0);
        while (pollSensor(2, TOPHAT_LEFT) < TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        	msleep(5);
    	}
    }
    else {
        drive(0, speed);
        while (pollSensor(2, TOPHAT_LEFT) > TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        	msleep(5);
    	}  
    }
    
    drive(0, 0);
    msleep(100);
}

void alignRightSensorToLine(int speed) {
    if (pollSensor(2, TOPHAT_RIGHT) < TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT) {
        drive(0, speed);
        while (pollSensor(2, TOPHAT_RIGHT) < TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT) {
        	msleep(5);
    	}
    }
    else {
        drive(speed, 0);
        while (pollSensor(2, TOPHAT_RIGHT) > TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT) {
        	msleep(5);
    	}  
    }
    
    drive(0, 0);
    msleep(100);
}

void driveToLine(int speed, int timeout, int multiplier) { //it is reversed
	while (timeout > 0) {
        double tophatLeft = round(normalize(TOPHAT_WHITE_LEFT, TOPHAT_BLACK_WHITE_THRESHOLD_LEFT, 1, 0, analog(TOPHAT_LEFT)));
        double tophatRight = round(normalize(TOPHAT_WHITE_RIGHT, TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT, 1, 0, analog(TOPHAT_RIGHT)));
        
        if (multiplier == 1) {    
        	if (tophatLeft < 0.1f && tophatRight < 0.1f) break;
            drive(speed * tophatLeft, speed * tophatRight);
        }
        else {
            if (tophatLeft > 0.9f && tophatRight > 0.9f) break;
            drive(speed * (1.0f-tophatLeft), speed * (1.0f-tophatRight));
        }
            
        msleep(1);
        timeout -= 1;
    }
    drive(0, 0);
}

void driveToLineFast(int speed, int timeout, int multiplier) {
    driveToLine(speed, timeout, multiplier);
    
    drive(0, 0);
    msleep(100);
    
    drive(-speed, -speed);
    msleep(100);
    
    driveToLine(speed/5, 300, multiplier);
}

int camera_warmup(int timeout) {
    int startTime = systime(), cameraWorking;
    
    while (systime() - startTime < timeout) {
        cameraWorking = camera_update();
        if (!cameraWorking) return 0;
        msleep(25);
    }
    
    return cameraWorking;
}
    

void setup() {
    //setup:
    enable_servos(); 
    interpolateServo(CLAW_SERVO, get_servo_position(CLAW_SERVO), CLAW_SLIP, 500);			//set initial servo position
	msleep(100);

    TOPHAT_WHITE_LEFT = pollSensor(10, TOPHAT_LEFT) - 100;
    TOPHAT_WHITE_RIGHT = pollSensor(10, TOPHAT_RIGHT) - 100;
    TOPHAT_GREY_LEFT = TOPHAT_WHITE_LEFT * 1.2;
    TOPHAT_GREY_RIGHT = TOPHAT_WHITE_RIGHT * 1.2;			    	//set white sensor value based on current sensor readings
    
    TOPHAT_BLACK_WHITE_THRESHOLD_LEFT = (TOPHAT_BLACK_LEFT + TOPHAT_WHITE_LEFT)*1/2;
    TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT = (TOPHAT_BLACK_RIGHT + TOPHAT_WHITE_RIGHT)*1/2;
    TOPHAT_BLACK_GREY_THRESHOLD_LEFT = (TOPHAT_BLACK_LEFT + TOPHAT_GREY_LEFT) / 2;
    TOPHAT_BLACK_GREY_THRESHOLD_RIGHT = (TOPHAT_BLACK_RIGHT + TOPHAT_GREY_RIGHT) / 2;
    
    set_compass_params(-22.190001, 22.356001, -17.760000, -0.197369, -0.030496, 1.019291, 1.039419);
    
    printf("Setup: Opening Camera: %s\n", (camera_open_black() ? "Success" : "Failure"));
	printf("Setup: Loading Camera Config TestConfig.conf: %s\n", (camera_load_config("TestConfig") ? "Success" : "Failure"));    
    
    printf("Setup: Left Tophat Values are %i / %i / %i, using threshold %i for black/white and %i for black/grey\n", TOPHAT_WHITE_LEFT, TOPHAT_GREY_LEFT, TOPHAT_BLACK_LEFT, TOPHAT_BLACK_WHITE_THRESHOLD_LEFT, TOPHAT_BLACK_GREY_THRESHOLD_LEFT);
    printf("Setup: Right Tophat Values are %i / %i / %i, using threshold %i for black/white and %i for black/grey\n", TOPHAT_WHITE_RIGHT, TOPHAT_GREY_RIGHT, TOPHAT_BLACK_RIGHT, TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT, TOPHAT_BLACK_GREY_THRESHOLD_RIGHT);

    //end setup																			
}

void drive_to_med_center(){ 
    drive(100, 100);
    msleep(1100);
    interpolateServo(CLAW_SERVO, CLAW_45, CLAW_CLOSEFIRM, 250); 						//drive past the starting box line to get ambulance in the claw
    
    drive(0, 0);
    interpolateServo(CLAW_SERVO, CLAW_CLOSEFIRM, CLAW_90, 400);							//extend the claw arm out 90 degrees to use its tophat sensor to see the line	
    
    drive(80, 80);
    while (pollSensor(5, TOPHAT_RIGHT) < TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT) {			//uses grey threshold (higher) because tire shadow makes white values much darker
    	msleep(10);
    }
    msleep(50);
    while (pollSensor(5, TOPHAT_RIGHT) > TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT) {
    	msleep(10);
    }																					//drive up to the right arm hitting the black line, then through the black line
    
    
    drive(0, 0);
    interpolateServo(CLAW_SERVO, CLAW_90, CLAW_CLOSEFIRM, 400);							//close the claw    
    
    drive(0, 20); msleep(20);
    drive(0, 40); msleep(20);
    drive(0, 60); msleep(20);
    drive(0, 90); msleep(ARC_TURN_MSLEEP);												//make a measured 90 degree turn, accelerating the motor to prevent motor damage
	drive(0, 0);  msleep(200);
    
    drive(100, 100);	
    while (pollSensor(2, TOPHAT_RIGHT) < TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT) {
    	msleep(20);
    }																					//drive toward med centers until right sensor sees the black line
    
    alignLeftSensorToLine(50);
    lineFollowLeftSensor(1300, 80);														//line follow the edge of the black line until robot is almost at the med center
   
    drive(75, -75);
    msleep(1200);
    drive(50, -50);
    while (pollSensor(2, TOPHAT_LEFT) < TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
    	msleep(5);
    }																					//turn to the right until the left sensor lines up with the black line																	//
    
    lineFollowLeftSensorET(10000, 50);													//line follow with the black line until the ET sensor detects the building	
    lineFollowLeftSensor(500, 50);														//and then a little bit farther
}

int med_center_is_burning() {
    camera_warmup(1000);
    
    if (get_object_count(0) == 0) {
        printf("\tNo red square\n");
        return 0;
    }
    
    int nObjects = get_object_count(0);
    int obj = 0;
    int isBurning = 0;
    
    while (obj < nObjects) {
        printf("\tCamera Obj %i has confidence %.2f and area %i\n", obj, get_object_confidence(0, obj), get_object_area(0, obj));
        
        if (get_object_confidence(0, obj) < 0.6f || get_object_area(0, obj) < 150) {
            ++obj;
            continue;
        }

        printf("\tCamera Obj %i identified as burning building\n", obj);
        isBurning = 1;
        break;
    }
    
    printf("\tCamera Operation done, closing camera: Success\n");
    camera_close();
    
    return isBurning;
}

void placeRectangleWater() {
    drive(70, -70);
    msleep(500*9/7);
    
    drive(40, -70); msleep(50);
    drive(0, -70); msleep(50);
    drive(-40, -70); msleep(50);
    
    drive(-70, -70);
    msleep(500*9/7);
    
    drive(-40, -70); msleep(50);
    drive(0, -70); msleep(50);
    drive(40, -70); msleep(50);
    
    drive(70, -70);
    msleep(500*9/7);
    
    drive(70, -40); msleep(50);
    drive(70, 0); msleep(50);
    drive(70, 40); msleep(50);
    
    drive(70, 70);
    msleep(500*9/7);
    
    drive(70, 40); msleep(50);
    drive(70, 0); msleep(50);
    drive(70, -40); msleep(50);
    
    drive(70, -70);
    msleep(1500*9/7);
    
    drive(40, -70); msleep(50);
    drive(0, -70); msleep(50);
    drive(-40, -70); msleep(50);
    
    drive(-70, -70);
    msleep(300*9/7);
    
    drive(-40, -65); msleep(50);
    drive(0, -60); msleep(50);
    drive(40, -55); msleep(50);
    
    drive(50, -50);
    msleep(400);
    
    while (pollSensor(5, TOPHAT_LEFT) < TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
    	msleep(5);
    }
    
    drive(20, -20); //10, 5 polls, 5 msec
    while (pollSensor(2, TOPHAT_LEFT) > TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
    	msleep(5);
    }
    
    drive(70, 70);
    interpolateServo(CLAW_SERVO, CLAW_CLOSEFIRM, CLAW_SLIP, 1500); //70,750
    
    drive(0, 0);
    msleep(200);
    
    drive(-30, 30);
    msleep(250);
    
    drive(0, 0);
    msleep(200);
	
    drive(-70, -70);
    msleep(1000); //500
    driveToLineFast(-70, 1500, 1);
}
            
void placeRectangle() {   
    drive(70, -70);
    msleep(3200*9/7); //70, 4000, 5 polls, 5 msec
    while (pollSensor(5, TOPHAT_LEFT) < TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
    	msleep(5);
    }
    
    drive(20, -20); //10, 5 polls, 5 msec
    while (pollSensor(2, TOPHAT_LEFT) > TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
    	msleep(5);
    }
    
    drive(70, 70);
    interpolateServo(CLAW_SERVO, CLAW_CLOSEFIRM, CLAW_SLIP, 1000); //70,750
    
    drive(0, 0);
    msleep(200);
    
    drive(-30, 30);
    msleep(250);
    
    drive(0, 0);
    msleep(200);
	
    drive(-70, -70);
    msleep(1000); //500
    driveToLineFast(-70, 1500, 1);
}

void placeCenter1Rectangle() {
    drive(-70, -70);
    while (pollSensor(5, TOPHAT_RIGHT) < TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT) {
        msleep(2);
        set_servo_position(CLAW_SERVO, CLAW_SQUEEZE);
    }
    
    drive(0, 0);
    msleep(200);
    
    drive(-40, -40);
    msleep(200);
    
    drive(0, 0);
    msleep(200);
    
    placeRectangle();
}

void placeCenter2Rectangle() {
    lineFollowRightSensor(1450, 90);    
    lineFollowRightSensorET(4000, 50);
    
    drive(-40, -40);
    msleep(250);
    drive(0, 0);
    placeRectangleWater();
}

void resetFromPlacementPosition() {
    lineFollowLeftSensorInsideEdge(720, 60);

    drive(30, 0); msleep(50);
    drive(60, 0); msleep(50);
    drive(100, 0);
    msleep(2000); //1500
    
    drive(60, 0);
    while (pollSensor(2, TOPHAT_LEFT) < TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        msleep(5);
    }

    drive(-40, 40);
    while (pollSensor(2, TOPHAT_RIGHT) < TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT) {
        msleep(5);
    }

    lineFollowRightSensorET(500, 33); 
}

void resetFromPlacementPositionCenter2() {
    drive(70, 70);
    msleep(500);

    drive(0, 30); msleep(50);
    drive(0, 60); msleep(50);
    drive(0, 100);
    msleep(1800);
    
    drive(0, 60);
    while (pollSensor(2, TOPHAT_RIGHT) < TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT) {
        msleep(5);
    }

    drive(50, -50);
    while (pollSensor(2, TOPHAT_LEFT) < TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        msleep(5);
    }

    drive(0, 0);
    msleep(200);

    lineFollowLeftSensor(750, 70);
}

void place_ambulance_and_fire_truck_non_burning() {
    placeCenter1Rectangle();
    
    //drive backwards through black line
    drive(-50, -50);
    msleep(300);
    driveToLine(-50, 300, -1);
	drive(-50, -50);
    msleep(300);
    
    //point turn - magic value
    drive(-10, 10); msleep(20);
    drive(-30, 30); msleep(20);
    drive(-50, 50); msleep(20);
    drive(-70, 70); msleep(POINT_TURN_MSLEEP*47/90);
    drive(0, 0);	msleep(200);
    
    //start driving toward fire truck - open the claw
    drive(100, 100);
    int startTime = systime();

    interpolateServo(CLAW_SERVO, CLAW_SLIP, CLAW_45, 650);

    //drive up to and through black line
    while (pollSensor(5, TOPHAT_LEFT) < TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        msleep(5);
    }
    msleep(20);
    
    while (pollSensor(5, TOPHAT_LEFT) > TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        msleep(5);
    }
    
    msleep(250);

    //start to close the claw after passing black line
    interpolateServo(CLAW_SERVO, CLAW_45, CLAW_CLOSEFOAM, 100);

    //drive back the exact distance driven forward
    drive(0, 0);
    int timeDrove = systime() - startTime;
    
    drive(-100, -100);
    msleep(timeDrove);

    //point turn backwards - magic value
    drive(10, -10); msleep(20);
    drive(30, -30); msleep(20);
    drive(50, -50); msleep(20);
    drive(70, -70); msleep(POINT_TURN_MSLEEP*5/9);
    drive(0, 0);	msleep(200);
    
    //drive up to and through black line
    driveToLine(40, 100, 1);
    drive(40, 40);
    msleep(50);
    driveToLine(40, 200, -1);

    resetFromPlacementPosition();
    
    placeCenter2Rectangle();
	
    resetFromPlacementPositionCenter2();
    lineFollowLeftSensor(50, 70);

    drive(-70, 70);
    msleep(2000);
    drive(-50, 50);
    while (pollSensor(2, TOPHAT_LEFT) > TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        msleep(5);
    }
    
    drive(0, 0);
    interpolateServo(CLAW_SERVO, CLAW_SLIP, CLAW_90, 500);
}

void place_ambulance_and_fire_truck_burning() {
    //when concurrent testing - add msleep in this part of the code so that le beveur can pick up pom pile
    placeCenter2Rectangle();
    
    resetFromPlacementPositionCenter2();
    
    //drive straight along black line, opening the claw
    lineFollowLeftSensor(750, 70);

    drive(90, 90);
    interpolateServo(CLAW_SERVO, CLAW_SLIP, CLAW_90, 1300);

    //find the fire station corner, make a slight turn because fire truck is off center
    alignLeftSensorToLine(40);
    
    drive(40, -40);
    msleep(100);

    //drive forward, grab fire truck
    drive(90, 90);
    interpolateServo(CLAW_SERVO, CLAW_90, CLAW_CLOSEFOAM, 800);

    //180 degree turn (until right sensor sees black of the line behind the robot)
    drive(-90, 90);
    msleep(1750);
    drive(-60, 60);
    while (pollSensor(5, TOPHAT_RIGHT) < TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT) {
        msleep(5);
    }

    //follow the line to base position
    lineFollowLeftSensorET(10000, 50);
    
    set_servo_position(CLAW_SERVO, CLAW_90);
    interpolateServo(CLAW_SERVO, CLAW_90, CLAW_CLOSEFOAM, 300);
    
    placeCenter1Rectangle();
    
    resetFromPlacementPosition();
    
    lineFollowRightSensor(450, 80);    
    
    drive(80, -80);
    msleep(1000);
   
    drive(60, -60);
    while (pollSensor(2, TOPHAT_LEFT) < TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        msleep(5);
    }

    drive(0, 0);
    interpolateServo(CLAW_SERVO, CLAW_SLIP, CLAW_90, 500);
}

void sweep_supplies() {
    interpolateServo(CLAW_SERVO, CLAW_90, CLAW_SUPPLIES, 500);
    
    lineFollowLeftSensor(3800, 90);
    
    drive(30, 30);
    interpolateServo(CLAW_SERVO, CLAW_SUPPLIES, CLAW_CLOSESUPPLIES, 600);
    
    drive(60, 60); msleep(50);
    drive(80, 80); msleep(200);
    
    alignLeftSensorToLine(30);
    drive(20, 0); msleep(20);
    drive(40, 0); msleep(20);
    drive(60, 0); msleep(20);
    drive(90, 0); msleep(ARC_TURN_MSLEEP);
    drive(0, 0);  msleep(200);
    
    driveToLine(70, 500, 1);
    driveToLine(70, 500, -1);
    
    drive(100, 100);
    msleep(2000);
    
    drive(-40, 40);
    interpolateServo(CLAW_SERVO, CLAW_CLOSESUPPLIES, CLAW_45, 250);
    
    drive(-50, -50);
    msleep(1000);
    
    drive(-80, -80);
    msleep(500);
    
 	drive(-50, -50);
    interpolateServo(CLAW_SERVO, CLAW_45, CLAW_PARALLEL, 300);
    
    driveToLineFast(-90, 2000, 1);
}

void starting_box_firefighter() {    
    drive(10, 0); msleep(50);
    drive(30, 0); msleep(50);
    drive(50, 0); msleep(50);
    drive(70, 0); msleep(500);
    
    drive(50, 0);
    while (pollSensor(2, TOPHAT_LEFT) < TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        msleep(5);
    }
    
    drive(70, 70);
    while (pollSensor(2, TOPHAT_LEFT) > TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        msleep(5);
    }
    
	drive(-50, 50);
    msleep(100);
    
    drive(-50, 50);
    unsigned long startTime = systime();
    while (pollSensor(2, TOPHAT_LEFT) > TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
        msleep(5);
    }
    
    msleep(systime() - startTime);
    
    drive(0, 0);
    interpolateServo(CLAW_SERVO, CLAW_PARALLEL, CLAW_OPENCUBE, 3000);
    
    drive(90, 90);
    msleep(1000);
    interpolateServo(CLAW_SERVO, CLAW_OPENCUBE, CLAW_CLOSEFIRM, 300);
    
    drive(-90, -90);
    msleep(600);
    
    drive(10, -10); msleep(20);
    drive(30, -30); msleep(20);
    drive(50, -50); msleep(20);
    drive(70, -70); msleep(POINT_TURN_MSLEEP);
    drive(0, 0);	msleep(200);
    
    driveToLine(-60, 1000, 1);
    
    drive(100, 100);	
    msleep(200);
    while (pollSensor(2, TOPHAT_LEFT) < TOPHAT_BLACK_WHITE_THRESHOLD_LEFT) {
    	msleep(20);
    }																					//drive toward med centers until right sensor sees the black line
    
    alignRightSensorToLine(30);
    lineFollowRightSensor(500, 80);
}

void grab_firefighter() {  
    lineFollowRightSensor(1000, 80);
    
    interpolateServo(CLAW_SERVO, CLAW_SLIP, CLAW_OPENCUBE, 100);
    
    drive(100, 100);
    msleep(250);
    interpolateServo(CLAW_SERVO, CLAW_OPENCUBE, CLAW_SQUEEZE, 400);
    
    drive(-80, -80);
    msleep(1150);
}

void deposit_firefighters(int isBurning, int pairNum) {
    drive(10, 0); msleep(50);
    drive(30, 0); msleep(50);
    drive(50, 0); msleep(50);
    drive(90, 0); msleep(2000);
    
    while (pollSensor(2, TOPHAT_RIGHT) < TOPHAT_BLACK_WHITE_THRESHOLD_RIGHT) {
    	msleep(5);
    }
    
    drive(-20, 20); msleep(50);
    drive(-40, 40); msleep(50);
    
    if (pairNum == 1) {
        drive(-60, 60); msleep(100);
    }
    
    set_servo_position(CLAW_SERVO, CLAW_CLOSEFIRM);
    driveToLine(80, 2000, 1);
    
    if (!isBurning) {
        driveToLine(40, 1000, -1);
        drive(70, 70); msleep(50);
        drive(90, 90); msleep(2000);
    }
    
    drive(-40, -40); msleep(50);
    drive(-80, -80); msleep(600);
    
    if (pairNum == 1) {
        drive(-80, -40); msleep(50);
        drive(-80, 0);   msleep(50);
        drive(-80, 40);  msleep(50);
        drive(-80, 80);  msleep(100);
        
        drive(-40, 40); msleep(50);
        drive(0, 0);	interpolateServo(CLAW_SERVO, CLAW_CLOSEFIRM, CLAW_OPENCUBE, 200);
        drive(-25, 25); msleep(50);
        drive(-50, 50); msleep(500);
        
        drive(-60, -35);  msleep(50);
        drive(-70, -70); msleep(1500*5/7);
        
        drive(-35, -70); msleep(50);
    	drive(0, -70);	 interpolateServo(CLAW_SERVO, CLAW_OPENCUBE, CLAW_SLIP, 200);
   		drive(35, -70);  msleep(50);
        drive(70, -70);  msleep(300);
        
        drive(0, -70); 	 msleep(50);
    	drive(-70, -70); msleep(500);
    }
    
    else {
        interpolateServo(CLAW_SERVO, CLAW_CLOSEFIRM, CLAW_OPENCUBE, 100);
        msleep(700);
        interpolateServo(CLAW_SERVO, CLAW_OPENCUBE, CLAW_SLIP, 200);
    }
    
    if (!isBurning) {
        msleep(3000);
    } 
    
    drive(-20, 0); msleep(20);
    drive(-40, 0); msleep(20);
    drive(-60, 0); msleep(20);
    drive(-90, 0); msleep(ARC_TURN_MSLEEP);	
    
    alignRightSensorToLine(30); 
}

int main(){
    
    unsigned long startTime = systime();
    
    setup();
    printf("Setup completed %.2f s\n", (systime() - startTime)/1000.f);
    
    alignRightSensorToLine(30);
    lineFollowRightSensor(500, 80);
    
    grab_firefighter();
    deposit_firefighters(0, 1);
    
    grab_firefighter();
    grab_firefighter();
    deposit_firefighters(0, 2);
    
    printf("2 firefighters %.2f s\n", (systime() - startTime)/1000.f);
    return 0;
    
    drive_to_med_center();
    printf("Drove to med center %.2f s, now running camera detection\n", (systime() - startTime)/1000.f); //21s
    
    int isBurning = med_center_is_burning(); //15
    
    switch (isBurning) {
        case -1:
            printf("Camera nonfunctional at time %.2f, assuming first med center is burning to save time\n", (systime() - startTime)/1000.f);
                            
        case 0:
            printf("Detected that med center is not burning at time %.2f s\n", (systime() - startTime)/1000.f);
            place_ambulance_and_fire_truck_non_burning();
            break;
       
        case 1:
            printf("Detected that med center is burning at time %.2f s\n", (systime() - startTime)/1000.f);
            place_ambulance_and_fire_truck_burning();
            break;
    }
    
    printf("Placed both ambulance and fire truck, sweeping supplies %.2f s\n", (systime() - startTime)/1000.f); //67 b, nb
        
    sweep_supplies();
    printf("Supplies Done %.2f s\n", (systime() - startTime)/1000.f);
    
    starting_box_firefighter();
    printf("Firefighter Grabbed %.2f s\n", (systime() - startTime)/1000.f); //1s
    
    firefighter_pair_one();
    
    return 0; 
}
