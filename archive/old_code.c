#include "drive.h"
#include <math.h>
#include <kipr/botball.h>


void drive_off() {
    off(MOT_RIGHT);
    off(MOT_LEFT);
}

void drive_freeze() {
    freeze(MOT_RIGHT);
    freeze(MOT_LEFT);
}

void drive_clear() {
    cmpc(MOT_RIGHT);
    cmpc(MOT_LEFT);
}

void drive(int left_speed, int right_speed) {
    mav(MOT_LEFT, left_speed);
    mav(MOT_RIGHT, right_speed);
}

void right(int degrees, double radius) {
    long left_arc = ((2 * radius + ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    long right_arc = ((2 * radius - ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    if (left_arc == 0l) {
        printf("Error, no turn. Aborting.");
        return;
    }
    int turn_r_speed = round(((float)right_arc / (float)left_arc) * SPD_R_TURN);
    if (turn_r_speed < 0)
        turn_r_speed = -turn_r_speed;
    if (left_arc > 0l)
        mav(MOT_LEFT, SPD_L_F);
    else
        mav(MOT_LEFT, -SPD_L_B);
    if (right_arc > 0l)
        mav(MOT_RIGHT, turn_r_speed);
    else
        mav(MOT_RIGHT, -turn_r_speed);
    left_arc += gmpc(MOT_LEFT);
    right_arc += gmpc(MOT_RIGHT);
    if (right_arc - gmpc(MOT_RIGHT) > 0l) {
        if (left_arc - gmpc(MOT_LEFT) > 0l) {
            while (right_arc > gmpc(MOT_RIGHT) || left_arc > gmpc(MOT_LEFT)) {
                if (right_arc < gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
                if (left_arc < gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
            }
        } else {
            while (right_arc > gmpc(MOT_RIGHT) || left_arc < gmpc(MOT_LEFT)) {
                if (right_arc < gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
                if (left_arc > gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
            }
        }
    } else {
        if (left_arc - gmpc(MOT_LEFT) > 0l) {
            while (right_arc < gmpc(MOT_RIGHT) || left_arc > gmpc(MOT_LEFT)) {
                if (right_arc > gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
                if (left_arc < gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
            }
        } else {
            while (right_arc < gmpc(MOT_RIGHT) || left_arc < gmpc(MOT_LEFT)) {
                if (right_arc > gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
                if (left_arc > gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
            }
        }
    }
    drive_freeze();
}

void right_speed(int degrees, double radius, int speed) {
    long left_arc = ((2 * radius + ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    long right_arc = ((2 * radius - ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    if (left_arc == 0l) {
        printf("Error, no turn. Aborting.");
        return;
    }
    int turn_r_speed = round(((float)right_arc / (float)left_arc) * SPD_R_TURN);
    if (turn_r_speed < 0)
        turn_r_speed = -turn_r_speed;
    if (left_arc > 0l)
        mav(MOT_LEFT, SPD_L_F * speed / MAX_SPEED);
    else
        mav(MOT_LEFT, -SPD_L_B * speed / MAX_SPEED);
    if (right_arc > 0l)
        mav(MOT_RIGHT, turn_r_speed * speed / MAX_SPEED);
    else
        mav(MOT_RIGHT, -turn_r_speed * speed / MAX_SPEED);
    left_arc += gmpc(MOT_LEFT);
    right_arc += gmpc(MOT_RIGHT);
    if (right_arc - gmpc(MOT_RIGHT) > 0l) {
        if (left_arc - gmpc(MOT_LEFT) > 0l) {
            while (right_arc > gmpc(MOT_RIGHT) || left_arc > gmpc(MOT_LEFT)) {
                if (right_arc < gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
                if (left_arc < gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
            }
        } else {
            while (right_arc > gmpc(MOT_RIGHT) || left_arc < gmpc(MOT_LEFT)) {
                if (right_arc < gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
                if (left_arc > gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
            }
        }
    } else {
        if (left_arc - gmpc(MOT_LEFT) > 0l) {
            while (right_arc < gmpc(MOT_RIGHT) || left_arc > gmpc(MOT_LEFT)) {
                if (right_arc > gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
                if (left_arc < gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
            }
        } else {
            while (right_arc < gmpc(MOT_RIGHT) || left_arc < gmpc(MOT_LEFT)) {
                if (right_arc > gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
                if (left_arc > gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
            }
        }
    }
    drive_freeze();
}

void left(int degrees, double radius) {
    long left_arc = ((2 * radius - ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    long right_arc = ((2 * radius + ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    if (right_arc == 0l) {
        printf("Error, no turn. Aborting.");
        return;
    }
    int turn_l_speed = round((float)left_arc / (float)right_arc * SPD_L_TURN);
    if (turn_l_speed < 0)
        turn_l_speed = -turn_l_speed;
    if (right_arc  > 0l)
        mav(MOT_RIGHT, SPD_R_F);
    else
        mav(MOT_RIGHT, -SPD_R_B);
    if (left_arc > 0l)
        mav(MOT_LEFT, turn_l_speed);
    else
        mav(MOT_LEFT, -turn_l_speed);
    right_arc += gmpc(MOT_RIGHT);
    left_arc += gmpc(MOT_LEFT);
    if (left_arc - gmpc(MOT_LEFT) > 0l) {
        if (right_arc - gmpc(MOT_RIGHT) > 0l) {
            while (left_arc > gmpc(MOT_LEFT) || right_arc > gmpc(MOT_RIGHT)) {
                if (left_arc < gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
                if (right_arc < gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
            }
        } else {
            while (left_arc > gmpc(MOT_LEFT) || right_arc < gmpc(MOT_RIGHT)) {
                if (left_arc < gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
                if (right_arc > gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
            }
        }
    } else {
        if (right_arc - gmpc(MOT_RIGHT) > 0l) {
            while (left_arc < gmpc(MOT_LEFT) || right_arc > gmpc(MOT_RIGHT)) {
                if (left_arc > gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
                if (right_arc < gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
            }
        } else {
            while (left_arc < gmpc(MOT_LEFT) || right_arc < gmpc(MOT_RIGHT)) {
                if (left_arc > gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
                if (right_arc > gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
            }
        }
    }
    drive_off();
}

void left_speed(int degrees, double radius, int speed) {
    long left_arc = ((2 * radius - ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    long right_arc = ((2 * radius + ROBOT_DIAMETER) * CM_TO_BEMF * M_PI) * (degrees / 360.);
    if (right_arc == 0l) {
        printf("Error, no turn. Aborting.");
        return;
    }
    int turn_l_speed = round((float)left_arc / (float)right_arc * SPD_L_TURN);
    if (turn_l_speed < 0)
        turn_l_speed = -turn_l_speed;
    if (right_arc  > 0l)
        mav(MOT_RIGHT, SPD_R_F * speed / MAX_SPEED);
    else
        mav(MOT_RIGHT, -SPD_R_B * speed / MAX_SPEED);
    if (left_arc > 0l)
        mav(MOT_LEFT, turn_l_speed * speed / MAX_SPEED);
    else
        mav(MOT_LEFT, -turn_l_speed * speed / MAX_SPEED);
    right_arc += gmpc(MOT_RIGHT);
    left_arc += gmpc(MOT_LEFT);
    if (left_arc - gmpc(MOT_LEFT) > 0l) {
        if (right_arc - gmpc(MOT_RIGHT) > 0l) {
            while (left_arc > gmpc(MOT_LEFT) || right_arc > gmpc(MOT_RIGHT)) {
                if (left_arc < gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
                if (right_arc < gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
            }
        } else {
            while (left_arc > gmpc(MOT_LEFT) || right_arc < gmpc(MOT_RIGHT)) {
                if (left_arc < gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
                if (right_arc > gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
            }
        }
    } else {
        if (right_arc - gmpc(MOT_RIGHT) > 0l) {
            while (left_arc < gmpc(MOT_LEFT) || right_arc > gmpc(MOT_RIGHT)) {
                if (left_arc > gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
                if (right_arc < gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
            }
        } else {
            while (left_arc < gmpc(MOT_LEFT) || right_arc < gmpc(MOT_RIGHT)) {
                if (left_arc > gmpc(MOT_LEFT))
                    freeze(MOT_LEFT);
                if (right_arc > gmpc(MOT_RIGHT))
                    freeze(MOT_RIGHT);
            }
        }
    }
    drive_off();
}

void forward(int distance) {
    if (distance < 0) {
        distance = -distance;
        printf("Error, negative distance! Switching to positive\n");
    }
    long move_distance = distance * CM_TO_BEMF;
    long l_target = gmpc(MOT_LEFT) + move_distance;
    long r_target = gmpc(MOT_RIGHT) + move_distance;
    mav(MOT_LEFT, SPD_L_F);
    mav(MOT_RIGHT, SPD_R_F);
    while (gmpc(MOT_LEFT) < l_target && gmpc(MOT_RIGHT) < r_target) {
        if (gmpc(MOT_LEFT) >= l_target)
            freeze(MOT_LEFT);
        if (gmpc(MOT_RIGHT) >= r_target)
            freeze(MOT_RIGHT);
    }
    drive_freeze();
}

void forward_speed(int distance, int speed) {
    if (distance < 0) {
        distance = -distance;
        printf("Error, negative distance! Switching to positive\n");
    }
    long move_distance = distance * CM_TO_BEMF;
    long l_target = gmpc(MOT_LEFT) + move_distance;
    long r_target = gmpc(MOT_RIGHT) + move_distance;
    mav(MOT_LEFT, speed * SPD_L_F / MAX_SPEED);
    mav(MOT_RIGHT, speed * SPD_R_F / MAX_SPEED);
    while (gmpc(MOT_LEFT) < l_target && gmpc(MOT_RIGHT) < r_target) {
        if (gmpc(MOT_LEFT) >= l_target)
            freeze(MOT_LEFT);
        if (gmpc(MOT_RIGHT) >= r_target)
            freeze(MOT_RIGHT);
    }
    drive_freeze();
}

void backward(int distance) {
    if (distance < 0) {
        distance = -distance;
        printf("Error, negative distance! Switching to positive\n");
    }
    long move_distance = distance * CM_TO_BEMF;
    long l_target = gmpc(MOT_LEFT) - move_distance;
    long r_target = gmpc(MOT_RIGHT) - move_distance;
    mav(MOT_LEFT, -SPD_L_B);
    mav(MOT_RIGHT, -SPD_R_B);
    while (gmpc(MOT_LEFT) > l_target && gmpc(MOT_RIGHT) > r_target) {
        if (gmpc(MOT_LEFT) <= l_target)
            freeze(MOT_LEFT);
        if (gmpc(MOT_RIGHT) <= r_target)
            freeze(MOT_RIGHT);
    }
    drive_freeze();
}

void backward_speed(int distance, int speed) {
    if (distance < 0) {
        distance = -distance;
        printf("Error, negative distance! Switching to positive\n");
    }
    long move_distance = distance * CM_TO_BEMF;
    long l_target = gmpc(MOT_LEFT) - move_distance;
    long r_target = gmpc(MOT_RIGHT) - move_distance;
    mav(MOT_LEFT, -speed * SPD_L_B / MAX_SPEED);
    mav(MOT_RIGHT, -speed * SPD_R_B / MAX_SPEED);
    while (gmpc(MOT_LEFT) > l_target && gmpc(MOT_RIGHT) > r_target) {
        if (gmpc(MOT_LEFT) <= l_target)
            freeze(MOT_LEFT);
        if (gmpc(MOT_RIGHT) <= r_target)
            freeze(MOT_RIGHT);
    }
    drive_freeze();
}

void forward_gyro(int distance, int speed) {
    if (distance < 0) {
        distance = -distance;
        printf("Error, negative distance! Switching to positive\n");
    }
    if (speed >= 1450) {
        speed = 1400;  // Cannot be full speed or slightly less otherwise gyro corrections won't work
    }

    // TODO: Some gyro calibration code here - Kipr has a function but hasn't implemented yet

    long move_distance = distance * CM_TO_BEMF;
    long l_target = gmpc(MOT_LEFT) + move_distance;
    long r_target = gmpc(MOT_RIGHT) + move_distance;

    mav(MOT_LEFT, speed);
    mav(MOT_RIGHT, speed);

    while (gmpc(MOT_LEFT) > l_target && gmpc(MOT_RIGHT) > r_target) {
        // Not sure which one to use here
        double gyroX = gyro_x();
        if (gyroX > 0.1) {
            mav(MOT_RIGHT, speed*Kp);
            mav(MOT_LEFT, speed);
            msleep(10);
        }
        if (gyroX < 0.1) {
            mav(MOT_LEFT, speed*Kp);
            mav(MOT_RIGHT, speed);
            msleep(10);
        }

        if (gmpc(MOT_LEFT) <= l_target)
            freeze(MOT_LEFT);
        if (gmpc(MOT_RIGHT) <= r_target)
            freeze(MOT_RIGHT);
    }
}

void right_turn(int speed, float degrees) {
    msleep(50);
    gyro_z();
    float offset = 0;
    float final_rot = -degrees * GYRO_PER_ROT / 360;
    while(offset > final_rot) {
        drive(speed * LEFT_OFFSET, -speed * RIGHT_OFFSET);
        if (offset < final_rot * 0.6)
            drive(speed * LEFT_OFFSET / 2, -speed * RIGHT_OFFSET / 2);
        int val = gyro_z();
        offset += val - GYRO_DEV;
        msleep(50);
    }
    ao();
    msleep(100);
}

void left_turn(int speed, float degrees) {
    msleep(50);
    gyro_z();
    float offset = 0;
    float final_rot = degrees * GYRO_PER_ROT / 360;
    while(offset < final_rot) {
        create_drive_direct(-speed * LEFT_OFFSET, speed * RIGHT_OFFSET);
        if (offset > final_rot * 0.6)
            drive(-speed * LEFT_OFFSET / 2, speed * RIGHT_OFFSET / 2);
        int val = gyro_z();
        offset += val - GYRO_DEV;
        msleep(50);
    }
    ao();
    msleep(100);
}

// -------------------------------------------------------------------------------------------------------------------------------------------------------------

#include <kipr/botball.h>
#define LEFT_MOTOR 0
#define RIGHT_MOTOR 1
#define ARM 1 // make sure this matches with actual ports
#define CLAW 0
#define FRONT_LEFT_IR 0
#define FRONT_RIGHT_IR 1
#define BACK_LEFT_IR 2
#define BACK_RIGHT_IR 3
#define LIGHT 5 //MAKE SURE THIS IS RIGHT

///////////////////////////
///    ENUMS/Globals    ///
///////////////////////////

int counter = 0;

typedef enum {
    false,
    true
} bool;

typedef enum {
    CCW = 1,
    CW = -1,
    FORWARD = 1,
    BACKWARD = -1
} Direction;

typedef enum {
    CLAW_GRAB = 1639,
    CLAW_RELEASE = 980, //1590
    CLAW_START = 1655,
    CLAW_OPEN = 550,
    CLAW_SECOND_CUBE = 1500,
    CLAW_OPEN_FOR_PEOPLE = 1200,
    CLAW_CLOSE_FOR_PEOPLE = 1700

} ClawPosition;

typedef enum {
    LEFT_IR_LIMIT = 3500,
    RIGHT_IR_LIMIT = 3500
} IRSensorsMaximumValues;

typedef enum {
    LEFT_WHITE = 2000,
    RIGHT_WHITE = 1500,
    RIGHT_WHITE_DUCT = 2000
} WhiteReading;

typedef enum {
    FRONT_LEFT_BLACK = 2000,
    FRONT_RIGHT_BLACK = 1500,
    BACK_LEFT_BLACK = 3500,
    BACK_RIGHT_BLACK = 3000
} BlackReading; // black > BlackReading

///////////////////////////////////
///    Function Declarations    ///
///////////////////////////////////

/***** Movement *****/

void move(int leftSpeed, int rightSpeed, int time); // manual movement
void quick_burst(int time, Direction direction);
void align_with_tape(Direction direction, bool useFrontSensors);
void move_along_main_street();
void move_out_with_cube();
//void move_when_both_sensors_see_black();

/***** Rotation *****/

void pivot(int pivot, double degrees, int direction);
void rotate(double degrees, int direction);
void pivot_strong(int pivot, double degrees, int direction);
void rotate_strong(double degrees, int direction);

/***** Servos *****/

void set_claw_to(ClawPosition endPos);
void set_claw_slowly_to(int endPos);
void restart();

///////////////////
///    Main    ////
///////////////////

int main() {
    /**/

    restart();
    wait_for_light(LIGHT);
    shut_down_in(119);


    // Preconditions: Closed claw (1959), upper-right most position in box possible, fingers slightly over 45 degrees from the line of sight
    // Positions: Ambulance left (somewhere that's not touching fingers), Firefighter right (flush with right finger)


    //////////////////// Phase 1A: Main Street ////////////////////

    printf("R CW 90\n");
    pivot(RIGHT_MOTOR, 101, CW);// a little over 90 deg. just so left IR sensor isn't so close to edge of box

    printf("out of box\n");
    quick_burst(2450, FORWARD);

    printf("align with tape\n");
    align_with_tape(FORWARD, true);

    printf("move forward a bit\n");
    quick_burst(100, FORWARD);

    printf("Pivot ccw around left wheel\n");
    pivot(LEFT_MOTOR, 97, CCW);

    printf("Go to main street\n");
    quick_burst(1400, FORWARD);

    printf("Align with tape\n");
    align_with_tape(FORWARD, false);

    printf("Pivot ccw around left wheel\n");
    pivot(LEFT_MOTOR, 87, CCW);

    //////////////////// Phase 1B: Little Items //////////////////// move into far med building


    printf("Move to med center\n");
    move_along_main_street(); // aligning with tape included

    printf(" rotate to right med building \n");
    move(90,35,1150);//800

    printf(" push things in a bit more \n");
    quick_burst(200, FORWARD);//300

    printf("move back \n");
    quick_burst(1250, BACKWARD);//2000

    printf("Pivot cw around left wheel\n");
    pivot(LEFT_MOTOR, 35, CCW);

    //////////////////// Phase 2: Prism  ////////////////////
    printf("speed up a bit\n");
    quick_burst(400, FORWARD);

    printf("align with tape backwards \n");
    align_with_tape(BACKWARD, true);

    printf("open claw\n");
    set_claw_to(CLAW_OPEN);

    printf("rotate to face prism\n");
    rotate(104,CW);

    printf("Move to fire truck\n");
    quick_burst(1300, BACKWARD);

    printf("Grab fire truck\n");
    set_claw_slowly_to(CLAW_GRAB);

    printf("move drop fire truck\n");
    quick_burst(2400, FORWARD);//1600

    printf("rotate to face med building\n");
    rotate(127,CW);//1

    printf("move back to drop fire truck\n");
    quick_burst(200, BACKWARD);//1600

    printf("Drop fire truck \n");
    set_claw_to(CLAW_OPEN);

    //////////////////// Phase 3: Firefighter 1 ////////////////////
    printf(" move out from dropping fire truck\n");
    quick_burst(950, FORWARD);//1200

    printf("rotate\n");
    rotate(53,CW);//53

    printf("speed up a bit\n");
    quick_burst(100,FORWARD);//

    printf("Align with tape forward \n");
    align_with_tape(FORWARD, false);

    printf(" move out from dropping fire truck\n");
    quick_burst(2250, FORWARD);//2350

    printf("rotate\n");
    rotate(90,CCW);//1

    // quick_burst(250, BACKWARD);

    printf("Align with tape backwards \n");
    align_with_tape(BACKWARD, false);

    printf("Pivot cw around left wheel \n");
    pivot(LEFT_MOTOR, 4 , CW);//3

    printf("Open claw \n");
    set_claw_to(CLAW_OPEN);

    printf("Go to firefighter pole \n");
    quick_burst(480, BACKWARD);

    printf("Grab firefighter \n");
    set_claw_to(CLAW_GRAB);

    printf("Move out a bit while holding cube so that it wonbt hit PVC \n");
    quick_burst(100, FORWARD);

    printf("Rotate ccw \n");
    rotate(60,CCW);

    printf("Move out from cube \n");
    quick_burst(1000, FORWARD);

    printf(" rotate so that cufe is facing me building \n");
    rotate(123,CW);//113

    printf("move back to drop fire firefighter\n");
    quick_burst(1900, BACKWARD);

    printf("Drop firefighter \n");
    set_claw_to(CLAW_RELEASE);

    //////////////////// Phase 4: Firefighter 2 ////////////////////

    printf(" move out from dropping 1st cube\n");
    quick_burst(2300, FORWARD);//2500

    printf("Pivot ccw around left wheel\n");
    pivot(LEFT_MOTOR, 105 , CCW);

    // quick_burst(100, FORWARD);

    printf("Align with tape backwards \n");
    align_with_tape(BACKWARD, false);

    printf("Pivot cw \n");
    pivot(LEFT_MOTOR, 4, CW);

    printf("Open claw \n");
    set_claw_to(CLAW_OPEN);

    printf("Go to firefighter pole \n");
    quick_burst(490, BACKWARD);

    printf("Grab firefighter \n");
    set_claw_to(CLAW_GRAB);

    printf("Move out a bit while holding cube so that it wonbt hit PVC \n");
    quick_burst(200, FORWARD);

    printf("Pivot ccw around left wheel\n");
    pivot(LEFT_MOTOR, 20, CCW);

    printf("Move forward a bit \n");
    quick_burst(300, FORWARD);

    //////////////////// Phase 5: Firefighter 3 ////////////////////

    printf("Align with tape backwards \n");
    align_with_tape(BACKWARD, false);

    printf("Pivot cw around left wheel\n");
    pivot(LEFT_MOTOR, 4, CW);

    printf("Open claw \n");
    set_claw_to(CLAW_OPEN);

    printf("Go to firefighter pole \n");
    quick_burst(490, BACKWARD);

    printf("Grab second firefighter \n");
    set_claw_to(CLAW_SECOND_CUBE);

    printf("come out at an angle with fire fighters\n");
    move(30,80,2000);

    printf("Rotate CW\n");
    rotate(120,CW);

    printf("Speed up\n");
    quick_burst(700, BACKWARD);

    printf("Open claw \n");
    set_claw_to(CLAW_OPEN);

    //////////////////// Phase 6: Firefighter 4 ////////////////////

    printf("move out from dropping 2nd and 3rd cube\n");
    quick_burst(1200, FORWARD);//1300

    printf("pivot 90 CW \n");
    rotate(80, CCW);

    printf("align with tape back \n");
    align_with_tape(BACKWARD, false);

    printf("Pivot cw \n");
    pivot(LEFT_MOTOR, 3, CW);

    printf("Open claw \n");
    set_claw_to(CLAW_OPEN);

    printf("Go to firefighter pole \n");
    quick_burst(490, BACKWARD);

    printf("Grab firefighter \n");
    set_claw_to(CLAW_GRAB);

    printf("Move out a bit while holding cube so that it wonbt hit PVC \n");
    quick_burst(200, FORWARD);

    printf("Pivot ccw around left wheel\n");
    pivot(LEFT_MOTOR, 20, CCW);

    printf("Move forward a bit \n");
    quick_burst(500, FORWARD);

    //////////////////// Phase ?: Firefighter 5 ////////////////////

    printf("Align with tape backwards \n");
    align_with_tape(BACKWARD, false);

    printf("Pivot cw around left wheel\n");
    pivot(LEFT_MOTOR, 4, CW);

    printf("Open claw \n");
    set_claw_to(CLAW_OPEN);

    printf("Go to firefighter pole \n");
    quick_burst(480, BACKWARD);

    printf("Grab second firefighter \n");
    set_claw_to(CLAW_SECOND_CUBE);

    printf("come out at an angle with fire fighters\n");
    move(30,80,2000);

    printf("Rotate CW\n");
    rotate(120,CW);

    printf("Speed Up \n");
    quick_burst(700, BACKWARD);

    printf("Open claw \n");
    set_claw_to(CLAW_OPEN);


    //////////////////// Phase ?: PEOPLE ////////////////////
    printf("move out from dropping\n");
    quick_burst(1100, FORWARD);

    printf("pivot 90 CCW \n");
    rotate(43, CCW);

    printf("Move to DRZ\n");
    quick_burst(1000, FORWARD);

    printf("align with tape forward \n");
    align_with_tape(FORWARD, true);

    printf("Move to center of tape\n");
    quick_burst(25, FORWARD);

    printf("Pivot cw around left wheel\n");
    pivot(LEFT_MOTOR, 90, CW);

    printf("Move to main street \n");
    quick_burst(1300, BACKWARD);

    printf("align with tape forward \n");
    align_with_tape(BACKWARD, false);

    printf("open claw for people \n");
    set_claw_to(CLAW_OPEN_FOR_PEOPLE);

    printf("Move to blue tape \n");
    quick_burst(1000, BACKWARD);

    printf("rotate to face entry properly \n");
    rotate(2, CW);

    printf("Move to blue tape \n");
    quick_burst(850, BACKWARD);

    printf("close claw \n");
    set_claw_slowly_to(CLAW_CLOSE_FOR_PEOPLE);

    printf("Move to blue tape \n");
    quick_burst(1550, FORWARD);

    printf("align with tape  \n");
    align_with_tape(FORWARD, false);

    printf("move so that people are centered on tape \n");
    quick_burst(700, FORWARD);

    //////bring prople to end zone

    printf("claw open \n");
    set_claw_to(CLAW_OPEN);

    printf("Move away from dropping the people at main street \n");
    quick_burst(500, FORWARD);

    printf("Rotate CCW \n");
    rotate(180, CCW);

    quick_burst(600, FORWARD);

    printf("Align with tape forwards \n");
    align_with_tape(FORWARD, true);

    printf("Pivot ccw around left wheel\n");
    pivot(LEFT_MOTOR, 87, CCW);

    printf("Move to med center\n");
    quick_burst(1700, FORWARD);

    /* */


    //printf("align with tape back \n");
    //align_with_tape(BACKWARD, false);
    /**/
    return 0;
}

///////////////////////
///    Movements    ///
///////////////////////

/* Set velocities of the wheels for a specific amount of time */
void move(int L_speed, int R_speed, int time) {
    motor(LEFT_MOTOR, L_speed);
    motor(RIGHT_MOTOR, R_speed);
    msleep(time);
    ao();
}

/* Move at maximum velocity in a certain direction for a specified period of time */
void quick_burst(int time, Direction direction) {
    motor(LEFT_MOTOR, 100 * direction);
    motor(RIGHT_MOTOR, 96 * direction);
    msleep(time);
    ao();
    msleep(175);
}

/* Align with a line of tape horizontally with front or back sensors */
void align_with_tape(Direction direction, bool useFrontSensors) {
    // align with the tape going *across* the robot, not from back to front
    // normal align_with_tape uses front

    int leftBlack = useFrontSensors ? FRONT_LEFT_BLACK : BACK_LEFT_BLACK;
    int rightBlack = useFrontSensors ? FRONT_RIGHT_BLACK : BACK_RIGHT_BLACK;
    int leftIR = useFrontSensors ? FRONT_LEFT_IR : BACK_LEFT_IR;
    int rightIR = useFrontSensors ? FRONT_RIGHT_IR : BACK_RIGHT_IR;

    int left = analog(leftIR);
    int right = analog(rightIR);
    // int i = 0;

    while ( left <= leftBlack || right <= rightBlack ) {
        if (left > leftBlack) {
            motor(LEFT_MOTOR, -10 * direction);
            motor(RIGHT_MOTOR, 28 * direction);
        } else if (right > rightBlack) {
            motor(RIGHT_MOTOR, -10 * direction);
            motor(LEFT_MOTOR, 28 * direction);
        } else {
            motor(LEFT_MOTOR, 31 * direction);
            motor(RIGHT_MOTOR, 28 * direction);
        }

        //i++;
        left = analog(leftIR);
        right = analog(rightIR);
        msleep(2);
    }
    ao();
}

/* Move along the long strip of tape that spans most of the board until both front sensors see black */
void move_along_main_street() {
    int left = analog(FRONT_LEFT_IR);
    int right = analog(FRONT_RIGHT_IR);
    int initial = get_motor_position_counter(LEFT_MOTOR);

    // left white = 2000
    // right white = 1500
    int traveled = traveled = get_motor_position_counter(LEFT_MOTOR) - initial;
    while ((left <= FRONT_LEFT_BLACK || right <= FRONT_RIGHT_BLACK) || /*counter < 1400*/ traveled < 13500) { // 250 250
        /*if (left < FRONT_LEFT_BLACK && right < FRONT_RIGHT_BLACK) {
      motor(LEFT_MOTOR, 98);
      motor(RIGHT_MOTOR, 100);

    }*/
        traveled = get_motor_position_counter(LEFT_MOTOR) - initial;
        if (traveled > 6600 && traveled < 7750) {
            motor(LEFT_MOTOR, 25);
            motor(RIGHT_MOTOR, 25);
        } else if (left > FRONT_LEFT_BLACK) {
            motor(LEFT_MOTOR, 100); // 80
            motor(RIGHT_MOTOR, 85); // 100
            //printf("yup left sees black\n");
        } else if (right > FRONT_RIGHT_BLACK) { // right > RIGHT_WHITE
            motor(LEFT_MOTOR, 100);
            motor(RIGHT_MOTOR, 75);
            //printf("yup right sees black\n");

        } else {
            motor(LEFT_MOTOR, 95);
            motor(RIGHT_MOTOR, 100);
        }

        msleep(1);
        counter++;
        //if (counter % 5 == 0) printf("%d\n", counter);
        left = analog(FRONT_LEFT_IR);
        right = analog(FRONT_RIGHT_IR);
    }


    /*
  left = analog(FRONT_LEFT_IR);
  right = analog(FRONT_RIGHT_IR);

  int i = 0;
  while (small > 3850 && i < 1000) { // i < 900
    if (left < LEFT_BLACK && right < RIGHT_BLACK) {
      motor(LEFT_MOTOR, 62);
      motor(RIGHT_MOTOR, 60);
      //printf("straight\n");
    } else if (left > LEFT_BLACK) {
      motor(LEFT_MOTOR, 40);
      motor(RIGHT_MOTOR, 50);
      //printf("yup left sees black\n");
    } else if (right > RIGHT_BLACK) {
      motor(LEFT_MOTOR,50);
      motor(RIGHT_MOTOR, 40);
      //printf("yup right sees black\n");
    }

    msleep(1);
    i++;
    left = analog(LEFT_IR);
    right = analog(RIGHT_IR);
    small = analog(SMALL_IR);
  }
  */

    ao();
}


//////////////////////
///    Rotation    ///
//////////////////////

/* Pivot a certain angle in a specified direction around a specified wheel */
void pivot(int pivot, double degrees, Direction direction) {
    // slightly more powerful than should be for the specified angle to be achieved
    if (pivot == LEFT_MOTOR) {
        motor(RIGHT_MOTOR, 70 * direction);
    } else {
        motor(LEFT_MOTOR, -70 * direction);//22
    }

    double revolutionTime = pivot == 0 ? 6400.0 : 6300.0;
    msleep(revolutionTime * degrees / 360.0);
    ao();
}

/* Rotate a certain angle in a specified direction about the center */
void rotate(double degrees, Direction direction) {
    motor(RIGHT_MOTOR, 33 * direction);
    motor(LEFT_MOTOR, -33 * direction);

    msleep(6850.0 * degrees / 360.0);
    ao();
}


////////////////////
///    Servos    ///
////////////////////

/* Set claw to a position */
void set_claw_to(ClawPosition endPos) {
    enable_servo(CLAW);
    set_servo_position(CLAW, endPos);
    msleep(300);
}

/* Set claw to a position using a slower motion */\
void set_claw_slowly_to(int endPos) {
    int currentPos = get_servo_position(CLAW);
    bool testGreaterThan = currentPos > endPos; // continue loop while startPos is greater than
    int increment = testGreaterThan ? -2 : 2;

    while ((testGreaterThan && currentPos > endPos) || (!testGreaterThan && currentPos < endPos)) {
        currentPos += increment;
        set_servo_position(CLAW, currentPos);
        msleep(3);
    }
    // for a good measure of how quick the servo moves, use the ratio of increment to msleep (in this case, 3/3) to compare to other speeds
}

/* Reset claw position */
void restart() {
    enable_servo(CLAW);
    set_servo_position(CLAW, CLAW_START);
}

// -------------------------------------------------------------------------------------------------------------------------------------------------------------

#include <kipr/botball.h>
#include <drive.h>
#include <stdio.h>
#include <stdbool.h>

double gyrocalibrate() {
    double changeGyroZ = 0;//find average gyro value when still
    int i = 0;
    for(i = 0; i < 2000; i++) {
        changeGyroZ += gyro_z();
    }
    changeGyroZ /= 2000;

    return changeGyroZ;
}

void move(int speed, int time, double changeGyroZ) {//speed -1500 to 1500, time is in miliseconds
    int i = 0;
    double anglechange = 0;
    for(i = 0; i < time; i++) {//gyro move
        anglechange += gyro_z()-changeGyroZ;
        mav(MOT_LEFT, speed+anglechange/10);
        mav(MOT_RIGHT, speed-anglechange/10);
    }
    ao();
}

void turn(int direction, int degree, int changeGyroZ) {
    double anglechange = 0;
    int x = degree*270000/90;
    x /= 10;
    if(direction == 1) { // 1 = left, 0 = right
        while(anglechange > -x) {
            anglechange -= gyro_z()-changeGyroZ;
            motor(2, -100);
            motor(3, 100);
            msleep(10);
        }
    } else {
        while(anglechange < x) {
            anglechange -= gyro_z()-changeGyroZ;
            motor(2, 100);
            motor(3, -100);
            msleep(10);
        }
    }
    ao();
}

void turnLeft(int degrees) {
    motor(2, 100);
    motor(3, -100);
    msleep(degrees*12);
    ao();
}

void turnRight(int degrees) {
    motor(2, -100);
    motor(3, 100);
    msleep(degrees*10);
    ao();
}

void pingpong(){
    double change = gyrocalibrate();
    while(true){
        move(1500, 1000, change);
        move(-1500, 1000, change);
    }
}

void findLine()  {
    bool found = false;
    while (found == false) {
        if (analog(0) < 2800) {
        	motor(2, 100);
        	motor(3, 100);
        	msleep(10);
        }
        else {
            turnRight(80);
            found = true;
        }
    }
}

void wait_button(){
	while(right_button() == 0){
    	msleep(2);
    }
}

int main() {

    double change = gyrocalibrate();

    shut_down_in(119);
    //wait_button();
    wait_for_light(1);
    move(1500, 3300, change);
    msleep(500);
    findLine();
    msleep(500);

    move(1500, 5000, change);

    return 0;
}

// -------------------------------------------------------------------------------------------------------------------------------------------------------------

/* Drive.h
	* The drive library contains code that should work on any robot running on two powered, forward-
	* facing motors. The main purpose of this library is to facilitate usage of four movements:
	* Forward, Backward, Left, and Right. Constants defined as $ need to be defined by the user.
	* !TODO! Left/Right with gyro.
	* Clean up right/left by using math.h's absolute value function.
	*/

	// This segment prevents including Drive.h twice.
	#ifndef DRIVE_H
	#define DRIVE_H

	#define PI 3.1415926539

	// Hard constants
	#define MAX_SPEED 1500

	// Mathematically defined constants
	#define CM_TO_BEMF (BEMFS_PER_ROTATION / (PI * WHEEL_DIAMETER)) //Centimeters travelled to motor ticks.
	#define BEMF_TO_CM ((PI * WHEEL_DIAMETER) / BEMFS_PER_ROTATION) //Motor ticks to centimeters travelled.
	#define SPD_L_TURN ((SPD_L_B + SPD_R_F) / 2)
	#define SPD_R_TURN ((SPD_R_B + SPD_L_F) / 2)

	// Robot-specific constants
	#define MOT_LEFT 3 // Port the left drive motor is plugged into.
	#define MOT_RIGHT 2 // Port the right drive motor is plugged into.
	#define WHEEL_DIAMETER 5.5 // Diameter of the wheel. Common values in appendix
	#define ROBOT_DIAMETER 19 // Distance from the center of one wheel to the center of the other.
	#define BEMFS_PER_ROTATION 1500 // Motor ticks per rotation. KIPR says this value should be around 1500.

	// Tuning Constants
	#define SPD_L_F 1500 // Left forward speed. Max is 1500.
	#define SPD_R_F 1450 // Right forward speed. Max is 1500.
	#define SPD_L_B 1500 // Left backward speed. Max is 1500.
	#define SPD_R_B 1410 // Right backward speed. Max is 1500.

	// Gyro Constants
	#define Kp 1 // Proportionality constant for Gyro driving
	double GYRO_PER_ROT;
	double GYRO_DEV;
	#define RIGHT_OFFSET 1
	#define LEFT_OFFSET 1

	// Low-Level drive commands

	/*
	* \brief Shuts down the two drive motors.
	*/
	void drive_off();

	/* (!NEEDS TESTING!)
	* \brief Actively brakes to shut off the two drive motors for more precision.
	*/
	void drive_freeze();

	/*
	* \brief Clears the position counter on the two drive motors.
	*/
	void drive_clear();

	/*
	* \brief Sets the two drive motors to move at certain speeds.
	* \param left_speed the speed of the left motor.
	* \param right_speed the speed of the right motor.
	*/
	void drive(int left_speed, int right_speed);

	// Main drive commands.

	/*
	* \brief Turns right a certain amount of degrees on a certain radius
	* \param degrees the amount to turn, in degrees. Negative values do a reverse turn.
	* \param radius the radius of the turn, in centimeters. Only use values 0 and above.
	* NOTE: degrees is an int because the wallaby only has precision up to about 2-3 degrees.
	*/
	void right(int degrees, double radius);

	/*
	* \brief Turns right a certain amount of degrees on a certain radius
	* \param degrees the amount to turn, in degrees. Negative values do a reverse turn.
	* \param radius the radius of the turn, in centimeters. Only use values 0 and above.
	* \param speed the speed to travel at.
	* NOTE: degrees is an int because the wallaby only has precision up to about 2-3 degrees.
	*/
	void right_speed(int degrees, double radius, int speed);

	/*
	* \brief Turns left a certain amount of degrees on a certain radius
	* \param degrees the amount to turn, in degrees. Negative values do a reverse turn.
	* \param radius the radius of the turn, in centimeters. Only use values 0 and above.
	* NOTE: degrees is an int because the wallaby only has precision up to about 2-3 degrees.
	*/
	void left(int degrees, double radius);

	/*
	* \brief Turns left a certain amount of degrees on a certain radius
	* \param degrees the amount to turn, in degrees. Negative values do a reverse turn.
	* \param radius the radius of the turn, in centimeters. Only use values 0 and above.
	* \param speed the speed to travel at.
	* NOTE: degrees is an int because the wallaby only has precision up to about 2-3 degrees.
	*/
	void left_speed(int degrees, double radius, int speed);

	/*
	* \brief Drives forward a certain distance at default drive speed.
	* \param distance the distance to travel, in centimeters. Only use values 0 and above.
	* NOTE: distance is an integer as the wallaby only has precision up to about 1-2 centimeters.
	*/
	void forward(int distance);

	/*
	* \brief Drives forward a certain distance at a custom speed.
	* \param distance the distance to travel, in centimeters. Only use values 0 and above.
	* \param speed the speed to travel at.
	* NOTE: distance is an integer as the wallaby only has precision up to about 1-2 centimeters.
	*/
	void forward_speed(int distance, int speed);

	/*
	* \brief Drives backward a certain distance at default drive speed.
	* \param distance the distance to travel, in centimeters. Only use values 0 and above.
	* NOTE: distance is an integer as the wallaby only has precision up to about 1-2 centimeters.
	*/
	void backward(int distance);

	/*
	* \brief Drives backward a certain distance at default drive speed.
	* \param distance the distance to travel, in centimeters. Only use values 0 and above.
	* \param speed the speed to travel at.
	* NOTE: distance is an integer as the wallaby only has precision up to about 1-2 centimeters.
	*/
	void backward_speed(int distance, int speed);

	void left_turn(int speed, float degrees);

	void right_turn(int speed, float degrees);


	#endif

	/* APPENDIX
	* Common wheel diameter values: (To be added at a future date)
	*
	*/

  // -----------------------------------------------------------------------------------------------------------------------------------------------------------
