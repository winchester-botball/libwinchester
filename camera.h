#ifndef CAMERA_H
#define CAMERA_H

void flagCamera();
void check_camera();
int updateCamera();
void checkStatus(int status, char failureMessage []);
void warmupCamera(int interval, int timeout);

int object_centered_in_area(point2* center, rectangle* r);
int object_partially_in_area(rectangle* bbox, rectangle* r);
int object_fully_in_area(rectangle* bbox, rectangle* r);


#endif 
