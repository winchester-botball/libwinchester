#include "create.h"
#include <kipr/wombat.h>

/** Used to Start-up the create
 * 
 * Starts up the create and prints the battery level in percent
 * 
 * @ speed In mav, the speed that the motors' will turn to execute the turn, thus the speed the turn is executed at
 * @ degrees How much the bot will turn in terms of degrees
 * 
 */
void create_start() {
    create_connect();
    create_full();

    printf("Create Battery %f%%\n", ((float)get_create_battery_charge()) / ((float)get_create_battery_capacity()) * 100.0);
    printf("Charging: %s\n", get_create_battery_charging_state() ? "true" : "false");
}

/** Sets the speed of rotation for the create's wheels
 * 
 * Used to make the create drive/move by setting the speeds respectivitly of the right and left wheel motor's. 
 * The motor's will continue at their set speeds untill otherwise changed or stopped
 * 
 * @ left 0-500 Speed at which the left wheel motor will rotate
 * @ right 0-500 Speed at which the left wheel motor will rotate
 * 
 */
void drive(int left, int right) {
    create_drive_direct(left, right);
}

/** Stops the create driving
 * 
 * Sets the the create's wheel motors to zero stopping the create from driving
 */
void drive_stop() {
    create_drive_direct(0, 0);
}

int rcliff() {
    return get_create_rcliff();
}

int lcliff() {
    return get_create_lcliff();
}

int rfcliff() {
    return get_create_rfcliff();
}

int lfcliff() {
    return get_create_lfcliff();
}

int rbump() {
    return get_create_rbump();
}

int lbump() {
    return get_create_lbump();
}
