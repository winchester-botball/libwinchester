#ifndef GENERAL_H
#define GENERAL_H

void competition_start(int port);

void interpolateServo(int port, int end, int time);
void interpolateServoWithStart(int port, int start, int end, int time);
void dualInterpolateServo(int portA, int portB, int endA, int endB, int time);
void dualInterpolateServoWithStart(int portA, int portB, int startA, int startB, int endA, int endB, int time);

void moveMotorFor(int port, int power, int time);

#endif