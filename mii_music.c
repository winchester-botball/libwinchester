#include <kipr/botball.h>

#define RUN_SCRIPT create_write_byte(141) //macro to run the currently loaded script

//defines some note durations
//change the top constant MEASURE (4/4 time) to get faster/slower speeds
#define MEASURE 160        //length of one measure in 64thbs of a second
#define HALF MEASURE/2        //length of a half note
#define Qd MEASURE*3/8         //length of a 3/8 note
#define Q MEASURE/4        //length of a quarter note
#define E MEASURE/8        //length of an eighth note
#define Ed MEASURE*3/16        //length of a 3/16 note
#define S MEASURE/16        //length of a sixteenth note
#define R 30                //byte code for rest
#define MEASURE_TIME 1000*MEASURE/64

//defines all the byte codes for notes, uses MIDI note definitions
int c2 = 36;
int cis2 = 37;
int des2 = 37;
int d2 = 38;
int dis2 = 39;
int ees2 = 39;
int e2 = 40;
int f2 = 41;
int fis2 = 42;
int ges2 = 42;
int g2 = 43;
int a2 = 45;
int ais2 = 46;
int bes2 = 46;
int b2 = 47;
int c3 = 48;
int cis3 = 49;
int des3 = 49;
int d3 = 50;
int dis3 = 51;
int ees3 = 51;
int e3 = 52;
int f3 = 53;
int fis3 = 54;
int ges3 = 54;
int g3 = 55;
int gis3 = 56;
int aes3 = 56;
int a3 = 57;
int ais3 = 58;
int bes3 = 58;
int b3 = 59;
int c4 = 60;
int cis4 = 61;
int des4 = 61;
int d4 = 62;
int dis4 = 63;
int ees4 = 63;
int e4 = 64;
int f4 = 65;
int fis4 = 66;
int ges4 = 66;
int g4 = 67;
int gis4 = 68;
int aes4 = 68;
int a4 = 69;
int ais4 = 70;
int bes4 = 70;
int b4 = 71;
int c5 = 72;
int cis5 = 73;
int des5 = 73;
int d5 = 74;
int dis5 = 75;
int ees5 = 75;
int e5 = 76;
int f5 = 77;
int fis5 = 78;
int ges5 = 78;
int g5 = 79;
int gis5 = 80;
int aes5 = 80;
int a5 = 81;
int ais5 = 82;
int bes5 = 82;
int b5 = 83;
int c6 = 84;
int cis6 = 85;
int des6 = 85;
int d6 = 86;
int dis6 = 87;
int ees6 = 87;
int e6 = 88;
int f6 = 89;
int fis6 = 90;
int ges6 = 90;


//one song bpacket,b sends a series of byte commands to the create to que up some notes
void mii_one(){
    create_write_byte(140);    //things command specifies that the following commands are songs
    create_write_byte(0);        //the song number, identifies which song (0-3) will be queued up
    create_write_byte(14);    //specifies the number of notes in the song
    create_write_byte(fis4);    //first note of the song
    create_write_byte(Q);        //duration of the first note
    create_write_byte(a4);    //second note of the song
    create_write_byte(E);        //duration of the second note
    create_write_byte(cis5);    //etc all the way down
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(a4);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(fis4);
    create_write_byte(E);
    create_write_byte(d4);
    create_write_byte(E);
    create_write_byte(d4);
    create_write_byte(E);
    create_write_byte(d4);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(Q);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(cis4);
    create_write_byte(E);
}

void mii_two(){
    create_write_byte(140);
    create_write_byte(1);
    create_write_byte(12);
    create_write_byte(d4);
    create_write_byte(E);
    create_write_byte(fis4);
    create_write_byte(E);
    create_write_byte(a4);
    create_write_byte(E);
    create_write_byte(cis5);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(a4);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(fis4);
    create_write_byte(E);
    create_write_byte(e5);
    create_write_byte(Qd);
    create_write_byte(ees5);
    create_write_byte(E);
    create_write_byte(d5);
    create_write_byte(Q);
    create_write_byte(R);
    create_write_byte(Q);
}

void mii_three(){
    create_write_byte(140);
    create_write_byte(2);
    create_write_byte(15);
    create_write_byte(gis4);
    create_write_byte(Q);
    create_write_byte(cis5);
    create_write_byte(E);
    create_write_byte(fis4);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(cis5);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(gis4);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(cis5);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(g4);
    create_write_byte(E);
    create_write_byte(fis4);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
}

void mii_four(){
    create_write_byte(140);
    create_write_byte(3);
    create_write_byte(10);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(Q);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(e4);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(E);
    create_write_byte(R);
    create_write_byte(Q);
}


/*
*This function is the song that you want to play; it strings together all of the bsongsb while queuing *up for the next one
*Note: you do have to msleep for the amount of time each bsongb takes, which is the number of *measures in the bsongb multiplied by 1000 and divided by 64 to convert from 64ths of a second *to milliseconds
*/
void mii_song(){
    mii_one();                //loads up the first song to be played
    RUN_SCRIPT;                //macro that runs the song
    create_write_byte(0);            //specified by this song number
    msleep(MEASURE*2*1000/64+10); //msleep for number of measures in the song (+ some extra)

    mii_two();                //loads up the second song to be played
    RUN_SCRIPT;                //macro that runs the song
    create_write_byte(1);            //specified by this song number
    msleep(MEASURE*2*1000/64+10); //msleep for number of measures in the song (+ some extra)

    mii_three();
    RUN_SCRIPT;
    create_write_byte(2);
    msleep(MEASURE*2*1000/64+10); 

    mii_four();
    RUN_SCRIPT;
    create_write_byte(3);
    msleep(MEASURE*3*1000/64/2+10);
}

void setup(){
    printf("Mii Song v1 \n");
    printf("WALLABY BATTERY CHARGE: %f%%\n", 100*(double)power_level());
    create_connect();
    create_full();
    printf("CREATE BATTERY CHARGE: %f%%\n", (float)get_create_battery_charge() / (float)get_create_battery_capacity());
    msleep(10);
    thread threadId = thread_create(mii_song);      //creates the background thread of the song
    thread_start(threadId);                			//starts the song in the background
    msleep(10);
}


int main(){
    setup();
    printf("MII SONG\n");
    msleep(30000);
    return 0;
}