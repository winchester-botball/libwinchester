#ifndef CREATE_H
#define CREATE_H

void create_start();

void drive(int left, int right);
void drive_stop();
int rcliff();
int lcliff();
int rfcliff();
int lfcliff();
int rbump();
int lbump();

#endif