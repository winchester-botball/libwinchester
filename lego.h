#ifndef LEGO_H
#define LEGO_H

extern const int LEFT_MOTOR;
extern const int RIGHT_MOTOR;

void drive_freeze();
void drive_clear();
void drive(int left, int right);
/*void motor_drive(int left, int right);
void right(int degrees, double radius);
void right_speed(int degrees, double radius, int speed);
void left(int degrees, double radius);
void left_speed(int degrees, double radius, int speed);
void forward(int distance);
void forward_speed(int distance, int speed);
void backward(int distance);
void backward_speed(int distance, int speed);

void drive_by_distance(float distance, int rate);
*/


#endif
