/** Setting Up

1. Copy all files over
2. Delete irrelevant files (Either delete lego.c and lego.h if you're using a create, or delete create.c and create.h if you're using a lego bot)
3. Set the below constants to values, and leave useless values as -1
4. Write code

*/

#include <kipr/wombat.h>

const int TOPHAT_FRONT = -1;
const int TOPHAT_LEFT = -1;
const int TOPHAT_RIGHT = -1;

const int LEFT_MOTOR = -1;
const int RIGHT_MOTOR = -1;

const int SWHITE = -1;
const int SBLACK = -1;
const int THRESHOLD = -1;

int main()
{
    printf("Hello World\n");
    return 0;
}
