#include "gyro.h"
#include <kipr/wombat.h>
#include <time.h>

float angle = 0;
float bias = 0;
float lastTime;
mutex biasMutex;

int c = 0;

void gyroThread() {
    while(1) {
        float curTime = systime();
        angle += (gyro_z()-bias)*(curTime-lastTime);
        lastTime = curTime;
        mutex_lock(biasMutex);
        c++;
        if(c%100==0) {
            printf("%f -> ", angle/2575000*360); // /560000*90/100*90
            c = 0;
        }
        mutex_unlock(biasMutex);
        msleep(1);
        // usleep(1);
    }
}

/*
int main()
{
    printf("Starting Calibration (Vivian!)\n");
    int tempSum = 0;
    int i = 0;
    float timeToCalibrate = 1000;
    for(i = 0; i < timeToCalibrate; i++) {
        tempSum += gyro_z();
        msleep(1);
    }
    bias = tempSum / timeToCalibrate;
    printf("Bias: %f\n", bias);
    printf("Starting Thread\n");
    biasMutex = mutex_create();
    thread t = thread_create(gyroThread);
    lastTime = systime();
    thread_start(t);
    msleep(100000);
    thread_destroy(t);
    return 0;
}
*/
