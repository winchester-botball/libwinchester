#include "general.h"
#include <kipr/wombat.h>
#include <math.h>



/*! \file general.c
* General functions
* Contains many general functions
*/

/**
 * Competition start at light function
 */
void competition_start(port) {
    wait_for_light(port);
    shut_down_in(119);
}

/**
 * Overloaded interpolate servo without start position
 */

void interpolateServo(int port, int end, int time) {
  interpolateServoWithStart(port, get_servo_position(port), end, time);
}

/** Uses the starting and ending positions of the servo and calculates a rate of change to complete the change of position in inputted amount of time
 * Allows servos to gradually change positon instead of suddenly changing, which can cause damage
 * 
 * @param port Port of servo which will be interpolated
 * @param start Starting position of servo (0-2047) 
 * @param end Ending position of servo (0-2047)
 * @param time Time for which the servo will gradually change position
 * 
 */

void interpolateServoWithStart(int port, int start, int end, int time) {
    int endTime = systime() + time;
    while(systime() < endTime) {
        set_servo_position(port, (int) ((end - start) * (1.0 - (((double) (endTime - systime())) / time)) + start));
        msleep(1);
    }
}

/**
 * Overload of dualInterpolateServo without starting positions
 */

void dualInterpolateServo(int portA, int portB, int endA, int endB, int time) {
    dualInterpolateServoWithStart(portA, portB, get_servo_position(portA), get_servo_position(portB), endA, endB, time);
}

/** Uses starting and ending positions of two servos to change the position of both of them in the smae amount of time. Calculate two separate rates of change
 * interpolateServo but its for 2 servos at the same
 * 
 * @param portA Port of first servo
 * @param portB Port of second servo
 * @param startA Starting position of first servo
 * @param startB Starting position of second servo
 * @param endA Ending position of first servo
 * @param endB Ending position of second servo
 * @param time Time in which both servos will interpolate
 * 
*/
void dualInterpolateServoWithStart(int portA, int portB, int startA, int startB, int endA, int endB, int time){
    unsigned long endTime = systime()+time;
    while(systime()<endTime){
        set_servo_position(portA, (endA-startA)*(1.0-(((double)(endTime-systime()))/((double)time)))+startA);
        set_servo_position(portB, (endB-startB)*(1.0-(((double)(endTime-systime()))/((double)time)))+startB);
        msleep(1);
    }
    set_servo_position(portA, endA);
    set_servo_position(portB, endB);
}

//usually shouldn't be using this unless time is a problem. speed of drive accerates then decrerates to preserve some accuracy
/*void drive(int left, int right, int time){
    int dx = 150;   //Speed gain per increment  edit this to your need
    int dt = 25;        //Time per increment    edit this to your need
    
    int accelSpeed = 0;
    int leftExtraTime = 0;
    int rightExtraTime = 0;
    
    while (time > dt) {  
        accelSpeed += dx;
        int left = fmin(abs(left), accelSpeed);
        int right = fmin(abs(right), accelSpeed);
        if (left != abs(left))  leftExtraTime   +=  dt * (abs(left) - left)      / abs(left);
        if (right != abs(right)) rightExtraTime     +=  dt * (abs(right) - right) / abs(right);
        left = (left < 0 ? -left : left);
        right  = (right < 0 ? -right : right);
        mav(LEFT_WHEEL, left);
        mav(RIGHT_WHEEL, -right);
        msleep(dt - 2);     //Calculations take ~2msec
        time -= dt;
    }
    
    mav(LEFT_WHEEL, left);
    mav(RIGHT_WHEEL, -right);
    msleep(fmin(leftExtraTime, rightExtraTime) + time);
    
    if (leftExtraTime > rightExtraTime) {
        mav(LEFT_WHEEL, left*fudge);
        mav(RIGHT_WHEEL, 0);
        msleep(leftExtraTime - rightExtraTime);
    }
    else {
        mav(LEFT_WHEEL, 0);
        mav(RIGHT_WHEEL, -rightspd);
        msleep(rightExtraTime - leftExtraTime);
    }

    freeze(LEFT_WHEEL);
    freeze(RIGHT_WHEEL);
    msleep(10);
}
*/

/** Used to move a individual specific motor
*
 * used to move a motor that isnt attached to a wheel like a pully for a arm. The motor must
 * be speficied with the port its plugged into. The rotation of the motor is speficied using power and
 * the duration of the rotating is set by specifing a time
 * 
 * @param port The port of the motor that is desired to be used
 * @param power This sets the speed of rotation for the motor in mav
 * @param time This sets the time that the rotation will occur for
 * 
 */
void moveMotorFor(int port, int power, int time){
    mav(port, power);
    msleep(time);
    motor(port, 0);
}
