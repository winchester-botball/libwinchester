#include "sensors.h"
#include "lego.h"
#include <math.h>
#include <kipr/wombat.h>

/*! \file sensors.c
* Sensor Functions
*
* Contains many of the functions that use sensors.
*/

/** Follows the chosen side of a black line
 *
 * Designed for lego. Uses a top hat on port "HAT_FRONT" to follow the chosen side of a line.
 * Uses the predefined variables of SWHITE and SBLACK. You can tweak the left and right power, as well as the time to line follow.
 * WARNING: DO NOT USE THIS TO GO BACKWARDS
 *
 * @param left The percent power to use for the left motor while line following
 * @param right The percent power to use for the right motor while line following
 * @param time The amount of time to follow the line for before "stopping." (Does not auto stop)
 * @param direction 0 = left side, 1 = right side of tape TBD might be flipped
 *
 */
void lineFollow(int left, int right, int time, int direction) {
    long endTime = systime() + time;
    int tophat = analog(TOPHAT_FRONT);
    double percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
    if (direction == 1){
        while (systime() < endTime) {
            drive((percent > 0.5 ? 1 : (percent / 0.5)) * left, (percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * right);
            msleep(1);
            tophat = analog(TOPHAT_FRONT);
            percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
        }
    }
    else if (direction == 0){
           while (systime() < endTime) {
                drive((percent < 0.5 ? 1 : (1.0 - percent) / 0.5) * left, (percent > 0.5 ? 1 : (percent / 0.5)) * right);
                msleep(1);
                tophat = analog(TOPHAT_FRONT);
                percent = ((double) (tophat - SWHITE)) / ((double) (SBLACK - SWHITE));
            }
    }
}

/* A simpler version of lineFollow
 *
 * Follows a chosen side 
 * Designed for a legobot
 *
 * @param time
 * @param power
 * @param adjustPower
 * @param tophat port of tophat you want lineFollowSimple to use
 * @param direction 0 being left side of tape, 1 being right side of tape
 */

void lineFollowSimple (int time, int power, int adjustPower, int tophat, int direction){
    int moop = 0;
    while (time > moop){
        if(direction == 1){
          if((analog(tophat) < THRESHOLD)){
                drive(power, power + adjustPower);
            }
            else if ((analog(tophat) > THRESHOLD)){
                drive(power + adjustPower, power);
            }
            else {
                drive(power, power);  
            }
        }
        else if(direction == 0){
            if((analog(tophat) > THRESHOLD)){
                drive(power + adjustPower, power);
            }
            else if ((analog(tophat) < THRESHOLD)){
                drive(power, power + adjustPower);
            }
            else {
                drive(power, power);  
            }
        }
        else{
            printf("No. I refuse.");
        }
        msleep(1);
        moop += 1;
     }
}


/** Returns whether the value is positive or negative
 *
 *  This function is used in driveToLine to determine whether the tophat is on black or white
 *  
 * @param n The value to get the sign of
 * @param tolerance The value that determines the amount of error for n due to the "gray" space in between the black tape and white
    board
 */

int getSign(int n, int tolerance) {
    if (n > tolerance) {
        return 1;
    } else if (n < -tolerance) {
        return -1;
    } else {
        return 0;
    }
}

/* Drives to black tape and aligns its wheels to the tape
 *
 * Designed for a lego robot. Uses two tophats on the left and right side of a robot and drives until the tophats see the black tape.
 * Will slightly adjust its power as it drives to align to the tape.
 * Also requires a predefined variable called THRESHOLD which is a value in between what the tophat reads as white and as black
 *
 * @param power the initial power the robot drives at 
 * @param adjustPower the power factor it adjusts by to align
 */
 
void driveToLine(int power, int adjustPower) {
    int leftPower = power;
    int rightPower = power;
    int leftSign = getSign(THRESHOLD - analog(TOPHAT_LEFT), 0);
    int rightSign = getSign(THRESHOLD - analog(TOPHAT_RIGHT), 0);
    while (!(leftSign == -1 && rightSign == -1)) {
        drive(leftSign * leftPower, rightSign * rightPower);
        msleep(1);
        leftSign = getSign(THRESHOLD - analog(TOPHAT_LEFT), 0);
        rightSign = getSign(THRESHOLD - analog(TOPHAT_RIGHT), 0);
        if (leftSign == -1) {
            leftPower = adjustPower;
        }
        if (rightSign == -1) {
            rightPower = adjustPower;
        }
        if (leftPower == adjustPower && rightPower == adjustPower) {
            break;
        }
    }
    drive(0, 0);
}

