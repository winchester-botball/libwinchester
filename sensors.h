#ifndef SENSORS_H
#define SENSORS_H

extern const int HAT_FRONT;
extern const int SWHITE;
extern const int SBLACK;
extern const int THRESHOLD;
extern const int TOPHAT_LEFT;
extern const int TOPHAT_RIGHT;
extern const int TOPHAT_FRONT;


void lineFollow(int left, int right, int time, int direction);
int getSign(int n, int tolerance);
void driveToLine(int power, int adjustPower);
void lineFollowSimple (int time, int power, int adjustPower, int tophat, int direction);

#endif
