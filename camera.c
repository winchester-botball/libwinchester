#include <kipr/wombat.h>
#include <kipr/geom.h>

/* How to set up camera on controller
 *
 * Plug camera into controller's USB port
 * Navigate to the channels page in controller's settings
 * Choose default camera configuration, if there is no default create a new one and favorite that one (indicated by a gold star)
 * Select the configuration you want to use and press edit, this will bring you to the channels
 * Hit the add button, keep the channel type on HSV Blob Tracking
 * Hit Channel 0 and click edit, this brings you to the channel's callibration
 * A rectangle should appear with the camera view, move the rectangle and adjust its size until you have the color and sensitivity
   desired
 * Repeat steps as necessary for more channels/colors
 */

 // Below are camera functions that you will need whenver you use the camere

int workingCamera = 0;

void flagCamera() {
    if(camera_update()==1){
        printf("Camera is working\n");
        workingCamera = 1;
    }
}

void checkCamera() {
    thread t = thread_create(flagCamera);
    thread_start(t);
}

int updateCamera() {
    if(workingCamera) {
        return camera_update();
    } else {
        return 0;
    }
}

void checkStatus(int status, char failureMessage []){
    if(status == 0) {
        printf(failureMessage);
    }
}

void warmupCamera(int interval, int timeout){
    printf("Warmup Camera: ");
    long time = systime();
    while(systime() - time < timeout){
        checkStatus(updateCamera(), "Fail -> ");
        msleep(interval);
    }
    printf("Done\n");
}


int object_centered_in_area(point2* center, rectangle* r) {
    if(center->x < r->ulx || center->x > r->ulx + r->width)
        return 0;
    if(center->y < r->uly || center->y > r->uly + r->height)
        return 0;
    return 1;
}

int object_partially_in_area(rectangle* bbox, rectangle* r) {
    if(bbox->ulx + bbox->width < r->ulx)
        return 0;
    if(bbox->ulx > r->ulx + r->width)
        return 0;
    if(bbox->uly + bbox->height < r->uly)
        return 0;
    if(bbox->uly > r->uly + r->height)
        return 0;
    return 1;
    
}

int object_fully_in_area(rectangle* bbox, rectangle* r) {
    if(bbox->ulx + bbox->width > r->ulx + r->width)
        return 0;
    if(bbox->ulx < r->ulx)
        return 0;
    if(bbox->uly + bbox->height > r->uly + r->height)
        return 0;
    if(bbox->uly < r->uly)
        return 0;
    return 1;
}


//-----------------------------DO NOT USE ANYTHING BELOW THIS LINE. IT IS MEANT FOR REFERENCE PURPOSES ONLY----------------------

//This is an example of camera code being used on Midbot (2018 robot), if you need further reference you can check the archive for Midbot's full code or Monstrosity's full code. 

/** Used to determine position
 *  
 *  Determines the position of the color specified in a certain channel
 *
 * @channel In the channel specified
 * @threshold gets all objects above the size of the threshold
 *
 */
/*
int getRelativePosition(int channel, int threshold){
    int count = get_object_count(channel);
    int totalPosition = 0;
    int totalArea = 0;
    int index = 0;
    while(index < count){
        int area = get_object_area(channel, index);
        if(area > threshold){
            //printf("Object: %d/%d w/ %d + %d -> ", index+1, count, get_object_centroid_x(channel, index), area);
            totalPosition += get_object_centroid_y(channel, index) * area;
            totalArea += area;
        }
        index++;
    }
    //printf("Done\n");
    if(totalArea == 0){
        return 999999;
    }
    return (int)(totalPosition / totalArea);
    if (get_object_count(channel) > 0){
        return get_object_area(channel,0);
    } else {
        return 999999;
    }
}
*/

/** Gets the center of a color
 *  
 *  Gets the center of the desired channel colored object
 *  
 *  @channel channel number of the color you wish to use, found on controller
 */ 

/*
int getcenter(int channel){
    int area = 0;
    int i = 0;
    int center = 0;
    while(area < 20 || center == 0){ //sometimes camera will detect a large blob at 0,0 randomly
        checkStatus(updateCamera(), "Failure to Update Camera\n");
        area = get_object_area(channel,0);
        center = get_object_centroid(channel,0).x;
        i++;
        if(i > 20){return -1;}
    }
    printf("Channel: %d, Area: %d, Center: %d, Iteration: %d\n", channel, area, center, i);
    return center;
}
*/

/** Used to sort order of colored objects
 *
 *  Sorts the position (assigned 0-2) of the colored objects by comparing the info received from camera
 *  
 *  @array empty array that receives the number order of the objects 
 *  @timeout time it allows to run the function before it says too bad
 */

/* Historical Code, should be used as reference only */
/*
void getOrder(int array[], int timeout){
    if(!workingCamera){
        array[RED_ZONE] = 0;
        array[GREEN_ZONE] = 1;
        array[YELLOW_ZONE] = 2;
    }
    int red = 999999;
    int green = 999999;
    int yellow = 999999;
    //long time = systime();
    checkStatus(updateCamera(), "Failure to Update Camera\n");
    red = getcenter(RED_ZONE);
    green = getcenter(GREEN_ZONE); 
    yellow = getcenter(YELLOW_ZONE);
    //if any color is not detected, automatically assume its the farthest
    if(red == -1){
        if(!state){ //if on left side
            red = 9999999;
        }
    }
    else if(green == -1){
        if(!state){ //if on left side
            green = 9999999;
        }
    }
    else if(yellow == -1){
        if(!state){ //if on left side
            yellow = 9999999;
        }
    }

    printf("Red: %d, Green: %d, Yellow: %d\n", red, green, yellow);
    array[RED_ZONE] = red;
    array[GREEN_ZONE] = green;
    array[YELLOW_ZONE] = yellow;

    if(array[YELLOW_ZONE] > array[RED_ZONE]){
        if(array[RED_ZONE] > array[GREEN_ZONE]){
            array[YELLOW_ZONE] = 0;
            array[RED_ZONE] = 1;
            array[GREEN_ZONE] = 2;
            // 0 < 1 < 2
        }else{
            if(array[YELLOW_ZONE] > array[GREEN_ZONE]){
                array[YELLOW_ZONE] = 0;
                array[RED_ZONE] = 2;
                array[GREEN_ZONE] = 1;
                // 0 < 2 < 1
            }else{
                array[YELLOW_ZONE] = 1;
                array[RED_ZONE] = 2;
                array[GREEN_ZONE] = 0;
                // 2 < 0 < 1
            }
        }
    }else{
        if(array[YELLOW_ZONE] > array[GREEN_ZONE]){
            array[YELLOW_ZONE] = 1;
            array[RED_ZONE] = 0;
            array[GREEN_ZONE] = 2;
            // 1 < 0 < 2
        }else{
            if(array[RED_ZONE] > array[GREEN_ZONE]){
                array[YELLOW_ZONE] = 2;
                array[RED_ZONE] = 0;
                array[GREEN_ZONE] = 1;
                // 1 < 2 < 0
            }else{
                array[YELLOW_ZONE] = 2;
                array[RED_ZONE] = 1;
                array[GREEN_ZONE] = 0;
                //2 < 1 < 0
            }
        }
    }
}
*/
